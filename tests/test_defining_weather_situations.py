import pytest
from pygg.run.gral import define_ranges_of_weather_situations

def test_define_ranges_of_weather_situations():

    ranges = define_ranges_of_weather_situations([1,2,3])

    # Check the list of ranges is as expected 
    assert ranges == [(1,4)], f"{ranges}!= [(1,4)]"

    ranges = define_ranges_of_weather_situations([1,2, 4,5])
    assert ranges == [(1,3), (4,6)], f"{ranges}!= [(1,3), (4,6)]"

    ranges = define_ranges_of_weather_situations([3,4,5])
    assert ranges == [(3,6)], f"{ranges}!= [(3,6)]"


if __name__ == '__main__':
    test_define_ranges_of_weather_situations()