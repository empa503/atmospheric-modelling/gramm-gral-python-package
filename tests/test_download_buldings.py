"""Test that the function to download the buildings works as expected."""

from pygg.inputs.streetmap import download_street_data, write_shape
from pygg.test_utils.grids import SMALL_ZH_GRID_LV95
from pygg.test_utils import TestPaths

import logging

logging.basicConfig()
logging.getLogger("pygg.inputs.streetmap").setLevel(logging.DEBUG)


def test_download_street_data():
    """Test that the function to download the buildings works as expected."""
    shape_file = download_street_data(TestPaths.OUTPUT / 'test.shp', SMALL_ZH_GRID_LV95)
    assert shape_file.exists()
    assert shape_file.is_file()
    assert shape_file.suffix == ".shp"

test_download_street_data()