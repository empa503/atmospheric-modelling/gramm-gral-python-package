from pathlib import Path
import yaml 
import pygg 


default_config = Path(*pygg.__path__, '..', 'examples', 'config.yaml')

def test_example_is_in_folder():
    assert default_config.exists()
    assert default_config.is_file()

def test_can_load_yaml():
    loaded = yaml.load(default_config.read_text(), Loader=yaml.FullLoader)
