#%%
from pygg.grids import BasicGrid

#%% create a grid
grid = BasicGrid( 6.3, 7.2, 45, 47.1, 10, 10, 47)

#%% get the center mesh
mesh_x, mesh_y = grid.get_center_mesh()

#%% get the corner mesh
mesh_x, mesh_y = grid.get_corner_mesh()

#%% get the index
grid.get_index(5, 5)

#%% get geopandas cell centers
centers = grid.get_gpd_cell_centers()
centers.to_crs(4326)
# %%

grid.get_mesh_from_gpd(centers.to_crs(4326))

#%%
grid_array = grid.get_xr_grid()
grid_array
# %%
import xarray as xr
da = xr.Dataset(coords={"x": grid.xcenter, "y": grid.ycenter})
da.rio.write_crs(grid.crs).rio.set_spatial_dims(x_dim="x", y_dim="y", inplace=True)
#.rio
#.rio.write_coordinate_system(inplace=True)

# %%
