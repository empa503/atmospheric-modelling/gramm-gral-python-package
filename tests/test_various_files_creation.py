from pygg.inputs.grammin import write_grammindat_file
from pygg.inputs.iin import write_iindat_file
from pygg.inputs.meteo import write_meteopgt_file
from pygg.test_utils import TestPaths

def test_grammindat_file_creation():
    """Test the creation of the 'grammin.dat' file."""
    path = TestPaths.OUTPUT / "GRAMMin.dat"
    file_path = write_grammindat_file(TestPaths.OUTPUT, 1)
    assert file_path == path
    assert path.exists()
    assert path.is_file()
    # Check that the file has 6 lines
    with open(path, "r") as file:
        lines = file.readlines()
        assert len(lines) == 6
        


test_grammindat_file_creation()


def test_iin_file_creation():
    """Test the creation of the 'iin.dat' file."""
    path = TestPaths.OUTPUT / "IIN.dat"
    file_path = write_iindat_file(TestPaths.OUTPUT, 36)
    assert file_path == path
    assert path.exists()
    assert path.is_file()


test_iin_file_creation()


def test_meteopgt_file_creation():
    """Test the creation of the 'meteopgt.all' file."""
    path = TestPaths.OUTPUT / "meteopgt.all"
    file_path = write_meteopgt_file(TestPaths.OUTPUT)
    assert file_path == path
    assert path.exists()
    assert path.is_file()

test_meteopgt_file_creation()