"""Utility functions for testing."""

import pygg

from dataclasses import dataclass
from pathlib import Path


@dataclass
class TestPaths:
    """A class that contains the names of directories and files for testing."""

    ASTER: Path = Path("/store/empa/em05/isuter/projects/gg/data/topography")
    CORINE: Path = Path("/store/empa/em05/isuter/projects/gg/data/landuse/g100_06.tif")

    OUTPUT: Path = Path(*pygg.__path__, '..', 'tests', 'output_files' )

