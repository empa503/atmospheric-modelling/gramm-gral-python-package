from pygg.grids import BasicGrid


WGS84 = "EPSG:4326"

ZH_GRID_WGS84 = BasicGrid(8.2593, 8.822, 47.131, 47.497, 10, 10, 47, crs=WGS84)
ZH_GRID_LV95 = BasicGrid( 2670000, 2690000, 1240000, 1260000, 10, 10, 47)
# Smaller domain and higher grid resolution
SMALL_ZH_GRID_LV95 = BasicGrid( 2682600, 2683620, 1247670, 1248575, 100, 100, 47)
