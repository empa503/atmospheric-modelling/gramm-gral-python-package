import xarray as xr
import numpy as np

VALID_GROUPBY = {"month", "hour"}
group_error = ValueError("groupby must be one of %r." % VALID_GROUPBY)


def root_mean_squared_error(
    x: xr.DataArray, y: xr.DataArray, group_by: str = None
) -> xr.DataArray:
    """
    Calculate the root mean squared error, describing the "degree of departure" between the two datasets (Paul et al. 2023).
    When group_by is "month" or "hour", calculate it separately per month or hour of day.
    """
    if group_by == None:
        # if no or an invalid group_by is given, calculate it for the whole dataset
        return np.sqrt(((x - y) ** 2).mean(dim="time"))
    else:
        if group_by not in VALID_GROUPBY:
            raise group_error
        return np.sqrt(((x - y) ** 2).groupby(f"time.{group_by}").mean(dim="time"))


def centered_root_mean_squared_error(
    x: xr.DataArray, y: xr.DataArray, group_by: str = None
) -> xr.DataArray:
    """
    Bias-corrected RMSE.
    The mean value is removed from each measurement before calculating the RMSE (as defined in GAW report 293).
    When group_by is "month" or "hour", calculate it separately per month or hour of day.
    """

    def get_diff(x, y):
        # unbiased differences
        return (x - x.mean(dim="time")) - (y - y.mean(dim="time"))

    if group_by == None:
        crmse = np.sqrt((get_diff(x, y) ** 2).mean(dim="time"))
    else:
        if group_by not in VALID_GROUPBY:
            raise group_error
        x = x.groupby(f"time.{group_by}")
        y = y.groupby(f"time.{group_by}")
        crmse = np.sqrt(
            (get_diff(x, y) ** 2).groupby(f"time.{group_by}").mean(dim="time")
        )

    # simplification (Paul et al. 2023):
    # rmse = root_mean_squared_error(x, y, group_by)
    # crmse = np.sqrt(rmse**2 - (x.mean(dim="time")-y.mean(dim="time"))**2)
    return crmse


def bias(x: xr.DataArray, y: xr.DataArray, group_by: str = None) -> xr.DataArray:
    """
    Mean of the differences between each simulation and observation.
    When group_by is "month" or "hour", calculate it separately per month or hour of day.
    """
    if group_by == None:
        return (x - y).mean(dim="time")
    else:
        if group_by not in VALID_GROUPBY:
            raise group_error
        return (x - y).groupby(f"time.{group_by}").mean(dim="time")


def meanbias(x: xr.DataArray, y: xr.DataArray, group_by: str = None) -> xr.DataArray:
    """
    Difference between the simulation-mean-value and the observation-mean-value.
    When group_by is "month" or "hour", calculate it separately per month or hour of day.
    """
    if group_by == None:
        return (x).mean(dim="time") - (y).mean(dim="time")
    else:
        if group_by not in VALID_GROUPBY:
            raise group_error
        return (x).groupby(f"time.{group_by}").mean(dim="time") - (y).groupby(
            f"time.{group_by}"
        ).mean(dim="time")


def correlation(x: xr.DataArray, y: xr.DataArray) -> xr.DataArray:
    """
    Pearson correlation coefficient between x and y.
    x = simulated, data array with sites and time
    y = observed, data array with sites and time
    """
    return xr.corr(x, y, dim="time")