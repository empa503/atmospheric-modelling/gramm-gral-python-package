"""This is the processing of the ggeom file.

From the GRAMM manual:

    The ggeom.asc contains most of the information needed to establish the GRAMM grid. 

    The following parameters and arrays are included in subsequent order:

        Int32 NX number of cells in x-direction
        Int32 NY number of cells in y-direction
        Int32 NZ number of cells in z-direction

        double[] X Distance of grid cell centre from eastern domain border
        double[] Y Distance of grid cell centre from southern domain border
        double[] Z Temporary array for generating the terrain-following grid

        double[][]   AH Height of the surface

        double[][][] VOL Volume of grid cells

        double[][][] AREAX Area of the grid cell in x-direction
        double[][][] AREAY Area of the grid cell in y-direction

        double[][][] AREAZX Projection of the ground area of the grid cell in x-direction
        double[][][] AREAZY Projection of the ground area of the grid cell in y-direction

        double[][][] AREAZ Bottom area of the grid cell

        double[][][] ZSP Height of the centre point of each grid cell

        double[] DDX Horizontal grid size in x-direction
        double[] DDY Horizontal grid size in y-direction
        double[] ZAX Distance between neighbouring grid cells in x-direction
        double[] ZAY Distance between neighbouring grid cells in y-direction

        Int32 IKOOA Western border of model domain
        Int32 JKOOA Southern border of model domain

        WINKEL is a historic parameter equal to zero (not used anymore).

        double[][][] AHE Heights of the corner points of each grid cell
"""

import logging
from os import PathLike

import numpy as np
from pygg.inputs.aster import smooth_borders
import xarray as xr

from scipy.signal import convolve2d

from pygg.grids import GrammGrid


def _array_to_string(arr: np.ndarray) -> str:
    """Convert an array to a string with spaces between the values.

    2d arrays are flattened in Fortran order.
    """
    return " ".join(arr.flatten(order="F").astype(str))


def write_ggeom_file(
    ggeom_file: PathLike,
    grid: GrammGrid,
    aster_data: xr.DataArray,
    smoothed_border_cells: int = 6,
) -> None:
    """Write the ggeom file for the given grid.

    This file is an input file for GRAMM.

    The aster data is smoothed at the borders for ??? reason. TODO explain why.
    This also calculates the aster data at the edges of the gridcells,
    assuming simple interpolation between the centers.
    TODO: decide if we want direct interpolation from the aster data.
    """

    logging.getLogger("pygg.inputs.ggeom").info("Writing ggeom file")

    # Z = height of cell center
    # Starts at the smallest height from the topography
    Z = aster_data.min().values - grid.dz0 + grid.z
    z_max = Z.max()

    # Smooth the borders of the aster data
    aster_smoothed, smooth_min = smooth_borders(aster_data, smoothed_border_cells)
    aster_centers = aster_smoothed.to_numpy()
    # Calculate aster data on the edges of the gridcells
    # Make a convolution using scipy and at the boundary to find the cell egdes
    aster_edges = convolve2d(aster_smoothed, np.ones((2, 2)) / 4.0, mode="full")
    # Set the edges to the smoothed minimum (the boundary value)
    aster_edges[[0, -1], :] = smooth_min
    aster_edges[:, [0, -1]] = smooth_min

    # AH = suface height
    AH = aster_smoothed.to_numpy()

    # ZSP = height of cell edges
    vertical_stretch = (z_max - aster_centers) / grid.z.max()
    ZSP = (
        aster_centers[:, :, np.newaxis]
        + grid.z[np.newaxis, np.newaxis, :] * vertical_stretch[:, :, np.newaxis]
    )
    ZSP = (ZSP[:, :, 0:-1] + ZSP[:, :, 1:]) / 2.0

    # AHE
    vertical_stretch_edges = (z_max - aster_edges) / grid.z.max()
    AHE = (
        aster_edges[:, :, np.newaxis]
        + grid.z[np.newaxis, np.newaxis, :] * vertical_stretch_edges[:, :, np.newaxis]
    )
    # AREAS of the surfaces along x- and y- directions
    AREAX = (
        ((AHE[:, :-1, 1:] - AHE[:, :-1, :-1]) + (AHE[:, 1:, 1:] - AHE[:, 1:, :-1]))
        * 0.5
        * grid.dy
    )

    AREAY = (
        ((AHE[:-1, :, 1:] - AHE[:-1, :, :-1]) + (AHE[1:, :, 1:] - AHE[1:, :, :-1]))
        * 0.5
        * grid.dx
    )

    # AREAS of the ground projected along x- and y- directions
    AREAZX = (
        ((AHE[:-1, :-1, :] - AHE[:-1, 1:, :]) + (AHE[1:, :-1, :] - AHE[1:, 1:, :]))
        * 0.5
        * grid.dy
    )

    AREAZY = (
        ((AHE[:-1, :-1, :] - AHE[1:, :-1, :]) + (AHE[:-1, 1:, :] - AHE[1:, 1:, :]))
        * 0.5
        * grid.dx
    )
    # Bottom area
    AREAZ = (
        grid.dxy
        * (1 + AREAZX**2 / grid.dxy**2) ** 0.5
        * (1 + AREAZY**2 / grid.dxy**2) ** 0.5
    )
    # VOL = volume of the cell
    VOL = (
        0.5 * (AREAZ[:, :, :-1] + AREAZ[:, :, 1:]) * grid.dz[np.newaxis, np.newaxis, :]
    )

    DDX = grid.dx * np.ones((grid.nx))
    DDY = grid.dy * np.ones((grid.ny))
    ZAX = grid.dx * np.ones((grid.nx))
    ZAY = grid.dy * np.ones((grid.ny))
    ZAX[-1] = 0
    ZAY[-1] = 0
    with open(ggeom_file, "w") as f:
        # NX, NY, NZ
        f.write(f"{grid.nx} {grid.ny} {grid.nz} ")  # No backline after that
        # X, Y = distance from border of domain
        X = (grid.xcorner - grid.xmin).astype(int)
        f.write(_array_to_string(X))
        f.write(" ")
        Y = (grid.ycorner - grid.ymin).astype(int)
        f.write(_array_to_string(Y))
        f.write(" ")

        f.write(_array_to_string(Z.round(2)))
        f.write("\n")

        for arr in [AH, VOL, AREAX, AREAY, AREAZX, AREAZY, AREAZ, ZSP]:
            f.write(_array_to_string(arr.round(2)) + "\n")

        for arr in [DDX, DDY, ZAX, ZAY]:
            f.write(_array_to_string(arr) + "\n")

        # IKOOA, JKOOA, WINKEL
        f.write(f"{int(grid.xmin)} {int(grid.ymin)} 0 \n")

        # AHE
        f.write(_array_to_string(AHE))
