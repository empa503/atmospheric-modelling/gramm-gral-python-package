"""Sonic data.

This file was imported from the previous gg package and was not correctly converted.
It is not used at the moment.
"""

import pandas as pd
import numpy as np

import gg.globals as cfg

def read_data(filename):
    """
    Read the 1 minute csv file into a dataframe

    Parameters
    ----------
    filename : string
        directory and and filename of the file containing the meteorology data

    Returns
    -------
    pandas.DataFrame
        data in a table-like format for easy handling of data
    """

    meteo_df = pd.read_csv(filename, delimiter=',',
                           header=0, index_col='datetime')
    meteo_df.index = pd.to_datetime(meteo_df.index)

    return meteo_df


def rotation_one(alpha):
    ''' Rotation about the vertical axis

    alpha = float in radians

    returns an array containing where the velocity vector is multiplied for the first rotation
    '''

    R1 = np.array([[np.cos(alpha), np.sin(alpha), 0],
                   [-np.sin(alpha), np.cos(alpha), 0],
                   [0, 0, 1]])

    return R1


def rotation_two(beta):
    ''' Rotation about the new horizontal axis \

    beta = float in radians
    '''

    R2 = np.array([[np.cos(beta), 0, np.sin(beta)],
                   [0, 1, 0],
                   [-np.sin(beta), 0, np.cos(beta)]])

    return R2


def rotation_three(gamma):
    ''' Rotation for the conservation of momentum flux

    gamma =  float in radians
    '''

    R3 = np.array([[1, 0, 0],
                   [0, np.cos(gamma), np.sin(gamma)],
                   [0, -np.sin(gamma), np.cos(gamma)]])

    return R3


def transform(meteo_df):
    ''' Full transformation of anemometer measurements '''

    u_rot, v_rot, w_rot = [], [], []
    all_alpha = []
    all_beta = []

    for U, V, W in zip(meteo_df['U'], meteo_df['V'], meteo_df['W']):
        alpha = np.arctan(V / U)
        all_alpha.append(alpha)

        R1 = rotation_one(alpha)
        vel0 = np.array([U, V, W])

        u1, v1, w1 = np.dot(R1, vel0)

        beta = np.arctan(w1 / u1)
        all_beta.append(beta)
        R2 = rotation_two(beta)

        vel2 = np.array([u1, v1, w1])

        u2, v2, w2 = np.dot(R2, vel2)
        u_rot.append(u2)
        v_rot.append(v2)
        w_rot.append(w2)

    return all_alpha, all_beta, u_rot, v_rot, w_rot


def fluc_transform(meteo_df, alpha, beta):
    ''' Transform the flux into streamwise coordinates '''

    ufluc_rot, vfluc_rot, wfluc_rot = [], [], []

    for A, B, UFluc, VFluc, WFluc in zip(alpha, beta, meteo_df['Ufluc'], meteo_df['Vfluc'], meteo_df['Wfluc']):

        R1 = rotation_one(A)
        fluc0 = np.array([UFluc, VFluc, WFluc])

        ufluc1, vfluc1, wfluc1 = np.dot(R1, fluc0)

        R2 = rotation_two(B)

        fluc2 = np.array([ufluc1, vfluc1, wfluc1])

        ufluc2, vfluc2, wfluc2 = np.dot(R2, fluc2)

        ufluc_rot.append(ufluc2)
        vfluc_rot.append(vfluc2)
        wfluc_rot.append(wfluc2)

    return ufluc_rot, vfluc_rot, wfluc_rot


def coriolis(latitude):

    fcor = 2 * 7.29 * 0.00001 * np.sin(np.deg2rad(latitude))

    return fcor


def bl_height(u_star, fcor, L):

    first_term = 0.4 * np.sqrt(u_star * 1000 / fcor)

    blh = np.min([first_term, 800]) + 300 * ((2.72) ** (L * 0.01))

    return blh


def vertical_fluc(u_star, blh, L):

    w_var = 0

    if L < 0 :
        w_var = u_star**2 * (1.15 + (0.1 * (blh / -L)**0.67))**2
    elif L >= 0 :
        w_var = 1.56 * u_star**2

    w_fluc = np.sqrt(w_var)

    return w_fluc


def rotated_meteo(meteo_df):
    '''5 min averaged rotated meteorology '''

    # Filter data to make it start with the start of the driving period
    # meteo_df = meteo_df[meteo_df.index > '2018-10-02 12:25:00']
    # meteo_df = meteo_df[meteo_df.index > '2018-12-02 13:45:00']
    alpha, beta, u_rot, v_rot, w_rot = transform(meteo_df)
    ufluc_rot, vfluc_rot, wfluc_rot = fluc_transform(meteo_df, alpha, beta)

    latitude = 49
    fcor = coriolis(latitude)

    w_fluc_list = np.zeros(len(meteo_df))

    for i in range(len(meteo_df)):
        blh = bl_height(meteo_df['U*'].iloc[i], fcor, meteo_df['L'].iloc[i])
        w_fluc = vertical_fluc(
            meteo_df['U*'].iloc[i], blh, meteo_df['L'].iloc[i])
        w_fluc_list[i] = w_fluc

    rotated_df = pd.DataFrame(index=meteo_df.index)

    rotated_df['u_rot'] = np.abs(u_rot)
    rotated_df['v_rot'] = v_rot
    rotated_df['w_rot'] = w_rot
    rotated_df['ufluc_rot'] = ufluc_rot
    rotated_df['vfluc_rot'] = vfluc_rot
    # rotated_df['wfluc_rot'] = wfluc_rot
    rotated_df['wfluc_rot'] = w_fluc_list
    rotated_df['wd'] = meteo_df['Winddir']
    rotated_df['u*'] = meteo_df['U*']
    rotated_df['L'] = meteo_df['L']

    rotated_df['ufluc_rot'] = np.sqrt(rotated_df['ufluc_rot']**2)
    rotated_df['vfluc_rot'] = np.sqrt(rotated_df['vfluc_rot']**2)
    rotated_df['wfluc_rot'] = np.sqrt(rotated_df['wfluc_rot']**2)

    return rotated_df


def filter_outlier(rotated_df, field):

    q1 = rotated_df[field].quantile(0.25)
    q3 = rotated_df[field].quantile(0.75)

    iqr = q3 - q1
    whisk = iqr * 1.5

    up_whisk = q3 + whisk
    low_whisk = q1 - whisk

    filtered_df = rotated_df[rotated_df[field] < up_whisk]
    filtered_df = filtered_df[filtered_df[field] > low_whisk]

    return filtered_df


def filtered_meteo(rotated_df):

    filtered_df = filter_outlier(rotated_df, 'ufluc_rot')
    filtered_df = filter_outlier(filtered_df, 'ufluc_rot')
    filtered_df = filter_outlier(filtered_df, 'vfluc_rot')
    filtered_df = filter_outlier(filtered_df, 'wfluc_rot')
    filtered_df = filter_outlier(filtered_df, 'L')
    filtered_df = filter_outlier(filtered_df, 'u*')
    filtered_df = filter_outlier(filtered_df, 'ufluc_rot')
    filtered_df = filter_outlier(filtered_df, 'vfluc_rot')
    filtered_df = filter_outlier(filtered_df, 'wfluc_rot')
    filtered_df = filter_outlier(filtered_df, 'L')
    filtered_df = filter_outlier(filtered_df, 'u*')

    return filtered_df


def write_sonic(filtered_df):
    ''' writes a .dat file that GRAL uses as input file for meteorology'''

    # resample 1 minute data into 5 minute data
    res_df = filtered_df.resample('5min', label='left').mean()
    # std_df = filtered_df.resample('5min', closed='right', label='left').std()

    # std_df.iloc[3] = 0.0

    res_df['u_rot'] = res_df['u_rot']  # + std_df['u_rot']
    res_df['ufluc_rot'] = res_df['ufluc_rot']  # - std_df['ufluc_rot']
    res_df['vfluc_rot'] = res_df['vfluc_rot']  # - std_df['vfluc_rot']
    res_df['wfluc_rot'] = res_df['wfluc_rot']  # - std_df['wfluc_rot']

    res_df = res_df.round(3)

    ficout = cfg.METEO_prefix + "_sonic.dat"

    with open(ficout, 'w') as f:
        f.write('1.700\n')
        for u, wd, u_star, sigma_a, sigma_c, sigma_v, l in zip(res_df['u_rot'], res_df['wd'],
                                                               res_df['u*'], res_df['ufluc_rot'],
                                                               res_df['vfluc_rot'], res_df['wfluc_rot'],
                                                               res_df['L']):
            sp = '    '
            f.write(str(u) + sp + str(wd) + sp + str(u_star) + sp + str(sigma_a) +
                    sp + str(sigma_c) + sp + str(sigma_v) + sp + str(l) + sp +
                    '0.000' + sp + '0.000' + sp + '0.000\n')
    return


def main(filename):

    meteo_df = read_data(filename)
    rotated_df = rotated_meteo(meteo_df)
    filtered_df = filtered_meteo(rotated_df)

    write_sonic(rotated_df)

    return


# main(filename)
