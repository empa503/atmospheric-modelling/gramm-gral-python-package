"""Aster data.

https://asterweb.jpl.nasa.gov/ 

The aster data can be loaded using the function :func:`load_aster_data`
for the grid of interest. It is then saved in the GGOEM.asc file.

.. note::
    It is not clear how the data was downloaded.
    This implementation is based on the data format from the 
    existing project. 
"""
from __future__ import annotations
import logging
from math import ceil
from os import PathLike
from pathlib import Path

import numpy as np
import xarray as xr
import scipy.interpolate

import rioxarray
from rioxarray.merge import merge_arrays

from pygg.grids import GrammGrid, BasicGrid

logger = logging.getLogger("pygg.inputs.aster")

# ASTER fixed resolution (m)
ASTER_DXY = 30
# CRS of ASTER data
ASTER_CRS = 4326


def latlon_2_aster_filename(lat: float, lon: float) -> str:
    """Convert lat/lon to ASTER file name.

    Example name: ASTGTM2_N45E009_dem.tif
    """

    lonstr = "E" if lon >= 0 else "W"
    latstr = "N" if lat >= 0 else "S"

    # return "ASTGTM2_" + latstr + "{:02d}".format(lat) + lonstr + "{:03d}".format(lon) + "_dem.tif"
    return f"ASTGTM2_{latstr}{int(lat):02d}{lonstr}{int(lon):03d}_dem.tif"


def smooth_borders(aster_array: xr.DataArray, ns: int) -> xr.DataArray:
    """Smooth the borders of the ASTER data.

    .. note:: From coli. I don't know what this does in details.
        If someone knows, please add a description.

    :arg aster_array: Data array with the ASTER data.
    :arg ns: Number of cells to smooth.

    :return: Data array with smoothed borders.
    """

    # Smooth the borders

    if ns < 2:
        print("No smoothing is applied")
        return aster_array

    aster_array = aster_array.copy()

    vmin = aster_array.min().values

    # Modifying the array will modify aster_array
    array = aster_array.to_numpy()

    # Get all the values at the border of the array
    borders = np.concatenate((array[0, :], array[:, 0], array[-1, :], array[:, -1]))
    bordermin = borders.min()

    minref = bordermin - (bordermin - vmin) / (ns - 0.5) * (ns - 2)

    # Filling two border lines with minref
    array[:2, :] = minref
    array[:, :2] = minref
    array[-2:, :] = minref
    array[:, -2:] = minref

    # Interpolation on the buffering cells
    # Consecutively, E, S, N, W borders
    dz = (array[ns, :] - array[0, :]) / (ns - 2)
    for k in range(2, ns):
        array[k, :] = minref + (k - 1.5) * dz

    dz = (array[:, ns] - array[:, 0]) / (ns - 2)
    for k in range(2, ns):
        array[:, k] = minref + (k - 1.5) * dz

    dz = (array[-ns - 1, :] - array[-1, :]) / (ns - 2)
    for k in range(2, ns):
        array[-k - 1, :] = minref + (k - 1.5) * dz

    dz = (array[:, -ns - 1] - array[:, -1]) / (ns - 2)
    for k in range(2, ns):
        array[:, -k - 1] = minref + (k - 1.5) * dz

    return aster_array, minref


def _load_aster_data(
    aster_dir: PathLike, bounds: tuple[float, float, float, float]
) -> xr.DataArray:
    """Load the aster data for the bounds defined in WGS84.

    :arg aster_dir: Directory containing the ASTER data.
    :arg bounds: Bounds of the data to load in WGS84.
        (minx, miny, maxx, maxy)

    :return: Data array with the ASTER data.

    """
    aster_dir = Path(aster_dir)
    if not aster_dir.exists():
        raise FileNotFoundError(f"Directory {aster_dir} does not exist.")

    minx, miny, maxx, maxy = bounds

    aster_filenames = [
        latlon_2_aster_filename(y, x)
        for y in range(int(miny), ceil(maxy))
        for x in range(int(minx), ceil(maxx))
    ]

    # Load the aster files
    logger.info(f"Reading {aster_filenames=} from {aster_dir=}")
    aster_array = merge_arrays(
        [rioxarray.open_rasterio(aster_dir / f_name) for f_name in aster_filenames]
    )
    assert aster_array.rio.crs == ASTER_CRS

    return aster_array


def load_aster_data(aster_dir: PathLike, grid: BasicGrid) -> xr.DataArray:
    """Load ASTER data for the given grid.

    Perform interpolation from the ASTER grid to the grid.

    :arg aster_dir: Directory containing the ASTER data.
    :arg grid: Grid to load the data for.

    :returns: Data array with the ASTER data.
    """

    # Convert to the ASTER CRS
    grid_centers = grid.get_gpd_cell_centers().to_crs(ASTER_CRS)
    logger.debug(f"Grid centers: {grid_centers}")
    da_aster = _load_aster_data(aster_dir, grid_centers.total_bounds)

    aster_interp = scipy.interpolate.RectBivariateSpline(
        # Axis y is flipped, because it must be striclty increasing for scipy
        da_aster.x,
        da_aster.y[::-1],
        da_aster.T[:, ::-1],
    )

    # Interpolate the data
    aster_interp_data = aster_interp(grid_centers.x, grid_centers.y, grid=False)

    return xr.DataArray(
        aster_interp_data.reshape(grid.shape),
        coords={"x": grid.xcenter, "y": grid.ycenter},
        dims=["x", "y"],
    )
