from os import PathLike

import pandas as pd
import geopandas as gpd


def read_stations_csv(filepath: PathLike, return_LV95: bool = True) -> pd.DataFrame:
    """Read the csv file with the stations infomration."""

    df_sites = pd.read_csv(filepath, sep=";")

    # Need to convert the crs for the momemnt
    # TODO: update the crs in the code for generating this file
    locations = gpd.points_from_xy(df_sites["x"], df_sites["y"], crs="LV03")
    if return_LV95:
        location_lv95 = locations.to_crs("LV95")
        df_sites["x"] = location_lv95.x
        df_sites["y"] = location_lv95.y

    raise DeprecationWarning("This function is deprecated, use Carbosense data now.")
    df_sites['x_wind'] = df_sites['x'] + df_sites['dx']
    df_sites['y_wind'] = df_sites['y'] + df_sites['dy']
    df_sites['z_wind'] = df_sites['inlet'] + df_sites['dz']

    df_sites['x_conc'] = df_sites['x'] 
    df_sites['y_conc'] = df_sites['y']
    df_sites['z_conc'] = df_sites['inlet']


    return df_sites


if __name__ == "__main__":

    print(
        read_stations_csv(
            "/users/lconstan/gramgral/imgg/global/observationsites/all_sites.csv"
        )
    )
