"""Land use data for the GRAMM run."""
from os import PathLike
from pathlib import Path
from typing import Any
import xarray as xr
import numpy as np
# Mapping from class to values 
import pandas as pd

classes_file = Path(__file__).parent / "classes.csv"

def apply_mapping(array: np.ndarray, mapping: dict[int, Any]) -> np.ndarray:
    """Apply a mapping to an array.

    :arg array: Array to apply the mapping to.
    :arg mapping: Mapping from the array values to the new values.
    """

    out = np.full(array.shape, np.nan)
    for key, value in mapping.items():
        out[array == key] = value
    return out


def landuse_to_asc_file(landuse_data: xr.DataArray, filename: PathLike) -> None:
    """Write the landuse data to an ASC file.

    :arg array: Data array with the landuse data.
    :arg filename: Path to the ASC file.
    """

    # Generate arrays of physical paramaters (albedo, roughness, etc.)
    df = pd.read_csv(classes_file, header=1, index_col=0)



    # albedo,emissivity,Soil moisture,roughness length [m],Heat conductivity [W/m/K],Thermal conductivity [m²/s]
    heat_conductivity = apply_mapping(landuse_data, df['Heat conductivity [W/m/K]'])
    thermal_conductivity = apply_mapping(landuse_data, df['Thermal conductivity [m²/s]'])

    # Why is this so ??? 
    rhob = np.round(heat_conductivity / thermal_conductivity / 900.).astype(int)

    alambda = heat_conductivity 
    z0 = apply_mapping(landuse_data, df['roughness length [m]'])
    fw = apply_mapping(landuse_data, df['Soil moisture'])
    epsg = apply_mapping(landuse_data, df['emissivity'])
    albedo = apply_mapping(landuse_data, df['albedo'])


    # Writing the output file
    with open(filename, 'w') as f:
        for array in [rhob, alambda, z0, fw, epsg, albedo]:
            f.write(' '.join(array.astype(str).flatten(order='F')) + '\n')

    f.close()

if __name__ == "__main__":
    
    df = pd.read_csv(classes_file, header=1, index_col=0)
    print(df)
