"""Meteo input data for GRAMM."""
from dataclasses import dataclass
from enum import Enum
from pathlib import Path

import numpy as np
import pandas as pd

N_HEADERS_METEOPGT = 2

class DayTime(Enum):
    DAY = "day"
    NIGHT = "night"
    BOTH = "both"


@dataclass
class WeatherSituation:
    grammid: int
    name: str
    windmin: float = 0.0  # m/s
    windmax: float = 999.0  # m/s
    daytime: DayTime = DayTime.DAY
    # Solar radiation, Not used by the model itself but could be used
    # later when looking at the observations (in match2obs)
    solarmin: float = 0.0
    solarmax: float = 10000.0
    # vertgrad: str = "+" #  Not used


# Dictionary with all the possible classes
# This could also be done as a dataframe
# See the recommendation document for more information
# https://github.com/GralDispersionModel/GRALRecommendations
PasquillGiffordClasses = {
    "A": WeatherSituation(
        1,
        name="Extremely Unstable",
        windmax=3.0,
        solarmin=675,
    ),
    "B": WeatherSituation(
        2,
        name="Very Unstable",
        windmax=5.0,
        solarmin=175,
    ),
    "C": WeatherSituation(
        3,
        name="Unstable",
        windmin=2.0,
        solarmin=175,
    ),
    "D": WeatherSituation(
        4,
        name="Neutral",
        solarmax=925,
        daytime=DayTime.BOTH,
    ),
    "E": WeatherSituation(
        5,
        name="Stable",
        windmin=2.0,
        windmax=3.0,
        solarmax=20,
        daytime=DayTime.NIGHT,
    ),
    "F": WeatherSituation(
        6,
        name="Very Stable",
        windmax=3.0,
        solarmax=20,
        daytime=DayTime.NIGHT,
    ),
    "G": WeatherSituation(
        7,
        name="Extremely Stable",
        windmax=2.0,
        solarmax=20,
        daytime=DayTime.NIGHT,
    ),
}


def write_meteopgt_file(
    output_dir: Path,
    anemometer_height: float = 10.0,
    sector_angle: float = 9.0,
    windclasses: list[float] = [0.25, 0.75, 1.5, 2.5, 4.0, 5.5, 7.0],
) -> Path:
    """Write the meteopgt.all file.

    The meteopgt file contains all physically possible weather situations.
    It is an input data to run the GRAMM model.

    Numbers in the first line denote the anemometer height in [m],
    the way in which meteorological data is
    provided (0 = categorized; 1 = raw data), and the sector width used in the wind-direction
    categorization (only applicable, if the data represent categorized meteorological data).

    Beginning from the third line, each line represents a specific weather situation.
    The first number represents either the wind-direction sector if the second number in the first
    line is set to zero, or the observed wind-direction if the second number in the first line is set
    equal one.
    The second number in each weather situation is the wind-speed in [m/s], the third number is
    the stability class, and the fourth number is the frequency of the specific weather situation in
    [1/1000].

    Args:
        output_dir: Path to the output directory.
        wind_height: height of the wind speed ().
        sector_angle: wind sector width (angle in degrees).


    Returns:
        Path: Path to the meteopgt file.
    """
    # Creating the output file
    file_path = Path(output_dir) / "meteopgt.all"

    # Sectors
    # If you want to understand this formula, you can look in the function 
    # that reads it in the source code of GRAMM (file TEMPINT.cs):
    # WINDDIR = WINDDIR - SECTORWIDTH / 20 + SECTORWIDTH / 10 * rnd.NextDouble();
    # WINDDIR *= 10;
    # Console.WriteLine("Wind direction: " + Convert.ToString(Math.Round(WINDDIR, 0)));
    # WINDDIR = (270 - WINDDIR) * Math.PI / 180;
    list_sectors = np.around(
        (np.arange(0, 360.0, sector_angle) + 0.5 * sector_angle) / 10, 3
    )

    # Stores the lines to be written in the file
    lines = []
    for wind_speed in windclasses:
        compatible_situations = [
            situation.grammid
            for situation in PasquillGiffordClasses.values()
            if situation.windmin <= wind_speed <= situation.windmax
        ]
        lines.extend(
            [
                f"{s},{wind_speed},{c},1"
                for s in list_sectors
                for c in compatible_situations
            ]
        )

    # Check the number of weather situations
    MAX_WEATHER_SITUATIONS = 99999
    if len(lines) > MAX_WEATHER_SITUATIONS:
        raise ValueError(
            "The number of weather situations is larger"
            " than the maximum allowed by the GRAMM model."
            f" Max: {MAX_WEATHER_SITUATIONS}, Current: {len(lines)}"
            " Try to reduce the number of wind classes or increase the"
            " sector angle."
        )

    with open(file_path, "w") as file:

        # Writing the header
        file.write(
            f"{anemometer_height},0,{sector_angle},   "
            " !Are dispersion situations classified =0 or not =1\n"
            "Wind direction sector,Wind speed class,stabilty"
            " class, frequency\n"
        )

        # Writing the lines
        file.write("\n".join(lines))
        file.write("\n")

    return file_path


def read_meteopgt(file: Path) -> pd.DataFrame:
    """Read the meteopgt file.

    Also adds the solar min and max for each situation.

    Args:
        file: Path to the meteopgt file.

    Returns:
        DataFrame: Dataframe with the meteopgt file.
            The winddirection is in radians.
    """
    # Reading the file
    df = pd.read_csv(
        file,
        sep=",",
        skiprows=N_HEADERS_METEOPGT,
        names=["wind_direction", "wind_speed", "stability", "frequency"],
    )
    # Shift the indexes so they start at 1
    df.index += 1

    # Give a name to the index
    df.index.name = "situation_id"

    # The wind direction is given as sectors in the data, to convert to angle we need to
    with open(file, "r") as f:
        anemometer_height_meter, sector_type, sector_angle, _ = f.readline().split(",")
        if int(sector_type) != 0:
            raise NotImplementedError(
                "pygg currently only support categorized data in meteopgt.all. Please"
                " see GRAMM documentation for more information."
            )
    # This is how the wind direction is determined from the sector angle 
    # which is written in the meteopgt file

    # First the sector is multiplied by 10:
    # The wind direction in meteopgt is given as angle / 10, because it is multiplied by 10 in the source code (inTEMPINT.cs) )

    df['wind_direction'] *= 10
    df['wind_direction_deg'] =  df['wind_direction'] 
    
    # Then it is rotated to obtain the mathematical (and not meteorological) wind direction in rad
    df['wind_direction'] = (270 - df['wind_direction']) * np.pi / 180

    # Add solar min solar max
    # Create an empty column in the df 
    df['solarmin'] = np.nan
    df['solarmax'] = np.nan
    df['windmin'] = np.nan
    df['windmax'] = np.nan
    df['stability_letter'] = np.nan
    for key, pg_class in PasquillGiffordClasses.items():
        df.loc[df['stability'] == pg_class.grammid, 'solarmin'] = pg_class.solarmin
        df.loc[df['stability'] == pg_class.grammid, 'solarmax'] = pg_class.solarmax
        df.loc[df['stability'] == pg_class.grammid, 'windmin'] = pg_class.windmin
        df.loc[df['stability'] == pg_class.grammid, 'windmax'] = pg_class.windmax
        df.loc[df['stability'] == pg_class.grammid, 'stability_letter'] = key
    
    return df


if __name__ == "__main__":


    f = Path('test_meteopgt')
    f.mkdir(exist_ok=True)
    pgt_file = write_meteopgt_file(f, sector_angle=9)
    df_meteopgt = read_meteopgt(pgt_file)

    import matplotlib.pyplot as plt
    plt.figure()
    plt.scatter(
        df_meteopgt["wind_speed"] * np.cos(df_meteopgt["wind_direction"]),
        df_meteopgt["wind_speed"] * np.sin(df_meteopgt["wind_direction"]),
    )
    #plt.title("Mean of all stations")
    plt.title("Meteopgt winds")
    plt.show()
