"""Downloading street data from openstreetmap."""


import logging
from pathlib import Path
import overpy
import fiona
from fiona.crs import from_string
from pygg.grids import BasicGrid, WGS84


logger = logging.getLogger("pygg.inputs.streetmap")


def write_shape(
    apiresult: overpy.Result,
    shape_file: Path,
) -> Path:
    """Returns a shape file that contains the buildings in the chosen domain

    Parameters
    ----------
    apiresult: str
        returned object from doing an inquiry using the overpass API
    shp_name: stt
        name of the shapefile. Do NOT INCLUDE '.shp' extension (e.g. 'foo').
    """
    schema = {
        "geometry": "Polygon",
        "properties": {
            "OGID": "str:17",
            "GID": "str:24",
            "EGID": "int:9",
            "ART": "str:20",
            "STATUS": "str:20",
            "REGION": "str:10",
            "H0": "float:5.1",
            "H1": "float:5.1",
            "H2": "float:5.1",
            "HRELMIN": "float:5.1",
            "HRELMAX": "float:5.1",
        },
    }

    shape_file = shape_file.with_suffix(".shp")

    with fiona.open(
        shape_file, "w", crs=WGS84, driver="ESRI Shapefile", schema=schema
    ) as output:
        for way in apiresult.ways:

            build = {
                "type": "Polygon",
                "coordinates": [[(node.lon, node.lat) for node in way.nodes]],
            }
            logger.debug(str(way.tags))

            # Note: by looking at the tags, we could use 'building:levels'
            # and convert it to height.

            prop = {
                "OGID": way.tags.get("ogid"),
                "GID": way.tags.get("gid"),
                "EGID": way.tags.get("egid"),
                "ART": way.tags.get("art"),
                "STATUS": way.tags.get("status"),
                "REGION": way.tags.get("region"),
                "H0": way.tags.get("h0"),
                "H1": way.tags.get("h1"),
                "H2": way.tags.get("h2"),
                "HRELMIN": way.tags.get("height", float(5.0)),
                "HRELMAX": way.tags.get("height", float(5.0)),
            }

            output.write({"geometry": build, "properties": prop})

    return shape_file


def download_street_data(
    shape_file: Path,
    grid: BasicGrid,
):
    """Get street data from openstreetmap.

    Returns an object from an inquiry using the overpass API.


    A query string used by the overpassAPI to read OSM files. It follows the
    format provided by 'https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide'

    e.g. ::

        (node["building"](52.72, 4.69,52.73,4.70);
        way["building"](52.72, 4.69,52.73,4.70);
        relation["building"](52.72, 4.69,52.73,4.70););
        (._;>;);
        out body;

    way = the OSM component (either node, way, or relative)
    (52.72, 4.69,52.73,4.70) = bounding box in (south, west, north, east) GPS coordinates
    out body; = used to close the query

    .. note:: If the query is too large, the  overpass API will return an error.
        We might need to implement some kind of splitting of the domain in the future.
    """

    # Get the bounds and convert to  WGS coordinates
    bounds = grid.get_gpd_mesh().to_crs(WGS84).total_bounds
    bounds_str = f"({bounds[1]}, {bounds[0]},{bounds[3]}, {bounds[2]})"

    # Querry to get the buildings from open stree map
    query_str = "\n".join(
        (
            f'(node["building"] {bounds_str};',
            f'way["building"]{bounds_str};',
            f'relation["building"]{bounds_str};);',
            "(._;>;);",
            "out body;",
        )
    )

    logger.info("Querying openstreetmap")
    logger.debug(f"{query_str=}")

    overpy_api = overpy.Overpass()

    result = overpy_api.query(query_str)

    write_shape(result, shape_file)

    return shape_file
