"""Corine is the data set for landuse.

.. note:: In the documentation, one can find some physical parameters for the
    different landuse classes. If not specified, gramm doc says that they
    use the default.
    Other wise it creates a LANDUSE.asc file with the parameters.
    If one would like to change these categories, we should implement 
    an approach based on a pandas dataframe containing the default values.
    Lists of parameters where used previously, but this is not intuitive.
    One could create a nice csv from which the pandas dataframe is generated.
"""
from os import PathLike

import rioxarray
import scipy.interpolate
import xarray as xr

from pygg.grids import BasicGrid


def code_ids_corine_2_gramm(codes: xr.DataArray) -> xr.DataArray:
    """Convert the Corine landuse codes to the GRAMM codes.

    :arg codes: Data array with the Corine codes.

    :return: Data array with the GRAMM codes.
    """

    # This is the hardcoded way to convert the codes.
    corine_classes = range(0, 45)
    gramm_classes = [
        -9999,
        111,
        112,
        121,
        122,
        123,
        124,
        131,
        132,
        133,
        141,
        142,
        211,
        212,
        213,
        221,
        222,
        223,
        231,
        241,
        242,
        243,
        244,
        311,
        312,
        313,
        321,
        322,
        323,
        324,
        331,
        332,
        333,
        334,
        335,
        411,
        412,
        421,
        422,
        423,
        511,
        512,
        521,
        522,
        523,
    ]

    for c, g in zip(corine_classes, gramm_classes):
        codes = codes.where(codes != c, g)

    return codes


def load_corine_data(corine_tif: PathLike, grid: BasicGrid) -> xr.DataArray:
    """Load Corine classes for the given grid.

    Perform interpolation from the Corine grid to the grid.

    .. note:: The codes are not converted to the GRAMM codes.
        You have to use :py:func:`code_ids_corine_2_gramm` to convert.

    :arg corine_tif: Path to the Corine tif file.
    :arg grid: Grid to load the data for.

    :returns: Data array with the Corine data.
    """

    corine = rioxarray.open_rasterio(corine_tif)

    # Convert centers to the corine CRS
    centers = grid.get_gpd_cell_centers().to_crs(corine.rio.crs)

    # Select the data for the grid
    arr = corine.rio.clip_box(*grid.bounds, crs=grid.crs)

    # Interpolate the data
    corine_interp = scipy.interpolate.interpn(
        (arr.x, arr.y),
        # Select the band 1 (this is the only band)
        arr.sel(band=1).T.to_numpy(),
        (centers.x, centers.y),
        method="nearest",
        bounds_error=False,
    )

    return code_ids_corine_2_gramm(
        xr.DataArray(
            corine_interp.reshape(grid.shape),
            dims=("x", "y"),
            coords={"x": grid.xcenter, "y": grid.ycenter},
        )
    )
