"""The buildings are required for the GRAMM model.

Building data is usually quite huge.
Therefore we proceed in two steps:
1. Download the building data from the an api 
2. Rasterizing the raw data to the simulation raster

There is also the solution to use data which is already prepared as a tif file.
-> Choosen option for pygg, as api has bad data.

.. warning:: One might have to remove the bridges from the building data.
    The issue is that otherwise the air will not be able to flow 
    under the bridges during the simulation.
"""

# Note: the code of this file was adapted already by Gerrit.
# Then coli replaced some parts.

from os import PathLike
from pathlib import Path

import geopandas as gpd
import numpy as np
import rasterio
from rasterio import features
from rasterio.transform import from_bounds


from pygg.grids import BasicGrid


def shapefile_to_grid(
    grid: BasicGrid,
    input_filename: PathLike,
) -> np.ndarray:
    """Convert building data from shape file to a raster on the grid.

    The shape file must contain the building information in the following columns:
    - HRELMIN: minimum height of the building
    - HRELMAX: maximum height of the building

    Parameters
    ----------
    grid:
        The grid on which the buildings are rasterized
    input_filename:
        The path to the shape file

    Returns
    -------
    heights
        The rasterized buildings heights


    """
    # Read the file
    gdf: gpd.GeoDataFrame = gpd.read_file(input_filename)

    # Project to the grid
    gdf = gdf.to_crs(grid.crs)

    # Raster shape
    out_shape = grid.shape

    # Arguments for the rasterization
    kwargs = dict(
        out_shape=out_shape, fill=0, transform=from_bounds(*grid.bounds, *out_shape)
    )

    # We do an average from the minimum and maximum heights of each shape
    burned_min = features.rasterize(shapes=zip(gdf.geometry, gdf["HRELMIN"]), **kwargs)
    burned_max = features.rasterize(shapes=zip(gdf.geometry, gdf["HRELMAX"]), **kwargs)
    return (burned_max + burned_min) / 2


def prepare_raster_buildings(
    grid: BasicGrid, input_filename: PathLike, filepath: PathLike, mode="a"
):
    """
    Prepare GRAL building file from GEO tiff (as provided by the canton of Zurich)
    """
    # Project building raster to GRAL grid
    src = rasterio.open(input_filename)
    dst = np.zeros((grid.ny, grid.nx))
    rasterio.warp.reproject(
        src.read(),
        dst,
        src_crs=src.crs,
        src_transform=src.transform,
        dst_crs=grid.crs,
        dst_transform=grid.get_transform(),
        dst_resolution=(grid.dy, grid.dx),
    )

    # Flip up-down, mask and round
    heigths = np.flipud(dst)
    heigths = np.transpose(heigths)

    heigths[heigths == 0] = np.nan
    heigths = np.round(heigths.astype(np.float), 1)

    # Writing the output raster for GRAL
    write_building_heights(grid, heigths, filepath, mode=mode)

    return heigths


def read_building_heights(filename, grid, mode="array") -> np.ndarray:
    """Read the file with the building heights for GRAL.
    
    The file contains each building, with their position and 
    their height. 
    You can load the data, as an array or as a list of buildings.
    this can be selected by specifying the parameter "mode"
    """
    x, y, _, h = np.loadtxt(filename, delimiter=",", unpack=True)

    if mode == "points":
        return x, y, h

    # Keeping only building within the GRAL domain
    mask = (x >= grid.xmin) & (x <= grid.xmax)
    mask &= (y >= grid.ymin) & (y <= grid.ymax)

    i, j = grid.get_index(x[mask], y[mask])

    # Filling a building array with building heights
    building_heights = np.full(grid.shape, np.nan, dtype=float)
    building_heights[i, j] = h[mask]

    return building_heights


def write_building_heights(
    grid: BasicGrid, heigths: np.ndarray, filepath: PathLike, mode="w"
) -> Path:
    """Save the heights of the buildings as the GRAL input file.

    It is used to define grid cells in the microscale model of GRAL, which are blocked. Each grid
    cell containing one (or more) of the listed coordinates in building.dat is blocked and treated
    as obstacle.
    
    The structure of the file is as follows:
    1st column: x-coordinate in [m]
    2nd column: y-coordinate in [m]
    3rd column: lower z-coordinate in [m] (currently not used; buildings reach always the
    ground, therefore, structures such as bridges cannot be modelled)
    4th column: height in [m] of a building. All grid cells up to this height are blocked. The
    height of the grid-cell centre determines whether a grid cell is blocked or not.
    """



    # Writing the output raster for GRAMM/GRAL
    xmesh, ymesh = grid.get_center_mesh()
    mask = np.isfinite(heigths)

    values = np.transpose(
        [xmesh[mask], ymesh[mask], np.zeros(mask.sum()), heigths[mask]]
    )

    filepath = Path(filepath)
    if filepath.is_dir():
        filepath = filepath / "buildings.dat"
    filepath.parent.mkdir(exist_ok=True, parents=True)

    with open(filepath, mode) as out_file:
        np.savetxt(out_file, values, fmt="%s", delimiter=", ")

    return filepath
