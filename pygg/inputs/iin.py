"""IIN.dat file from GRAMM.

IIN.dat is the main control file for the GRAMM execution. It is in ASCII file format.

In our use cases, the simulation is driven by meteorological data stored
in meteopgt.all . This makes some of the parameters in IIN.dat irrelevant.
"""

from os import PathLike
from pathlib import Path


def write_iindat_file(
    path: PathLike,
    latitude: int,
    max_allowed_time_step: int = 5,
    modelling_time: int = 3600,
    relaxation_velocity: float = 0.1,
    relaxation_scalars: float = 0.1,
):
    """Create the 'IIN.dat' file.

    The parameters that can be set in this file are described in the GRAMM
    model. The description is copied here for convenience.

    :param path: Path to the directory where the file should be written.
    :param latitude: [deg] Latitude of the model domain. Influences the Coriolis force.
        Negative values are eligible
    :param max_allowed_time_step: [s] Each simulation starts with the minimum time
        step of 1.5 s. It increases in dependency on
        the convergence of the model up to the limit
        set here. If the upper limit set here is too high,
        the model might become numerically
        unstable. Guideance on reasonable upper
        limits are given in the GRAL recommendation
        guide.
    :param modelling_time: [s] Total modelling time in seconds.
        (for values >1(s) and <1[%])
        Note that if
        GRAMM is driven by meteopgt.all the
        modelling time is automatically adjusted by
        GRAMM in dependence on the stability class:
        in neutral (SC 4) conditions it is fixed with
        600s, in convective (SC 1-3) conditions the
        modelling time is halfed, and in stable (SC 5-
        7) conditions it is doubled. If the modelling
        time is set to values below 1, it is
        automatically interpreted as percentage of the
        time it takes the flow for the given wind speed
        to pass the given percentage of the model
        domain diagonally. If the value is above 1, the
        modelling time is taken as seconds.
    :param relaxation_velocity: Value between 0 and 1. Applied for the three
        wind components. The lower the value the
        better the numerical stability, but the lower
        the rate of convergence. Recommended
        values can be found in the GRAL
        recommendation guide.
    :param relaxation_scalars: Value between 0 and 1. Applied for
        temperature, humidity, turbulent kinetic
        energy, and dissipation. The lower the value
        the better the numerical stability, but the
        lower the rate of convergence.
        Recommended values can be found in the
        GRAL recommendation guide.
    """

    path = Path(path)
    filepath = path / "IIN.dat"

    if max_allowed_time_step < 1 or max_allowed_time_step > 100:
        raise ValueError(
            "max_allowed_time_step should be between 1 and 100. "
            f"Got {max_allowed_time_step}"
        )
    
    if modelling_time <= 0:
        raise ValueError(
            "modelling_time should be greater than 0. "
            f"Got {modelling_time}"
        )
    
    if relaxation_velocity < 0.01 or relaxation_velocity > 1:
        raise ValueError(
            "relaxation_velocity should be between 0.01 and 1. "
            f"Got {relaxation_velocity}"
        )
    
    if relaxation_scalars < 0.01 or relaxation_scalars > 1:
        raise ValueError(
            "relaxation_scalars should be between 0.01 and 1. "
            f"Got {relaxation_scalars}"
        )

    parameters = [
        "COMPUTATION DATE         (YYMMDD)             :  080210               !No influence",
        "BEGINNING OF COMPUTATION   (hhmm)             :  0000                 !No influence",
        f"MAXIMUM ALLOWED TIME STEP DT  [ s ]           :  {max_allowed_time_step}                   !Range 1-100",
        f"MODELLING TIME (FOR VALUES >1(s) AND <1[%])   :  {modelling_time}                 !Range 0.01-1% or 2-infinite sec.",
        "BUFFERING AFTER TIMESTEPS                     :  10                   !No influence",
        "MAX. PERMISSIBLE W-DEVIATION ABOVE < 1 [mm/s] :  0.01                 !Not used",
        "RELATIVE INITIAL HUMIDITY  [ % ] GT.0         :  0.5                  !No influence",
        "HEIGHT OF THE LOWEST COMPUTATION HEIGHT [ m ] :  330.                 !Not used",
        "AIR TEMPERATURE AT GROUND  [ K ]              :  283.0                !No influence",
        "GRADIENT OF THE POTENTIAL TEMPERATURE [K/100m]:  0.01                 !Not used",
        "NEUTRAL LAYERING UP TO THE HEIGHT ABOVE GROUND:  10000                !Not used",
        "SURFACE TEMPERATURE [ K ]                     :  283.0                !No influence",
        "TEMPERATURE OF THE SOIL IN 1 M DEPTH  [ K ]   :  283.0                !No influence",
        f"LATITIUDE                                     :  {latitude}                   !Range -90 - + 90deg ",
        "UPDATE OF RADIATION (TIMESTEPS)               :  10000                !Should not be changed",
        "DEBUG LEVEL 0 none, 3 highest                 :  0                    !0 or 1 recommended",
        "COMPUTE  U V W PN T GW FO            YES=1    :  1111100              !Should not be changed",
        "COMPUTE  BR PR QU PSI TE TB STR      YES=1    :  1110111              !Should not be changed",
        "DIAGNOSTIC INITIAL CONDITIONS        YES=1    :  1                    !Should not be changed",
        "EXPLICIT/IMPLICIT TIME INTEGRATION  I=1/E=0   :  1                    !Should not be changed",
        f"RELAXATION VELOCITY                           :  {relaxation_velocity}                  !Range 0.01-1.0",
        f"RELAXATION SCALARS                            :  {relaxation_scalars}                  !Range 0.01-1.0",
        "Force catab flows -40/-25/-15 W/m2 AKLA 7/6/5 :  0                    !1=ON suitable with steady state meteo.all",
        "NESTING (=1) OR LARGE SCALE FORCING    (=0)   :  0                    !Should not be changed",
        "BOUNDARY CONDITION (1,2,3,4,5,6)              :  6                    !6 = nudging",
        "NON-STEADYSTATE/FORCING W. 'METEO.ALL' YES=1  :  0                    !Should not be changed",
        "NON-STEADYSTATE/FORCING W. IFU-MM5 DATA YES=1 :  0                    !Not Used",
        "GRAMM IN GRAMM NESTING YES=1                  :  0                    !Not Used",
        "Flowfield Output Format                       :  0                    !0 ascii, 1, binary, 2 0&1 3 stream IOUT for asc/bin/stream flowfields",
    ]

    with open(filepath, "w") as file:
        file.write('\n'.join(parameters))

    return filepath
