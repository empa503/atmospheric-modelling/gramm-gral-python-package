"""GRAMMin.dat file creation.

From the GRAMM documentation:

The first line provides information about the model version and is not used further.

The letter in the second line defines whether the GRAMM model is driven by categorized
meteorological data as stored in the file meteopgt.all (“y”) or if the model is initialized by
specific meteorological observations (“n”) (point- and profile measurements of wind and
temperature). More information about the two ways to initialize the model is provided later in
this document

The number in the third line is the roughness length in [m]

The  first number in the fourth
line defines the weather situation to start with (only applicable and used if meteopgt.all is
used for providing meteorogical input data), while the second number in the fourth line
provides information about the number of cells at the lateral boundaries over which
orographical data is smoothed. This information is not used in GRAMM.


Line 5 defines whether files named “xxxxx_steady_state.txt“ will be written by GRAMM at the
end of each simulation. These files contain information about the fulfilment of the steady-
state requirement of the VDI guideline 3783-7 (see chapt. 5 and for further descriptions of
these files chapt. 9.4.2.5).

Line 6 triggers the storage of intermediate flow-field files to be used in the match-to-
observation algorithm in the Graphical User Interface. If larger than zero, intermediate files
will be written. Furthermore, global radiation is computed transient.
"""

from os import PathLike
from pathlib import Path


def write_grammindat_file(
    path: PathLike,
    weather_situation_id: int,
    roughness: float = 0.2,
    version: float = 19.01,
) -> Path:
    """Create the 'grammin.dat' file.

    :param path: Path to the directory where the file should be written.
    :param weather_situation_id: The id of the weather situation to use.
    :param roughness: The roughness length in [m].
    :param version: The version of the model.

    :return: The path to the created file.
    """
    path = Path(path)
    filepath = path / "GRAMMin.dat"

    with open(filepath, "w") as file:
        # The first line provides information about the model version and is not used further
        file.write(f"Version {version}\n")
        #
        file.write("y\n")
        file.write(f"{roughness}\n")
        file.write(f"{weather_situation_id}, 0\n")
        file.write("no     ! write steady-state\n")
        file.write("0")

    return filepath
