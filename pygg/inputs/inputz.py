"""Inputz data for meteo.

This file was imported from the previous gg package and was not correctly converted.
It is not used at the moment.
"""
import pandas as pd
import numpy as np

import gg.globals as cfg

def read_data(filename):
    ''' Read the 1 minute csv file into a dataframe '''

    meteo_df = pd.read_csv(filename, delimiter=';',
                           header=0, index_col='datetime')
    meteo_df.index = pd.to_datetime(meteo_df.index)

    return meteo_df


def rotation_one(alpha):
    ''' Rotation about the vertical axis '''

    R1 = np.array([[np.cos(alpha), np.sin(alpha), 0],
                   [-np.sin(alpha), np.cos(alpha), 0],
                   [0, 0, 1]])

    return R1


def rotation_two(beta):
    ''' Rotation about the new horizontal axis '''

    R2 = np.array([[np.cos(beta), 0, np.sin(beta)],
                   [0, 1, 0],
                   [-np.sin(beta), 0, np.cos(beta)]])

    return R2


def rotation_three(gamma):
    ''' Rotation for the conservation of momentum flux '''

    R3 = np.array([[1, 0, 0],
                   [0, np.cos(gamma), np.sin(gamma)],
                   [0, -np.sin(gamma), np.cos(gamma)]])

    return R3


def rot_angles(meteo_df):
    '''
    '''

    all_alpha = []
    all_beta = []

    for U, V, W in zip(meteo_df['U'], meteo_df['V'], meteo_df['W']):
        alpha = np.arctan(V / U)
        all_alpha.append(alpha)

        R1 = rotation_one(alpha)
        vel0 = np.array([U, V, W])

        u1, v1, w1 = np.dot(R1, vel0)

        beta = np.arctan(w1 / u1)
        all_beta.append(beta)

    return all_alpha, all_beta


def fluc_transform(meteo_df, alpha, beta):
    ''' Transform the flux into streamwise coordinates '''

    horizon_fluc, wfluc_rot = [], []

    for A, B, UFluc, VFluc, WFluc in zip(alpha, beta, meteo_df['Ufluc'], meteo_df['Vfluc'], meteo_df['Wfluc']):

        R1 = rotation_one(A)
        fluc0 = np.array([UFluc, VFluc, WFluc])

        ufluc1, vfluc1, wfluc1 = np.dot(R1, fluc0)

        R2 = rotation_two(B)

        fluc2 = np.array([ufluc1, vfluc1, wfluc1])

        ufluc2, vfluc2, wfluc2 = np.dot(R2, fluc2)
        hfluc = np.sqrt((ufluc2**2 + vfluc2**2) / 2)

        horizon_fluc.append(hfluc)
        wfluc_rot.append(wfluc2)

    return horizon_fluc, wfluc_rot


# def horizon_fluc(meteo_df):
#     u_fluc_list, v_fluc_list, w_fluc_list = [], [], []

#     for u_fluc, v_fluc, w_fluc in zip(meteo_df['Ufluc'], meteo_df['Vfluc'], meteo_df['Wfluc']):

#         alpha = np.arctan(v_fluc / u_fluc)

#         R1 = rotation_one(alpha)
#         fluc0 = np.array([u_fluc, v_fluc, w_fluc])

#         u_fluc1, v_fluc1, w_fluc1 = np.dot(R1, fluc0)

#         beta = np.arctan(w_fluc1 / u_fluc1)
#         R2 = rotation_two(beta)

#         fluc1 = np.array([u_fluc1, v_fluc1, w_fluc1])

#         u_fluc2, v_fluc2, w_fluc2 = np.dot(R2, fluc1)
#         u_fluc_list.append(u_fluc2)
#         v_fluc_list.append(v_fluc2)
#         w_fluc_list.append(w_fluc2)

#     return u_fluc_list, v_fluc_list, w_fluc_list


def component_meteo(meteo_df):
    ''' 5 minute average component value '''

    meteo_df['V'] = meteo_df['V'] * -1
    meteo_df = meteo_df[meteo_df.index > '2018-10-02 12:25:00']

    u_comp, v_comp, w_comp = [], [], []
    for U, V, W in zip(meteo_df['U'], meteo_df['V'], meteo_df['W']):

        R1 = rotation_one(np.deg2rad(60.04))
        vel0 = np.array([U, V, W])

        u1, v1, w1 = np.dot(R1, vel0)

        u_comp.append(u1)
        v_comp.append(v1)
        w_comp.append(w1)

    alpha, beta = rot_angles(meteo_df)
    horizon_comp, wfluc_comp = fluc_transform(meteo_df, alpha, beta)

    component_df = pd.DataFrame(index=meteo_df.index)

    component_df['u_comp'] = u_comp
    component_df['v_comp'] = v_comp
    component_df['w_comp'] = w_comp
    component_df['horizon_fluc'] = horizon_comp
    # component_df['vfluc_comp'] = vfluc_comp
    component_df['wfluc_comp'] = wfluc_comp
    component_df['u*'] = meteo_df['U*']
    component_df['L'] = meteo_df['L']

    return component_df


def filter_outlier(component_df, field):

    q1 = component_df[field].quantile(0.25)
    q3 = component_df[field].quantile(0.75)

    iqr = q3 - q1
    whisk = iqr * 1.5

    up_whisk = q3 + whisk
    low_whisk = q1 - whisk

    filtered_df = component_df[component_df[field] < up_whisk]
    filtered_df = filtered_df[filtered_df[field] > low_whisk]

    return filtered_df


def filtered_meteo(component_df):

    filtered_df = filter_outlier(component_df, 'horizon_fluc')
    filtered_df = filter_outlier(filtered_df, 'L')
    filtered_df = filter_outlier(filtered_df, 'u*')
    filtered_df = filter_outlier(filtered_df, 'horizon_fluc')
    filtered_df = filter_outlier(filtered_df, 'L')
    filtered_df = filter_outlier(filtered_df, 'u*')
    filtered_df = filter_outlier(filtered_df, 'horizon_fluc')
    filtered_df = filter_outlier(filtered_df, 'L')
    filtered_df = filter_outlier(filtered_df, 'u*')

    return filtered_df


def write_inputz(filtered_df):
    ''' writes inputzr.dat that GRAL uses as an input file for the meteorology '''

    # resample 1 minute component meteo data into 5 minute data
    res_df = filtered_df.resample('1min', closed='right', label='left').mean()
    std_df = filtered_df.resample('1min', closed='right', label='left').std()

    # std_df.iloc[4] = 0

    res_df['u_comp'] = res_df['u_comp'] # + std_df['u_comp']
    res_df['v_comp'] = res_df['v_comp'] # + std_df['v_comp']
    res_df['horizon_fluc'] = res_df['horizon_fluc'] # - std_df['horizon_fluc']

    res_df = res_df.round(3)

    met_no = range(1, len(res_df) + 1)

    ficout = cfg.METEO_prefix + "_inputzr.dat"

    with open(ficout, 'w') as f:
        f.write('1\n')
        f.write('1.7\n')
        for i, u_star, l, fluc, u, v in zip(met_no, res_df['u*'], res_df['L'],
                                            res_df['horizon_fluc'], res_df['u_comp'],
                                            res_df['v_comp']):
            sp = '    '
            f.write(str(i) + sp + str(u_star) + sp + str(l) + sp + str(-1) +
                    sp + str(fluc) + sp + str(u) + sp + str(v) + '\n')

    return


def main(filename):

    meteo_df = read_data(filename)
    component_df = component_meteo(meteo_df)
    filtered_df = filtered_meteo(component_df)
    write_inputz(filtered_df)

    return
