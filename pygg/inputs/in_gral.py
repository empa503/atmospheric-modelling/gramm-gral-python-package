"""The in.dat file from GRAL.

It defines the main control parameters to run GRAL.
Each line is a parameter.
"""
from __future__ import annotations
from collections import namedtuple

from dataclasses import dataclass, field
from os import PathLike
from pathlib import Path
import textwrap

import numpy as np


@dataclass
class GralSimulationParameters:
    """Parameters for a GRAL simulation.

    These parameters are shared among all the grall subsimulations.

    Makes it easier to keep track of the parameters used for a simulation.

    Please check the official GRAL documentation for more information

    :param n_particles: Numbers of released particles per second
    :param dispersion_time: Dispersion time in [s].
        The shorter the dispersion time the smaller the
        horizontal standard deviations of wind speed
    :param steady_state: If True, the simulation will be run in steady state mode.
        Otherwise, it will be run in transient mode.
    :param plume_meandering: Meandering Effect Off=J/On=N. N is recommended.
    :param slices: Height of the horizontal slices in [m] above ground level.
    :param slicethick: Vertical extension of the concentration grid.
        Together with horizontal grid size
        it defines the volume size of the concentration grid
    :param building_method: Flag indicating the method to take buildings into account
        0. No buildings
        1. diagnostic approach
        2. prognostic approach

        the second number determines
        the number of cells around obstacles, where the prognostic equations are
        applied.
    :param building_roughness: Roughness of the buildings in [m]
    :param micro_vert_layers: Number of vertical layers used in the prognostic
        microscale flow field model. Note that above this height still
        wind fields are computed by utilizing a diagnostic approach.
    :param relaxation_factor_velocity: Relaxation factor for the velocity
        in the prognostic microscale flow field model.
    :param relaxation_factor_pressure: Relaxation factor for the pressure
        in the prognostic microscale flow field model.
    :param turbulence_model: Number of tubulence model to use.
        0: no diffusion
        1: mixing-length model (invoked when the file “turbulence_model.txt” is not existent.
        2: standard k-ε model
    :param integration_time_min: minimum number of iterations for the
        solution algorithm in the prognostic microscale flow field model
    :param integration_time_max: maximum number of iterations for the
        solution algorithm in the prognostic microscale flow field model
    """

    n_particles: int = 666
    dispersion_time: int = 3600
    plume_meandering: str = "N"
    slices: list[int] = field(
        default_factory=lambda: [2, 5, 10, 20, 30, 45, 60, 75, 100]
    )
    slicethick: float = 1.0
    building_method: int = 2

    # The following parameters are used for other files
    building_roughness: float = 0.001
    micro_vert_layers: int = 40
    relaxation_factor_velocity: float = 0.1
    relaxation_factor_pressure: float = 1.0
    turbulence_model: int = 1
    integration_time_min: float = 100
    integration_time_max: float = 500

    # Transient mode parameters
    steady_state: bool = True
    transient_conc_threshold: float = 0.001
    transient_keep_temporary_files: bool = False
    transient_n_hours: int = 24
    transient_situations: list[int]|None = None
    emissions_timeseries: str|None = None

    def __post_init__(self):

        # Check the possible values for the turbulence model
        if self.turbulence_model not in [0, 1, 2]:
            raise ValueError(
                f"Turbulence model {self.turbulence_model} is not supported, only 0, 1"
                " and 2 are supported."
            )

        if self.integration_time_min >= self.integration_time_max:
            raise ValueError(
                f"Integration time min ({self.integration_time_min}) must be smaller"
                f" than integration time max ({self.integration_time_max})."
            )

        if self.plume_meandering not in ["J", "N"]:
            raise ValueError(
                f"Plume meandering '{self.plume_meandering}' is not supported, only 'J'"
                " and 'N' are supported."
            )


def write_indat_file(
    run_dir: PathLike,
    latitude: float,
    weather_situation_start: int,
    simulation_parameters: GralSimulationParameters,
    receptor_points: bool = False,
) -> Path:
    """Create the 'in.dat' file.

    For choosing these parameters one should check the recommendations from
    `https://github.com/GralDispersionModel/GRALRecommendations`_ .

    :param run_dir: Path to the directory where the file should be written.
    :param latitude: Latitude of the simulation area.
    :param weather_situation_start:The number of the weather situation
        from which onward the simulation starts



    """
    run_dir = Path(run_dir)
    in_file = run_dir / "in.dat"

    if not simulation_parameters.steady_state:
        # Steady state does not use catalogue, start from 1
        weather_situation_start = 1 

    content = [
        (simulation_parameters.n_particles, "Particle Number"),
        (simulation_parameters.dispersion_time, "Dispersion Time"),
        (1 if simulation_parameters.steady_state else 0, "Steady State"),
        (
            4,
            (
                "Meteorology:"
                " inputzr.dat=0,meteo.all=1,elimaeki.prn=2,SONIC.dat=3,meteopgt.all=4"
            ),
        ),
        ((1 if receptor_points else 0), "Receptor Points: Yes=1"),
        (
            0.2,
            (
                "Surface Roughness in [m], not used as coupled with GRAMM with"
                " landuse.asc file"
            ),
        ),
        (latitude, "Latitude"),
        (
            simulation_parameters.plume_meandering,
            "Meandering Effect Off=J/On=N. N is recommended.",
        ),
        ("UNUSED", "Pollutant: NOx,CO,PM,HC, now defined in polluant.txt"),
        (
            ",".join(np.array(simulation_parameters.slices).astype(str)),
            (
                "Horizontal slices [m] seperated by a comma (number of slices has to be"
                " defined in GRAL.geb!)"
            ),
        ),
        (f"{simulation_parameters.slicethick:.1f}", "Vertical grid spacing in [m]"),
        (weather_situation_start, "Weather Situation Start"),
        (simulation_parameters.building_method, "Building Method"),
        (
            -2,
            (
                "Flag determining the output format of the concentration files (*.con"
                " files). This value should be 0 or -2. GRAL writes the file"
                " “building_heights.txt” if this flag is set to -2. The value 1 is"
                " reserved for a Soundplan output format."
            ),
        ),
        ("normal", "Output format of the concentration files (*.con files)"),
        ("nokeystroke", "Specify the programm should not wait for a keystroke"),

    ]

    str_content = "\n".join([f"{value}\t!{comment}" for value, comment in content])

    # Apparently we don't need to add the lines after that.
    with open(in_file, "w") as f:
        f.write(str_content.expandtabs(30))

    return in_file
