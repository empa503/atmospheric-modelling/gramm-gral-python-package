"""Grids for GRAL and GRAMM models.

TODO: some cleaning.
"""
from __future__ import annotations
import logging
import os
import textwrap
from pathlib import Path
from functools import cached_property

import numpy as np
from shapely.geometry.polygon import Polygon
import xarray as xr
import rasterio
import rioxarray
import geopandas as gpd
import shapely.geometry as geom


LV95 = "EPSG:2056"
WGS84 = "EPSG:4326"


class BasicGrid:
    """Basic grid class.

    Other classes should inherit from this class.
    Provides basic properties and methods for all grids.
    """

    xmin: float
    xmax: float
    ymin: float
    ymax: float

    # Size of a cell
    dx: float
    dy: float
    # Area of a cell
    dxy: float

    nx: int
    ny: int

    # Vertical grids generation
    dz0: float  # thickness of surface layer [m]
    ddz: float  #  stretching factor for vertical layers

    xcorner: np.ndarray
    ycorner: np.ndarray

    logger: logging.Logger

    def __init__(
        self,
        xmin: float,
        xmax: float,
        ymin: float,
        ymax: float,
        nx: int,
        ny: int,
        crs: int | str = LV95,
    ):
        self.xmin, self.xmax = xmin, xmax
        self.ymin, self.ymax = ymin, ymax

        self.nx, self.ny = nx, ny
        self.dx = (self.xmax - self.xmin) / self.nx
        self.dy = (self.ymax - self.ymin) / self.ny
        self.dxy = self.dx * self.dy

        self.xcorner = np.linspace(self.xmin, self.xmax, self.nx + 1)
        self.ycorner = np.linspace(self.ymin, self.ymax, self.ny + 1)

        self.shape = self.nx, self.ny

        self._latitude = None

        self.crs = crs

        self.logger = logging.getLogger(f"pygg.grids.{self.__class__.__name__}")

    def __repr__(self) -> str:
        return textwrap.dedent(
            f"""\
            {self.__class__.__name__}:
                - x: {self.xmin} - {self.xmax} [m]
                - y: {self.ymin} - {self.ymax} [m]
                - dx: {self.dx} [m]
                - dy: {self.dy} [m]
                - nx: {self.nx}
                - ny: {self.ny}
                - crs: {self.crs}
            """
        )



    @property
    def latitude(self) -> float:
        """Latitude of the grid."""
        if self._latitude is None:
            # Calulate the average latitude of the domain
            sereie = gpd.points_from_xy(
                [(self.xmin + self.xmax) / 2],
                [(self.ymin + self.ymax) / 2],
                crs=self.crs,
            )
            self._latitude = sereie.to_crs(WGS84).y[0]
        return self._latitude

    @cached_property
    def xcenter(self):
        return self.xcorner[:-1] + 0.5 * self.dx

    @cached_property
    def ycenter(self):
        return self.ycorner[:-1] + 0.5 * self.dy

    @cached_property
    def bounds(self):
        return self.xmin, self.ymin, self.xmax, self.ymax

    @cached_property
    def dz(self):
        """Thickness of model layers."""
        if self.ddz <= 0.99:
            raise NotImplementedError(
                "The stretch factors smaller than 0.99 are not supported."
                "Please check GRAL code for z generation."
            )
        return self.dz0 * self.ddz ** np.arange(self.nz)

    @cached_property
    def z(self):
        """Relative height of model layers"""
        return np.append(0, np.cumsum(self.dz))

    @cached_property
    def wgs84_bounds(self):
        if self.crs == WGS84:
            return self.bounds
        return self.get_gpd_mesh().to_crs(WGS84).total_bounds

    def get_center_mesh(self) -> tuple[np.ndarray, np.ndarray]:
        """Return the center mesh of the grid."""
        return np.meshgrid(self.xcenter, self.ycenter, indexing="ij")

    def get_corner_mesh(self) -> tuple[np.ndarray, np.ndarray]:
        """Return the corner mesh of the grid."""
        return np.meshgrid(self.xcorner, self.ycorner, indexing="ij")

    def get_mesh_from_gpd(
        self, geo_serie: gpd.GeoSeries
    ) -> tuple[np.ndarray, np.ndarray]:
        """Return the center mesh of the grid."""
        return geo_serie.x.to_numpy().reshape(
            self.shape
        ), geo_serie.y.to_numpy().reshape(self.shape)

    def get_gpd_cell_centers(self) -> gpd.GeoSeries:
        """Return the cell centers as a geopandas.GeoSeries."""

        x, y = self.get_center_mesh()
        return gpd.GeoSeries.from_xy(x.flatten(), y.flatten(), crs=self.crs)

    def get_gpd_mesh(self) -> gpd.GeoSeries:
        """Return the cell centers as a geopandas.GeoSeries."""

        x, y = self.get_corner_mesh()
        return gpd.GeoSeries.from_xy(x.flatten(), y.flatten(), crs=self.crs)

    def get_xr_grid(self) -> xr.Dataset:
        """Return the grid as xr dataset compatible with rioxarray."""
        da = xr.Dataset(coords={"x": self.xcenter, "y": self.ycenter})
        return (
            da.rio.write_crs(self.crs, inplace=True)
            .rio.set_spatial_dims(x_dim="x", y_dim="y", inplace=True)
            .rio.write_coordinate_system(inplace=True)
        )

    def get_index(self, x, y):
        """Return the index of the cell containing the point (x, y)."""
        return (
            np.array(np.round((x - (self.xmin + 0.5 * self.dx)) / self.dx), dtype=int),
            np.array(np.round((y - (self.ymin + 0.5 * self.dy)) / self.dy), dtype=int),
        )

    def get_transform(self):
        return rasterio.transform.from_bounds(
            self.xmin, self.ymin, self.xmax, self.ymax, self.nx, self.ny
        )

    def get_bounding_polygon(self) -> Polygon:
        """Return the bounding polygon of the grid."""
        return Polygon(
            [
                (self.xmin, self.ymin),
                (self.xmin, self.ymax),
                (self.xmax, self.ymax),
                (self.xmax, self.ymin),
                (self.xmin, self.ymin),
            ]
        )
    
    def to_emiproc(self):
        """Convert the grid to a grid compatible with emiproc.
        
        The emiproc grid looses some information especially considering 
        the vertical dimensison.
        """
        from emiproc.grids import RegularGrid

        return RegularGrid(
            xmin=self.xmin,
            xmax=self.xmax,
            ymin=self.ymin,
            ymax=self.ymax,
            nx=self.nx,
            ny=self.ny,
            name=type(self).__name__,
            crs=self.crs,
        )


class GralGrid(BasicGrid):
    """The gral grid.

    Note that the vertical grid correspond to the flow fields (wind).
    The grid for the concentration is defined by the attribute
    `slices`.
    """

    # Array defining the building height for each cell (size: nx, ny)
    building_heights: np.ndarray

    def __init__(
        self,
        nx,
        ny,
        xmin,
        xmax,
        ymin,
        ymax,
        dz0,
        ddz,
        slices: list[int] | None = None,
        slicethick: int = 1,
        crs: int | str = LV95,
        building_heights: np.ndarray | None = None,
        sourcegroups: list[int] = [],
    ):

        super().__init__(
            xmin=xmin,
            xmax=xmax,
            ymin=ymin,
            ymax=ymax,
            nx=nx,
            ny=ny,
            crs=crs,
        )

        self.dz0, self.ddz = dz0, ddz

        self.slicethick = slicethick
        self.slices = slices

        self.sourcegroups = sourcegroups

        self.logger.debug(f"{self.xcenter = }")
        self.logger.debug(f"{self.ycenter = }")

        if building_heights is not None:
            self.building_heights = building_heights

    @cached_property
    def nz(self):
        """Number of cells in GRAL model."""

        # The number of cells in the vertical axis of gral varies based
        # on the fact that gral domain goes exactly 800 m above the highest
        # point in the topography. One will have to crop the z axis
        # Or read the info from the output directory
        return 2048

    @property
    def slices(self) -> list[int] | None:
        return self._slices

    @slices.setter
    def slices(self, slices: list[int] | None):
        self.logger.debug(f"Setting {slices = }")
        self._slices = slices
        # Automatically set the number of slices
        if slices is not None:
            if len(slices) > 9:
                raise ValueError(
                    "Cannot set more than 9 slices for concentration output in GRAL,"
                    f" received {slices=}"
                )
            self.nslice = len(slices)
        else:
            self.nslice = None

    @property
    def building_heights(self):
        """Height of the buildings.

        Each grid cell has a building height.
        """
        if not hasattr(self, "_building_heights"):
            raise ValueError("Building heights not set.")
        return self._building_heights

    @building_heights.setter
    def building_heights(self, building_heights):
        assert (
            building_heights.shape == self.shape
        ), "Building heights must have the same shape as the grid."
        self._building_heights = building_heights

    @classmethod
    def from_geb(cls, filename):
        """
        GRAL model grid read from GRAL.geb file.
        """
        with open(filename) as geb_file:
            content = [line.split()[0].strip() for line in geb_file]

        logging.getLogger("pygg.grids.from_geb").debug(f"{content = }")

        dx = float(content[0])
        dy = float(content[1])

        dz0, ddz = [float(v) for v in content[2].split(",")]

        nx = int(content[3])
        ny = int(content[4])

        nslice = int(content[5])

        if content[6].startswith("!"):
            sourcegroups = []
        else:
            sourcegroups = [int(v) for v in content[6].split(",") if v]

        xmin = float(content[7])
        xmax = float(content[8])
        ymin = float(content[9])
        ymax = float(content[10])

        grid = cls(
            nx=nx,
            ny=ny,
            xmin=xmin,
            xmax=xmax,
            ymin=ymin,
            ymax=ymax,
            dz0=dz0,
            ddz=ddz,
            sourcegroups=sourcegroups,
        )
        grid.nslice = nslice

        # TODO: fix this assertion to what it should mean
        assert (dx == np.diff(grid.xcenter)[0]) & (dy == np.diff(grid.ycenter)[0])

        return grid

    @classmethod
    def from_gral_rundir(cls, rundir: os.PathLike) -> GralGrid:
        """Contruct the gral grid from a rundir of gral."""

        # TODO create a correct gral run file object

        rundir = Path(rundir)
        grid = cls.from_geb(rundir / "GRAL.geb")

        crs_file = rundir / "crs.txt"
        if not crs_file.is_file():
            grid.logger.warning(
                f"{crs_file} not found. Cannot read the crs."
            )
        else:
            # Read the crs file
            with open(crs_file, "r") as f:
                grid.crs = f.read()

        buildings_file = rundir / "buildings.dat"
        if buildings_file.is_file():
            # Import only here to avoid circular imports
            from pygg.inputs.buildings import read_building_heights

            grid.building_heights = read_building_heights(buildings_file, grid)

        # Read the indat file to find the slices
        in_dat = rundir / "in.dat"
        if not in_dat.is_file():
            grid.logger.warning(
                f"{in_dat} not found. Cannot read the slices for concentration."
            )
        else:
            n_slice = grid.nslice
            with open(in_dat, "r") as f:
                # Line where the slices are located
                slices_line = f.readlines()[9]
                slices_str = slices_line.split("!")[0]
                slisces_list_str = slices_str.split(",")
                # Case trailing space 
                try: 
                    int(slisces_list_str[-1])
                except:
                    slisces_list_str = slisces_list_str[:-1]
                slices = [int(s) for s in slisces_list_str]

                assert (
                    len(slices) == n_slice
                ), f"mismatch with {n_slice} and {len(slices)}"
            grid.slices = slices

        return grid

    def to_geb(self, path: os.PathLike) -> Path:
        """Save grid to GRAL.geb file.

        :param path: Path to directory where GRAL.geb file should be saved.
        """
        path = Path(path)
        path.mkdir(exist_ok=True)
        filepath = path / "GRAL.geb"

        groups = ",".join([str(sg) for sg in self.sourcegroups])

        with open(filepath, "w") as geb_file:

            geb_file.write(
                textwrap.dedent(
                    f"""\
                {self.dx:.1f}                ! cell-size for cartesian wind field in GRAL in x-direction
                {self.dy:.1f}                ! cell-size for cartesian wind field in GRAL in y-direction
                {self.dz0:.1f},{self.ddz:.1f}            ! cell-size for cartesian wind field in GRAL in z-direction, streching factor for increasing cells heights with height
                {self.nx:d}                ! number of cells for counting grid in GRAL in x-direction
                {self.ny:d}                ! number of cells for counting grid in GRAL in y-direction
                {self.nslice:d}                  ! Number of horizontal slices
                {groups}     ! Source groups to be computed seperated by a comma
                {self.xmin:.0f}             ! West border of GRAL model domain [m]
                {self.xmax:.0f}             ! East border of GRAL model domain [m]
                {self.ymin:.0f}             ! South border of GRAL model domain [m]
                {self.ymax:.0f}             ! North border of GRAL model domain [m]
            """
                )
            )

        return filepath
    


class GrammGrid(BasicGrid):
    """The grid for the gram model.

    In additonal to the basic grid, the vertical coordinates are defined.
    """

    nz: int

    # Arrays for the vertical coordinates
    dz: np.ndarray
    z: np.ndarray
    zcenter: np.ndarray

    def __init__(
        self,
        xmin: float,
        xmax: float,
        ymin: float,
        ymax: float,
        nx: int,
        ny: int,
        nz: int,
        dz0: float = 10,
        ddz: float = 1.2,
        **kwargs,
    ):
        """Create a new grid.

        :param xmin: Minimum x coordinate of the grid.
        :param xmax: Maximum x coordinate of the grid.
        :param ymin: Minimum y coordinate of the grid.
        :param ymax: Maximum y coordinate of the grid.
        :param nx: Number of cells in x direction.
        :param ny: Number of cells in y direction.
        :param nz: Number of cells in z direction.
        :param dz0: Thickness of surface layer [m].
        :param ddz: Stretching factor for vertical layers.
        :param kwargs: Additional keyword arguments from the basegrid.
        """
        super().__init__(xmin, xmax, ymin, ymax, nx, ny, **kwargs)
        self.dz0 = dz0
        self.ddz = ddz
        self.nz = nz

    def to_geb(self, path: os.PathLike) -> Path:
        """Save grid to GRAMM.geb file.

        :param path: Path to directory where GRAMM.geb file should be saved.
        """
        path = Path(path)
        filepath = path / "GRAMM.geb"

        with open(filepath, "w") as geb_file:
            geb_file.write(
                textwrap.dedent(
                    f"""\
                        {self.nx}                !number of cells in x-direction
                        {self.ny}                !number of cells in y-direction
                        {self.nz}                !number of cells in z-direction
                        {self.xmin:.0f}              !West border of GRAMM model domain [m]
                        {self.xmax:.0f}              !East border of GRAMM model domain [m]
                        {self.ymin:.0f}              !South border of GRAMM model domain [m]
                        {self.ymax:.0f}              !North border of GRAMM model domain [m]
                    """
                )
            )
        return filepath

    @classmethod
    def from_geb(cls, file_path: os.PathLike):
        """
        GRAMM model grid read from GRAMM.geb file.
        """
        with open(file_path) as geb_file:
            content = [line.split()[0].strip() for line in geb_file]

        nx = int(content[0])
        ny = int(content[1])
        nz = int(content[2])

        xmin = float(content[3])
        xmax = float(content[4])
        ymin = float(content[5])
        ymax = float(content[6])

        return cls(xmin, xmax, ymin, ymax, nx, ny, nz)

    @cached_property
    def zcenter(self):
        """Relative height of the center of the model layers"""
        return 0.5 * (self.z[1:] + self.z[:-1])


def make_gral_grid(x0, y0, dx, dy, width=1200.0, height=1000.0):
    """
    Make GRAL grid with lower lefter corner (x0, y0), resolution (dx, dy)
    and width (default: 1200 m) and height (1000 m).
    """

    xmin = x0 - 0.5 * width
    ymin = y0 - 0.5 * height

    x = np.arange(xmin, x0 + 0.5 * width + dx, dx)
    y = np.arange(ymin, y0 + 0.5 * height + dy, dy)

    xmin, xmax = x[0], x[-1]
    ymin, ymax = y[0], y[-1]

    nx = x.size - 1
    ny = y.size - 1

    dz = 2.0
    stretch = 1.0
    nslice = 100
    sourcegroups = [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        20,
        21,
        34,
        35,
        36,
        37,
    ]

    return gg.grids.GralGrid(
        dx, dy, dz, stretch, nx, ny, nslice, sourcegroups, xmin, xmax, ymin, ymax
    )
