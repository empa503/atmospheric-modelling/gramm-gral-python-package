"""Functions to prepare files for the run."""

from __future__ import annotations
import logging
from pathlib import Path
from typing import Any
import rioxarray 

from pygg.grids import GralGrid, GrammGrid
from pygg.run.gral import GralRunFiles
from pygg.run.gramm import GrammRunFiles
from pygg.inputs.corine import load_corine_data
from pygg.inputs.landuse import landuse_to_asc_file
from pygg.inputs.ggeom import write_ggeom_file
from pygg.inputs.aster import load_aster_data

logger = logging.getLogger("pygg.run.prepare_files")


def prepare_grids(
    gramm_runfiles: GrammRunFiles,
    gral_runfiles: GralRunFiles,
    gramm_grid_kwargs: dict[str, Any],
    gral_grid_kwargs: dict[str, Any],
    crs: str,
    force_recreate_gramm: bool = False,
    force_recreate_gral: bool = False,
) -> tuple[GrammGrid, GralGrid]:
    """Prepare the GRAMM and GRAL grids in the run directory.

    :arg gramm_runfiles: The runfiles for the gramm run.
    :arg gral_runfiles: The runfiles for the gral run.
    :arg gramm_grid_kwargs: The arguments for the gramm grid.
    :arg gral_grid_kwargs: The arguments for the gral grid.
    :arg force_recreate_gramm: If True, the gramm grid is recreated even if it already exists.
    :arg force_recreate_gral: If True, the gral grid is recreated even if it already exists.

    :returns: The gramm and gral grid from the run directory.
    """

    # Check if the gram grid is already present in the run directory
    if not gramm_runfiles.grid_geb.exists() or force_recreate_gramm:
        logger.info("Creating the GRAMM grid")
        if gramm_grid_kwargs is None:
            raise ValueError("Cannot create the gramm grid without arguments.")
        gramm_grid = GrammGrid(**gramm_grid_kwargs, crs=crs)
        gramm_grid.to_geb(gramm_runfiles.run_dir)
    else:
        logger.info("Using existing GRAMM grid")
        gramm_grid = GrammGrid.from_geb(gramm_runfiles.grid_geb)
        gramm_grid.crs = crs
    # Same with the gral grid
    if not gral_runfiles.grid_geb.exists() or force_recreate_gral:
        logger.info("Creating the GRAL grid")
        gral_grid = GralGrid(**gral_grid_kwargs, crs=crs)
        gral_grid.to_geb(gral_runfiles.run_dir)
    else:
        logger.info("Using existing GRAL grid")
        gral_grid = GralGrid.from_geb(gral_runfiles.grid_geb)
        gral_grid.crs = crs
        # Add the parameters that are not stored in the geb file
        gral_grid.slices = gral_grid_kwargs["slices"]


    return gramm_grid, gral_grid


def prepare_landuse(
    gramm_runfiles: GrammRunFiles,
    gramm_grid: GrammGrid,
    force_recreate: bool = False,
    corine_tif: Path | None = None,
):
    """Prepare the land use data in the run directory.

    :arg gramm_runfiles: The runfiles for the gramm run.
    """
    if not gramm_runfiles.landuse.exists() or force_recreate:
        logger.debug(f"{gramm_runfiles.landuse=}, {force_recreate=}")
        if corine_tif is not None:
            logger.info("Creating the land use data from CORINE data")
            landuse_data = load_corine_data(corine_tif, gramm_grid)
            landuse_data.to_netcdf(gramm_runfiles.corine_nc)
        else:
            raise ValueError(
                "No land use data available, specify 'corine_tif' to create it from"
                " CORINE data."
            )
        landuse_to_asc_file(landuse_data, gramm_runfiles.landuse)

    else:
        logger.info("Using existing land use data")


def prepare_ggeom(
    aster_dir: Path,
    gramm_runfiles: GrammRunFiles,
    gramm_grid: GrammGrid,
    force_recreate: bool = False,
    ggeom_kwargs: dict[str, Any] = {},
):
    if not gramm_runfiles.ggeom.is_file() or force_recreate:
        logger.debug(f"{gramm_runfiles.ggeom=}, {force_recreate=}")
        logger.info("Creating the ggeom file")
        aster_data = load_aster_data(aster_dir=aster_dir, grid=gramm_grid)
        aster_data.to_netcdf(gramm_runfiles.aster_nc)
        write_ggeom_file(
            ggeom_file=gramm_runfiles.ggeom,
            grid=gramm_grid,
            aster_data=aster_data,
            **ggeom_kwargs,
        )
    else:
        logger.info("Using existing ggeom file")


def prepare_gral_topofile(
    aster_dir: Path,
    gral_runfiles: GralRunFiles,
    gral_grid: GralGrid,
    force_recreate: bool = False,
):

    if not gral_runfiles.topofile.is_file() or force_recreate:
        logger.info(f"Creating {gral_runfiles.topofile.name}")
        aster_data = load_aster_data(aster_dir=aster_dir, grid=gral_grid)
        aster_data.transpose('y', 'x').rio.to_raster(gral_runfiles.topofile.with_suffix(".asc"))
        # Move file renaming the extension to .txt
        gral_runfiles.topofile.with_suffix(".asc").rename(gral_runfiles.topofile)
    else:
        logger.info("Using existing gral topofile")


def prepare_gral_receptors(
    receptors_dict: dict[str, Any],
    receptor_file: Path,
    gral_grid: GralGrid
):
    """Create the Receptor.dat file for the gral run.
    
    It is optional and defines the location of receptor points for which GRAL
    generates an additional file zeitreihe.dat containing the concentrations
    at each receptor separated for each defined source group.

    The first line sets the total number of receptor points. From the second line onwards the file
    is structured as follows:
    1st column: Number of receptors in ascending order
    2nd column: x-coordinate of a receptor point in [m]
    3rd column: y-coordinate of a receptor point in [m]
    4th column: z-coordinate of a receptor point in [m] above ground level
    5th column: optional: name of the receptor point
    6th column: optional: user defined receptor point value for the GUI

    :arg receptors_dict: A dictionary with the receptor points.
        It maps a site name to a tuple with the coordinates (x, y, z). 
    """

    # Check that the receptors are in the grid
    valid_receptors = {
        name: (x, y, z)
        for name, (x, y, z) in receptors_dict.items()
        if x >= gral_grid.xmin and x <= gral_grid.xmax and y >= gral_grid.ymin and y <= gral_grid.ymax
    }
    if len(valid_receptors) != len(receptors_dict):
        logger.warning(
            f"Receptors outside the grid were removed: "
            f"{[name for name in receptors_dict if name not in valid_receptors]}"
        )
    with open(receptor_file, "w") as f:
        f.write(f"{len(valid_receptors)}\n")
        for i, (name, (x, y, z)) in enumerate(valid_receptors.items()):
            f.write(f"{i+1},{x},{y},{z},{name},{i+1}\n")