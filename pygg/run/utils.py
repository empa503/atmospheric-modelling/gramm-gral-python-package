from __future__ import annotations

import logging
import os
import subprocess

from dataclasses import dataclass, field
from os import PathLike
from pathlib import Path
from typing import Iterable
from pygg.utils import Model


logger = logging.getLogger("pygg.run.utils")


@dataclass
class DaintParameters:
    """Parameters for the daint cluster.

    These are specified in the config['daint'] .
    """


    account: str = "em05"
    partition: str = "normal"
    runtime: str  = "00:30:00"
    nodes: int = 1
    mem: str | None = None
    email: str | None = None




@dataclass
class RunFiles:
    """Abstract dataclass for a class that stores run files.
    
    Run files should be only inputs and outputs of the model.
    This can contain some parsed output files.
    It does not include raw input data files that have not been processed.
    The raw files should be specified in the config file.
    """

    run_dir: Path
    model: Model = field(init=False)

    dll: Path = field(init=False)
    grid_geb: Path = field(init=False)
    crs_file: Path = field(init=False)
    meteopgt: Path = field(init=False)

    job_file: Path = field(init=False)
    log_file: Path = field(init=False)

    catalogue_nc: Path = field(init=False)

    def __post_init__(self):
        if self.run_dir is None:
            raise ValueError("run_dir is None")
        self.run_dir = Path(self.run_dir)
        self.crs_file = self.run_dir / "crs.txt"

        self.catalogue_nc = self.run_dir / "catalogue.nc"
        self.meteopgt = self.run_dir / "meteopgt.all"

    def symlink_to(self, path: PathLike) -> None:
        """Symlink the files to a directory.

        :arg path: The directory where the files should be symlinked.
        """
        raise NotImplementedError()

    def dir_simu(self, weather_situation_id: int) -> Path:
        """Return the directory of the simulations with the given weather situation id."""
        return self.run_dir / f"{self.model.value}_{weather_situation_id:05d}"

    @property
    def outputs_nc(self) -> Path:
        """NC file containing all the output of a simulation."""
        return self.run_dir / "outputs.nc"

def define_ranges_of_weather_situations(
    weather_situations: Iterable[int],
) -> list[tuple[int, int]]:
    """Define the ranges of the weather situations based on the given weather situations.

    :arg weather_situations: The number of the weather situations to simulate.
    """
    ranges = []

    range_started = None
    for i in range(1, max(weather_situations) + 1):
        if i in weather_situations:
            if range_started is None:
                # start a range
                range_started = i
        else:
            if range_started is not None:
                # stop the range
                ranges.append((range_started, i))
                range_started = None

    # Add the last range if needed
    if range_started is not None:
        ranges.append((range_started, i + 1))

    return ranges


def distribute_ranges(
    ranges: list[tuple[int, int]], max_jobs: int = 20
) -> list[tuple[int, int]]:
    """Distribute the ranges to optimize based on the number of jobs."""
    # Sort the ranges by size
    def sort_ranges(ranges):
        return sorted(ranges, key=lambda r: r[1] - r[0], reverse=True)

    if len(ranges) >= max_jobs:
        # Already too many jobs
        # Take only the largest jobs
        return sort_ranges(ranges)[:max_jobs]

    while len(ranges) < max_jobs:
        ranges = sort_ranges(ranges)
        # split the first job (longest range)
        start, end = ranges.pop(0)
        if end - start <= 1:
            # Too small to split
            # Add back the range and return
            ranges.append((start, end))
            return ranges
        # split the range in two ranges
        mid = (start + end) // 2
        ranges.append((start, mid))
        ranges.append((mid, end))

    return ranges


def jobs_in_queue() -> list[str]:
    # get the user name
    user = os.environ["USER"]

    # call squeue returning only the name of the jobs
    squeue = subprocess.Popen(
        ["squeue", "-u", user, "-O", "name"], stdout=subprocess.PIPE
    )

    # get the output
    output = squeue.communicate()[0]

    # decode the output
    output = output.decode("utf-8")

    # split the output into lines
    output = output.split("\n")

    return output[1:-1]


def model_jobs_in_queue(model: Model) -> list[int]:
    """Return the job ids of the running model jobs."""

    prefix = f"{model.value}_"
    # Remove and convert to integers
    return [int(line[6:]) for line in jobs_in_queue() if line.startswith(prefix)]



def run_sim(job_file: Path, wait: bool = True):
    """Run a simulation using sbatch."""

    logger.info(f"Starting {job_file=}")

    os.system(f"sbatch {'--wait' if wait else ''} {job_file}")


if __name__ == "__main__":
    print(jobs_in_queue())
    print(model_jobs_in_queue(Model.GRAMM))
