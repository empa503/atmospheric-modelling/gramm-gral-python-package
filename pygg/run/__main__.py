"""Command line interface for running GRAL simulations."""

import argparse
import logging
import os
import shutil
import sys
from pathlib import Path
from typing import TYPE_CHECKING, Any, Iterable, Optional
from pygg.inputs.buildings import prepare_raster_buildings, write_building_heights
from pygg.inputs.in_gral import GralSimulationParameters

import yaml

import pygg.run.gramm
import pygg.run.gral

from pygg.run.utils import DaintParameters, RunFiles


from pygg.run.gramm import GrammRunFiles
from pygg.run.gral import GralRunFiles
from pygg.grids import GralGrid, GrammGrid
from pygg.run.prepare_files import (
    prepare_grids,
    prepare_landuse,
    prepare_ggeom,
    prepare_gral_topofile,
    prepare_gral_receptors,
)
from pygg.inputs.meteo import write_meteopgt_file
from pygg.inputs.iin import write_iindat_file


def main():
    parser = argparse.ArgumentParser(
        description="Run GRAMM-GRAL simulations from a configuration file.",
    )
    parser.add_argument(
        "config_file",
        type=Path,
        help="Path to the configuration file.",
    )
    parser.add_argument(
        "-d",
        "--debug",
        help="Debugging statements, overrides loglevel from the config file.",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=None,
    )
    parser.add_argument(
        "-v",
        "--verbose",
        "--info",
        help="Write out the output, overrides loglevel from the config file.",
        action="store_const",
        dest="loglevel",
        const=logging.INFO,
    )

    args = parser.parse_args()

    config = yaml.load(args.config_file.read_text(), Loader=yaml.FullLoader)

    # Set up logging, log in the terminal and in a file
    logger = logging.getLogger("pygg")
    loglevel_str = config["log_level"] if "log_level" in config else "INFO"
    loglevel = getattr(logging, loglevel_str)
    # Command line overrides the config log level
    if args.loglevel is not None:
        loglevel = args.loglevel

    logger.setLevel(loglevel)
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(logging.Formatter("%(name)s:%(levelname)s: %(message)s"))
    logger.addHandler(ch)

    run_dir = Path(config["run_dir"])
    logger.debug(f"{run_dir=}")
    if not run_dir.exists():
        run_dir.mkdir(parents=True)
        logger.info(f"Created {run_dir=}")
    gral_run_dir = Path(config["gral_run_dir"]) if "gral_run_dir" in config else run_dir
    if not gral_run_dir.exists():
        gral_run_dir.mkdir(parents=True)
        logger.info(f"Created {gral_run_dir=}")

    # Also log to file
    fh = logging.FileHandler(gral_run_dir / "pygg.log")
    fh.setFormatter(
        logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    )
    logger.addHandler(fh)

    # Create a copy of the config file in the run directory
    run_config_file = gral_run_dir / "config.yaml"
    shutil.copyfile(args.config_file, run_config_file)
    logger.debug(f"Created {run_config_file=}")

    # Get the run files
    gramm_kwargs = {}
    gral_kwargs = {}
    if 'wind_dir' in config:
        gramm_kwargs['wind_dir'] = config['wind_dir']
        gral_kwargs['wind_dir'] = config['wind_dir']
    gramm_runfiles = GrammRunFiles(run_dir=run_dir, **gramm_kwargs)
    if 'gff_dir' in config:
        gral_kwargs['gff_dir'] = config['gff_dir']
    if 'geometries_file' in config:
        gral_kwargs['geometries_file'] = config['geometries_file']
    

    gral_runfiles = GralRunFiles(run_dir=gral_run_dir, **gral_kwargs)

    # Daint config
    daint_params = DaintParameters(**config["daint"])

    # Prepare the grids files
    gramm_grid, gral_grid = prepare_grids(
        gramm_runfiles=gramm_runfiles,
        gral_runfiles=gral_runfiles,
        gramm_grid_kwargs=config["gramm_grid"],
        gral_grid_kwargs=config["gral_grid"],
        force_recreate_gramm="gramm_grid" in config["force_recreate"],
        force_recreate_gral="gral_grid" in config["force_recreate"],
        crs=config["crs"]
    )
    # Save the crs in the run dirs 
    with open(run_dir / 'crs.txt', 'w') as f:
        f.write(str(config["crs"]))
    with open(gral_run_dir / 'crs.txt', 'w') as f:
        f.write(str(config["crs"]))

    # prepare land use data
    prepare_landuse(
        gramm_runfiles=gramm_runfiles,
        gramm_grid=gramm_grid,
        force_recreate="landuse" in config["force_recreate"],
        **(config["landuse"] if 'landuse' in config else {}),
    )

    # Prepare the ggeom file
    if 'topography' not in config:
        ggeom_kwargs = {}
        aster_dir = None
    else:
        ggeom_kwargs = config["topography"].copy()
        ggeom_kwargs.pop("aster_dir")
        aster_dir = config["topography"]["aster_dir"]
    prepare_ggeom(
        aster_dir=aster_dir,
        gramm_runfiles=gramm_runfiles,
        gramm_grid=gramm_grid,
        force_recreate="gramm_grid" in config["force_recreate"]
        or "topography" in config["force_recreate"],
        ggeom_kwargs=ggeom_kwargs,
    )

    # prepare meteopgt
    if not gramm_runfiles.meteopgt.is_file() or "meteo" in config["force_recreate"]:
        logger.info("Preparing meteopgt file")
        meteo_kwargs = config["meteo"].copy() if config["meteo"] else {}
        write_meteopgt_file(output_dir=gramm_runfiles.run_dir, **meteo_kwargs)
    else:
        logger.info("Meteopgt file already exists")

    # Write the in file
    if "gramm_in" not in config:
        # Check that we don't run the gramm model 
        if "run" in config and "model" in config["run"] and config["run"]["model"] == "GRAMM":
            raise ValueError(
                "GRAMM input file not specified in config file."
                " Please specify 'gramm_in:' in the config file."
            )
        
        logger.info(
            "GRAMM input file not specified in config file."
            " Not writing the file."
        )

    else:
        write_iindat_file(
            path=gramm_runfiles.run_dir,
            latitude=gramm_grid.latitude,
            **config["gramm_in"],
        )

    # Prepare the executables
    if "gramm_dll" not in config:
        if "run" in config and "model" in config["run"] and config["run"]["model"] == "GRAMM":
            raise KeyError("GRAMM executable ('gramm_dll') not specified in config file.")
        logger.info(
            "GRAMM executable ('gramm_dll') not specified in config file."
            " Not preparing the executable."
        )
    elif config["gramm_dll"] is None or not Path(config["gramm_dll"]).is_file():
        raise FileNotFoundError(f"GRAMM executable not found: {config['gramm_dll']=}")
    else:
        if gramm_runfiles.dll.is_file():
            # delete the existing link
            os.unlink(gramm_runfiles.dll)
        os.symlink(config["gramm_dll"], gramm_runfiles.dll)

    weather_situations = (
        config["run"]["weather_situations"]
        if "run" in config and "weather_situations" in config["run"]
        else None
    )

    start_jobs = (
        config["run"]["max_jobs"] > 0
        if "run" in config and "max_jobs" in config["run"]
        else False
    )
    start_gramm = start_jobs and (
        config["run"]["model"] == "GRAMM" if "model" in config["run"] else False
    )
    max_jobs = (
        config["run"]["max_jobs"]
        if "run" in config and "max_jobs" in config["run"]
        else 20
    )
    dirs_simu_gramm = pygg.run.gramm.prepare_daint_runscripts(
        dotnet_exe=config["dotnet_exe"],
        daint_parameters=daint_params,
        gramm_run_files=gramm_runfiles,
        weather_situations=weather_situations,
        overwrite="gramm_dir_simu" in config["force_recreate"],
        start_jobs=start_gramm,
        max_jobs=max_jobs,
    )

    # copy the directory with all the emissions if it already exists
    if (
        "emissions" in config["force_recreate"]
        or not gral_runfiles.emissions_dir.is_dir()
        or not gral_runfiles.has_valid_emission_dir()
    ):
        if "emissions" not in config or "emiproc_dir" not in config["emissions"]:
            raise ValueError(
                "No emissions directory specified in config file."
                " Please specify 'emissions:emiproc_dir:' in the config file."
            )

        if not Path(config["emissions"]["emiproc_dir"]).is_dir():
            raise ValueError(
                "Invalid format of emissions directory:"
                f" {config['emissions']['emiproc_dir']=}"
            )
        shutil.copytree(
            config["emissions"]["emiproc_dir"],
            gral_runfiles.emissions_dir,
            dirs_exist_ok=True,
        )
        if not gral_runfiles.has_valid_emission_dir():
            if start_jobs:
                raise ValueError(
                    "Invalid format of emissions directory:"
                    f" {config['emissions']['emiproc_dir']=}"
                )
            logger.warning(
                f"Emissions directory {config['emissions']['emiproc_dir']}"
                " does not contain valid emissions files."
                " You can create them using emiproc."
            )
    
    # building data 
    if 'buildings' in config['force_recreate'] or not gral_runfiles.buildings.is_file():
        logger.info(f"Preparing the building files.")
        prepare_raster_buildings(
            grid=gral_grid,
            input_filename=config['buildings']['raster_file'],
            filepath=gral_runfiles.buildings,
            mode="w",
        )
    
    # Receptors
    if 'receptors' in config: 
        if 'receptors' in config['force_recreate'] or not gral_runfiles.receptors.is_file():
            logger.info(f"Preparing the receptor files.")
            prepare_gral_receptors(config['receptors'], gral_runfiles.receptors, gral_grid=gral_grid)

    # prepare GRAL executable
    if "gral_dll" not in config:
        raise KeyError("GRAL executable ('gral_dll') not specified in config file.")
    elif config["gral_dll"] is None or not Path(config["gral_dll"]).is_file():
        raise FileNotFoundError(f"GRAL executable not found: {config['gral_dll']=}")
    else:
        if gral_runfiles.dll.exists():
            # delete the existing link
            gral_runfiles.dll.unlink()
        gral_runfiles.dll.symlink_to(config["gral_dll"])
        logger.debug(f"Created {gral_runfiles.dll=}")

    # Make gral topography
    if not gral_runfiles.topofile.is_file() or "topography" in config["force_recreate"]:
        prepare_gral_topofile(
            aster_dir=config["topography"]["aster_dir"],
            gral_runfiles=gral_runfiles,
            gral_grid=gral_grid,
            force_recreate="gral_grid" in config["force_recreate"]
            or "topography" in config["force_recreate"],
        )

    gral_parameters = GralSimulationParameters(
        **(
            config["gral_parameters"]
            if "gral_parameters" in config and config["gral_parameters"] is not None
            else {}
        )
    )

    start_gral = start_jobs and (
        config["run"]["model"] == "GRAL" if "model" in config["run"] else False
    )
    logger.debug(f"{start_gral=}")

    # Find all the folders
    dirs_simu_gral = pygg.run.gral.prepare_daint_runscripts(
        dotnet_exe=config["dotnet_exe"],
        grid=gral_grid,
        weather_situations=weather_situations,
        run_dir=gral_run_dir,
        daint_parameters=daint_params,
        parameters=gral_parameters,
        gramm_files=gramm_runfiles,
        gral_files=gral_runfiles,
        overwrite="gral_dir_simu" in config["force_recreate"],
        start_jobs=start_gral,
        max_jobs=max_jobs,
    )


if __name__ == "__main__":
    main()
