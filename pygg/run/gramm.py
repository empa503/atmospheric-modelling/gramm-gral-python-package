"""Prepare the runscripts for the gramm runs.

Gramm runs for each of the weather situations.
This module porvides controllers for the gramm runs.
"""

from __future__ import annotations

import logging
import shutil
import time
import os
from os import PathLike
from pathlib import Path
from dataclasses import dataclass, field
from typing import Iterable


import numpy as np

from pygg.grids import GrammGrid
from pygg.inputs.grammin import write_grammindat_file
from pygg.inputs.iin import write_iindat_file
from pygg.inputs.meteo import read_meteopgt
from pygg.run.utils import (
    DaintParameters,
    RunFiles,
    run_sim,
    distribute_ranges,
    define_ranges_of_weather_situations,
)

from pygg.outputs.wind import available_wind_fields
from pygg.utils import Model

logger = logging.getLogger("pygg.run.gramm")


@dataclass
class GrammRunFiles(RunFiles):
    """All the files needed for a gramm run.

    Keeps track only on the shared files by all the gramm subsimulations.
    """

    meteopgt: Path = field(init=False)
    corine_nc: Path = field(init=False)
    landuse: Path = field(init=False)
    ggeom: Path = field(init=False)

    # An external directory where to store all the wind file
    wind_dir: Path | None = None

    def __post_init__(self):
        super().__post_init__()
        self.grid_geb = self.run_dir / "GRAMM.geb"
        self.corine_nc = self.run_dir / "corine.nc"
        self.aster_nc = self.run_dir / "aster.nc"
        self.landuse = self.run_dir / "landuse.asc"
        self.ggeom = self.run_dir / "ggeom.asc"
        self.dll = self.run_dir / "GRAMM.dll"
        self.iindat = self.run_dir / "IIN.dat"

    @property
    def model(self) -> Model:
        return Model.GRAMM

    def symlink_to(self, dir_simu: PathLike, for_gral: bool = False) -> None:
        """Symlink the files to a directory.

        :arg path: The directory where the files should be symlinked.
        :arg for_gral: If True, only files for a gral run are symlinked.
        """
        files = (
            [
                self.ggeom,
                self.landuse,
                self.meteopgt,
                self.grid_geb,
                self.dll,
                self.iindat,
            ]
            if not for_gral
            else [
                self.ggeom,
                self.landuse,
                self.meteopgt,
                self.grid_geb,
            ]
        )
        for filepath in files:
            if filepath is None:
                raise ValueError(f"File {filepath} is None")
            os.symlink(filepath, dir_simu / filepath.name)

    def symlink_windfiles_to(
        self, dest_dir: PathLike, weather_situations: Iterable[int]
    ) -> None:

        dest_dir = Path(dest_dir)
        # Find all the available gramm folders
        prefix = f"{self.model.value}_"
        dir_situation_ids = sorted(
            [int(f.stem[len(prefix) :]) for f in self.run_dir.rglob(f"{prefix}*")]
        )

        for weather_situation_id in weather_situations:
            # The wind file is in the directory where the simulation started and has name of the situation
            logger.debug(f"{weather_situation_id=}, {dir_situation_ids=}")
            wnd_file_name = f"{weather_situation_id:05d}.wnd"
            if self.wind_dir is None:
                corresponding_dir_index = (
                    np.searchsorted(dir_situation_ids, weather_situation_id, side="right")
                    - 1
                )
                dir_to_search = dir_situation_ids[corresponding_dir_index]
                logger.debug(f"{corresponding_dir_index=}, {dir_to_search=}")
                wind_file = Path(self.dir_simu(dir_to_search), wnd_file_name)
            else:
                wind_file = Path(self.wind_dir, wnd_file_name)
            if not wind_file.is_file():
                raise ValueError(
                    f"Wind file {wind_file} does not exist. "
                    "Please check you don't have GRAMM files left to run."
                )
            # Create the symlink
            (dest_dir / wnd_file_name).symlink_to(wind_file)
            logger.debug(f"Created symlink {dest_dir / wnd_file_name} -> {wind_file}")


def decide_weather_situations(
    gramm_run_files: GrammRunFiles, max_jobs: int = 20
) -> list[int]:
    """Decide which weather situations should be calculated.

    Gramm runs the next simulation once the first one is finished, so we don't need
    to run simulations right after the others.
    """
    if max_jobs == 0:
        return []

    df_weather = read_meteopgt(gramm_run_files.meteopgt)
    if len(df_weather) > 9999:
        raise ValueError(
            "pygg was not planned for more than 9999 weather situations. "
            "Naming of the folders should be adapted."
        )
    already_calculated = list(
        available_wind_fields(gramm_run_files.run_dir, model=Model.GRAMM).keys()
    )
    missing_weather_situations = set(range(1, len(df_weather) + 1)) - set(
        already_calculated
    )
    if not missing_weather_situations:
        return []

    ranges = define_ranges_of_weather_situations(missing_weather_situations)

    optimal_ranges = distribute_ranges(ranges, max_jobs)

    return [r[0] for r in optimal_ranges]


def prepare_daint_runscripts(
    dotnet_exe: PathLike,
    gramm_run_files: GrammRunFiles,
    weather_situations: list[int] | None,
    daint_parameters: DaintParameters,
    overwrite: bool = False,
    start_jobs: bool = False,
    max_jobs: int = 20,
) -> list[Path]:
    """Prepare the data and runscripts for the gramm runs on daint.

    Though this is hardcoded for daint, one could easily adapt it to other computers.

    :arg run_dir: Directory where the runscripts should be created.
        This function will create subdirectories of this directory for each weather situation.
        The data shared between all the subsimulations will be stored in the directory.
        The other data specific to each weather situation will be stored in the subdirectories.
    :arg gramm_grid: The grid on which the simulations should be run.
    :arg gramm_executable: The path to the executable of gramm.
    :arg weather_situations: The list of weather situations for which the simulations should be run.
    :arg account: The account on daint to which the jobs should be submitted.
    :arg overwrite: If True, the runscripts will be overwritten if they already exist.
    :arg runtime: The runtime of the jobs.
    :arg partition: The partition on daint to which the jobs should be submitted.
    :arg aster_dir: The directory where the ASTER data is stored.
    :arg start_jobs: If True, the jobs will be submitted after the runscripts are created.
        Otherwise, the runscripts will be created but not submitted.

    """

    # Check that the executable exists
    if not gramm_run_files.dll.exists():
        msg = f"The executable {gramm_run_files.dll} does not exist"
        if start_jobs:
            raise ValueError(msg)
        else:
            logger.warning(msg)

    if weather_situations is None:
        weather_situations = decide_weather_situations(
            gramm_run_files, max_jobs=max_jobs
        )
        logger.info(f"Running the following weather situations: {weather_situations}")
    # Check valid values for weather_situations
    # weather situations start at 1
    if np.any(np.array(weather_situations) < 0) or np.any(
        np.array(weather_situations) > len(read_meteopgt(gramm_run_files.meteopgt))
    ):
        raise ValueError(
            "The weather situation ids should be positive and smaller than the number"
            " of weather situations"
        )

    # simulation directories
    dirs_simu = []

    if not weather_situations and start_jobs:
        print(
            "No weather situations to run with GRAMM are left, everything was run"
            " already. "
            "You can now run GRAL."
        )

    for weather_situation_id in weather_situations:

        # Creating the directory of the simulations
        dir_simu = gramm_run_files.dir_simu(weather_situation_id)
        if dir_simu.exists():
            if overwrite:
                shutil.rmtree(dir_simu)
            else:
                logger.warning(
                    f"{dir_simu} already exists. Therefore it was not run again. If you"
                    " want to overwrite it, set overwrite=True or delete the folder"
                    " manually."
                )
                continue
        dir_simu.mkdir()
        dirs_simu.append(dir_simu)

        write_grammindat_file(dir_simu, weather_situation_id)

        # Create an empty file to store the maximum number of processes used
        # it will be filled by the sbatch runscript
        process_file = dir_simu / "Max_Proc.txt"
        process_file.touch()

        # Linking input files to the simulation directory
        gramm_run_files.symlink_to(dir_simu)

        # Submitting the simulation to daint

        job_file = dir_simu / "run_gramm.slurm"
        log_file = dir_simu / f"slurm.gramm.log"

        with open(job_file, "w") as job:
            header = "#!/bin/bash -l \n"
            header += f"#SBATCH --account={daint_parameters.account}\n"
            header += f"#SBATCH --job-name={dir_simu.stem} \n"
            header += f"#SBATCH --time={daint_parameters.runtime} \n"
            header += f"#SBATCH --nodes={daint_parameters.nodes}\n"
            header += "#SBATCH --ntasks-per-core=2\n"
            header += "#SBATCH --ntasks-per-node=1\n"
            header += "#SBATCH --cpus-per-task=72\n"
            header += f"#SBATCH --partition={daint_parameters.partition} \n"
            header += "#SBATCH --constraint=mc\n"
            header += f"#SBATCH --chdir={str(dir_simu)}\n"
            header += f"#SBATCH --output={log_file}\n\n"

            header += "export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK\n"
            # Here it was hardcoded to 144 in the legacy code (before pygg package)
            header += "echo $SLURM_CPUS_PER_TASK >> Max_Proc.txt\n\n"

            header += f"srun {str(dotnet_exe)} {gramm_run_files.dll.name}\n"

            job.write(header)

        if start_jobs:
            # Launch the job on daint
            run_sim(job_file, False)

    return dirs_simu
