"""Prepare gral for running on daint.

GRAL simulations are done in subfolders.
A subfolder contains simulations for few consecutive weather situations.
The name of the subfolders corresponds to the first weather situation number.
As we don't know how many simulations were managed to be run, we 
provide additional functions to help diagnose the simulations.
"""
from __future__ import annotations
from dataclasses import dataclass, field
from functools import cached_property
import json
import numpy as np

import logging
from os import PathLike
import os
from pathlib import Path
import shutil
from typing import TYPE_CHECKING, Iterable


from pygg.inputs.in_gral import write_indat_file, GralSimulationParameters
from pygg.grids import GralGrid
from pygg.inputs.meteo import N_HEADERS_METEOPGT, read_meteopgt
from pygg.run.utils import (
    DaintParameters,
    RunFiles,
    run_sim,
    define_ranges_of_weather_situations,
    distribute_ranges,
)
from pygg.inputs import emission
from pygg.utils import Model

if TYPE_CHECKING:
    from pygg.run.gramm import GrammRunFiles

logger = logging.getLogger("pygg.run.gral")


@dataclass
class GralRunFiles(RunFiles):
    """All the files needed for a gral run."""

    # emission files
    emissions_dir: Path = field(init=False)

    gff_dir: Path | None = None
    geometries_file: Path | None = None
    wind_dir: Path | None = None

    def __post_init__(self):
        super().__post_init__()

        self.grid_geb = self.run_dir / "GRAL.geb"

        self.emissions_dir = self.run_dir / "emissions"
        try:
            self.emissions_dir.mkdir(exist_ok=True)
        except PermissionError:
            # Probably just reading from someone elses files so cannot create
            pass
        except OSError:
            # Probably just reading from someone elses files so cannot create
            # OSError: [Errno 30] Read-only file system:
            pass

        self.groups_mapping = self.emissions_dir / "source_groups.json"
        self.emissions_points = self.emissions_dir / "point.dat"
        self.emissions_lines = self.emissions_dir / "line.dat"
        self.emissions_cadastre = self.emissions_dir / "cadastre.dat"
        self.emissions_portals = self.emissions_dir / "portals.dat"

        self.topofile = self.run_dir / "GRAL_topofile.txt"

        self.dll = self.run_dir / "GRAL.dll"

        self.buildings = self.run_dir / "buildings.dat"

        self.receptors = self.run_dir / "Receptor.dat"

        if self.gff_dir is not None:
            self.gff_dir = Path(self.gff_dir)
        if self.geometries_file is not None:
            self.geometries_file = Path(self.geometries_file)

    @property
    def model(self) -> Model:
        return Model.GRAL

    def has_valid_emission_dir(self) -> bool:
        """Check if the emissions files format are valid."""

        if not self.groups_mapping.is_file():
            return False
        if not any(
            [
                f.is_file
                for f in [
                    self.emissions_points,
                    self.emissions_lines,
                    self.emissions_cadastre,
                    self.emissions_portals,
                ]
            ]
        ):
            return False
        return True

    def symlink_to(self, path: PathLike) -> None:
        """Symlink the files to a directory.

        :arg path: The directory where the files should be symlinked.
        """
        optional_files = [
            self.receptors,
            self.emissions_points,
            self.emissions_lines,
            self.emissions_cadastre,
            self.emissions_portals,
        ]
        for filepath in [
            self.grid_geb,
            self.crs_file,
            self.dll,
            self.topofile,
            self.buildings,
        ] + optional_files:
            if filepath is None:
                raise ValueError(f"File {filepath} is None")
            if filepath.is_file():
                os.symlink(filepath, path / filepath.name)
            elif filepath in optional_files:
                pass
            else:
                raise FileNotFoundError(
                    f"File {filepath} does not exist. Missing for {self.model}"
                )

        if self.gff_dir is not None:
            # Add the file that specifies where the gff files are
            with open(path / "GFF_FilePath.txt", "w") as f:
                # Two lines are needed becaue of different os
                f.write(str(self.gff_dir) + "\n")
                f.write(str(self.gff_dir) + "\n")
        else:
            # Create the desired output files
            # GRAL_FlowFields.txt will ask gral to save the flow fields
            for f_name in ["GRAL_FlowFields.txt"]:
                f = path / f_name
                f.touch()
        if self.geometries_file is not None:
            shutil.copy(self.geometries_file, path / self.geometries_file.name)

        if self.wind_dir is not None:
            with open(path / "windfeld.txt", "w") as f:
                # Two lines are needed becaue of different os
                f.write(str(self.wind_dir) + "\n")
                f.write(str(self.wind_dir) + "\n")


def runned_gral_runs(gral_simu_dir: Path) -> list[int]:
    """Find the runs that have already been simulated.

    :arg gral_simu_dir: The directory of the gral simulations
        (parent containing) all the .

    """

    # Read the wind files and start at the smallest situation
    prefix = Model.GRAL.value
    prefix_len = len(prefix) + 1  # Also the underscore
    gral_dirs = [f for f in gral_simu_dir.rglob(f"{prefix}_*") if f.is_dir()]
    # Check the flow fields to determine the Run simulations
    runned_situations = []
    for gral_dir in gral_dirs:
        # .con files contain the concentrations, the 5 digits are the run number
        # following digits are for horizontal slices and for source groups
        runned_situations.extend([int(f.stem[:5]) for f in gral_dir.rglob(f"*.con")])

    return sorted(list(set(runned_situations)))


def missing_gral_runs(
    gral_simu_dir: Path,
    number_of_situations: int,
) -> list[int]:
    """Find the runs that have not been simulated.

    :arg gral_simu_dir: The directory of the gral simulations
        (parent containing) all the.
    :arg number_of_situations: The number of weather situations to simulate.
    """

    runned_runs = runned_gral_runs(gral_simu_dir)

    return [i for i in range(1, number_of_situations + 1) if i not in runned_runs]


def prepare_daint_runscripts(
    dotnet_exe: PathLike,
    run_dir: PathLike,
    grid: GralGrid,
    parameters: GralSimulationParameters,
    gramm_files: GrammRunFiles,
    gral_files: GralRunFiles,
    daint_parameters: DaintParameters,
    weather_situations: list[int] | None = None,
    overwrite: bool = False,
    start_jobs: bool = False,
    max_jobs: int = 20,
) -> Path:
    """This should assume that GRAMM is already prepared.

    :arg run_dir: The main directory of the gramm gral run.
    :arg dir_simu: The directory where the simulations will be run.
    :arg weather_situations: The number of the weather situations to start.
        If sepcif
    """

    run_dir = Path(run_dir)
    files = gral_files

    df_weather = read_meteopgt(gramm_files.meteopgt)

    # Check the weather situations
    if weather_situations is None:
        weather_situations = missing_gral_runs(
            gral_simu_dir=files.run_dir,
            number_of_situations=len(df_weather),
        )
    logger.info(f"Missing {weather_situations=}.")

    # Package the weather situations
    weather_situations_ranges = define_ranges_of_weather_situations(weather_situations)
    weather_situations_ranges = distribute_ranges(weather_situations_ranges, max_jobs)

    logger.info(f"Will run {weather_situations_ranges=}.")

    # when running transient simulation, only create a folder for the first situation given in weather_situations.
    # The situations given in transient_situations are simulated, weather_situations are ignored.
    if (not parameters.steady_state) and parameters.transient_situations:
            if len(weather_situations) > 1:
                logger.warning(
                    f"You gave several weather situations as an input, but this is a transient simulation. The weather_situations are ignored and the transient simulation will be run for the situations {parameters.transient_situations=}."
                )
            if parameters.transient_n_hours != len(parameters.transient_situations):
                parameters.transient_n_hours = len(
                    parameters.transient_situations
                )
                logger.info(
                    f"This is a transient simulation, and the number of simulated hours was set to the number of situations given ({parameters.transient_n_hours})."
                )

    # Prepare the emission files
    # We will need to add the information on the substances in this
    if files.has_valid_emission_dir():
        source_file = files.emissions_dir / "source_groups.json"
        with open(source_file, "r") as f:
            source_groups = [int(k) for k in json.load(f).keys()]
        logger.info(f"Found source groups {source_groups} in {source_file}.")
        if any([sg > 99 or sg < 0 for sg in source_groups]):
            raise ValueError(
                "Gral only accept source groups between 0 and 100."
                f"Found invalid values in {source_file}."
            )
    else:
        source_groups = []

    # Specify the sourcegroups to compute from the emissions
    # as necesarry for the grid 🤷
    grid.sourcegroups = source_groups
    files.grid_geb = grid.to_geb(run_dir)

    dir_simus = []
    # Creating the directory of the simulations
    for weather_start_id, weather_stop_id in weather_situations_ranges:
        dir_simu = files.dir_simu(weather_start_id)

        if dir_simu.is_dir():
            if overwrite:
                shutil.rmtree(dir_simu)
            else:
                continue
        dir_simu.mkdir(exist_ok=True)

        # Symlink files from GRAMM needed by GRAL
        gramm_files.symlink_to(dir_simu, for_gral=True)

        # Symlink files for GRAL
        files.symlink_to(dir_simu)

        # Create the in.dat file
        write_indat_file(
            dir_simu,
            grid.latitude,
            weather_start_id,
            parameters,
            receptor_points=files.receptors.exists(),
        )

        # Create the micro vert layers file
        with open(dir_simu / "Micro_vert_layers.txt", "w") as f:
            f.write(f"{parameters.micro_vert_layers}\n")

        with open(dir_simu / "Turbulence_model.txt", "w") as f:
            f.write(f"{parameters.turbulence_model}\n")

        with open(dir_simu / "Relaxation_factors.txt", "w") as f:
            f.write(f"{parameters.relaxation_factor_velocity}\n")
            f.write(f"{parameters.relaxation_factor_pressure}\n")

        with open(dir_simu / "Integrationtime.txt", "w") as f:
            f.write(f"{parameters.integration_time_min}\n")
            f.write(f"{parameters.integration_time_max}\n")

        with open(dir_simu / "Building_roughness.txt", "w") as f:
            f.write(f"{parameters.building_roughness}\n")

        if not parameters.steady_state:
            # File required for the output 
            with open(dir_simu / "GRAL_Vert_Conc.txt", "w") as f:
                pass
            # conc threshold file 
            with open(dir_simu /  "Trans_conc_threshold.txt", "w") as f:
                f.write(f"{parameters.transient_conc_threshold}")

            if parameters.transient_keep_temporary_files:
                with open(dir_simu / "KeepAndReadTransientTempFiles.dat", "w") as f:
                    # This will keep every temp file 
                    # Could be changed in the future
                    f.write(f"1")

            ## prepare the mettimeseries file for transient simulations
            with open(dir_simu / "mettimeseries.dat", "w") as f:
                with open(gramm_files.meteopgt, "r") as f_pgt:
                    lines = f_pgt.readlines()
                for i in range(parameters.transient_n_hours):
                    if parameters.transient_situations:
                        #use a different weather situation for each hour (for consequent transient simulation)
                        weather_situation = parameters.transient_situations[i] # read the ith weather situation to use in hour transient_n_hours
                    else:
                        # Simply simulate the same situation in transient mode
                        weather_situation = weather_start_id
                    # Skip the header
                    line = lines[weather_situation + N_HEADERS_METEOPGT - 1]
                    # 3 first of line 
                    direction, velocity, stability = line.split(",")[:3]
                    # Inverted in the meteo file
                    meteo_params = f"{velocity},{direction},{stability}"
                    day = int(np.ceil((i+1)/24))
                    f.write(f"{day:02}.01,{i},{meteo_params}\n") 

            if parameters.emissions_timeseries is not None:
                # copy the file to the run directory
                shutil.copy(parameters.emissions_timeseries, dir_simu)
            else:
                #write a Emissions_timeseries file with scaling 1
                with open(dir_simu / "Emissions_timeseries.txt", "w") as f:
                    # Write the header  
                    f.write('\t'.join(
                        [
                            'Day.Month',
                            'Hour',
                            *map(str, source_groups),
                        ]
                    ))
                    f.write('\n')
                    line_end = '\t'.join(['1'] * len(source_groups))
                    for i in range(parameters.transient_n_hours):
                        day = int(np.ceil((i+1)/24))
                        f.write(f"{day:02}.01\t{i}\t{line_end}\n")

           
        

        job_file = dir_simu / "run_gral.slurm"
        log_file = dir_simu / f"slurm.gral.log"

        with open(job_file, "w") as job:
            header = "#!/bin/bash -l \n"
            header += f"#SBATCH --account={daint_parameters.account}\n"
            header += f"#SBATCH --job-name={dir_simu.stem} \n"
            header += f"#SBATCH --time={daint_parameters.runtime} \n"
            header += "#SBATCH --nodes=1\n"
            header += "#SBATCH --ntasks-per-core=1\n"  # This is not the same as GRAMM
            header += "#SBATCH --ntasks-per-node=1\n"
            header += "#SBATCH --cpus-per-task=72\n"
            header += f"#SBATCH --partition={daint_parameters.partition} \n"
            header += "#SBATCH --constraint=mc\n"
            header += f"#SBATCH --chdir={str(dir_simu)}\n"
            header += f"#SBATCH --output={log_file}\n"
            if daint_parameters.mem is not None:
                header += f"#SBATCH --mem={daint_parameters.mem}\n"
            if daint_parameters.email is not None:
                header += f"#SBATCH --mail-user={daint_parameters.email}\n"
                header += "#SBATCH --mail-type=ALL\n"

            header += "\n"

            header += "export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK\n"
            # Here it was before hardcoded to 72 (before the pygg package)
            header += "echo $SLURM_CPUS_PER_TASK >> Max_Proc.txt\n\n"

            header += f"srun {str(dotnet_exe)} {files.dll.name}\n"

            job.write(header)

        if start_jobs:
            # Launch the job on daint
            run_sim(job_file, False)

    return dir_simus


if __name__ == "__main__":
    print(distribute_ranges([(1, 50), (55, 75)]))
