"""Few utility."""
import argparse
import os
from pathlib import Path

from pygg.inputs.meteo import read_meteopgt
from pygg.outputs.wind import available_wind_fields
from pygg.outputs.concentrations import available_conc_fields

from pygg.utils import Model


def main():
    # task argument for defining what to do
    parser = argparse.ArgumentParser(
        description=(
            "Utility for pygg module. \nCall pygg.run to do the preporcessing, see"
            " 'python -m pygg.run -h ' for more information ."
        )
    )
    parser.add_argument(
        "task",
        type=str,
        help="Task to do.",
        choices=["n_weathers", "help", "squeue", "jobs_status"],
    )

    # add argument for specifying run directory
    parser.add_argument(
        "-d",
        "--run-dir",
        type=str,
        help="Path to run directory.",
        default=".",
    )

    # parse
    args = parser.parse_args()

    # if n_weathers print out the number of weather situations availabe
    if args.task == "n_weathers":
        directory = Path(args.run_dir)
        meteopgt_file = directory / "meteopgt.all"

        if not meteopgt_file.is_file():
            print("No meteopgt file found. You are not in a pygg run directory.")
            return
        meteopgt = read_meteopgt(meteopgt_file)
        print(f"Number of weather situations: {len(meteopgt)}")
        # Check for the genreated wind fields
        wind_fields_gramm = available_wind_fields(directory, Model.GRAMM)
        print(f"Simulated with gramm        : {len(wind_fields_gramm)}")
        wind_fields_gral = available_wind_fields(directory, Model.GRAL)
        print(f"Simulated with gral         : {len(wind_fields_gral)}")
        # Check for the generated concentration fields
        conc_fields = available_conc_fields(directory)
        print(f"Concentration fields        : {len(conc_fields)}")
    elif args.task == "help":
        parser.print_help()
    elif args.task == "squeue":
        # Get all the jobs in the queue of the current user
        import subprocess

        # get the user name
        user = os.environ["USER"]

        # call squeue
        squeue = subprocess.Popen(["squeue", "-u", user], stdout=subprocess.PIPE)

        # get the output
        output = squeue.communicate()[0]

        # decode the output
        output = output.decode("utf-8")

        # split the output into lines
        output = output.split("\n")

        # print the output
        gral_jobs = []
        gramm_jobs = []
        for line in output:
            # check if the line is running GRAMM or GRAL
            if "GRAMM" in line:
                gramm_jobs.append(line)
            elif "GRAL" in line:
                gral_jobs.append(line)

        def print_jobs(jobs, name):
            if len(jobs) > 10:
                print(len(jobs), name, "jobs running")
            elif len(jobs) > 0:
                print(name, "jobs running:")
                # add the header
                print(output[0])
                for job in jobs:
                    print(job)
            else:
                print("No", name, "jobs running")

        print_jobs(gramm_jobs, "GRAMM")
        print_jobs(gral_jobs, "GRAL")
    elif args.task == "jobs_status":
        # Get all the jobs in the queue of the current user
        import subprocess

        # get the user name
        user = os.environ["USER"]

        # call squeue
        squeue = subprocess.Popen(["squeue", "-u", user], stdout=subprocess.PIPE)

        # get the output
        output = squeue.communicate()[0]

        # decode the output
        output = output.decode("utf-8")

        # split the output into lines
        output = output.split("\n")

        # print the output
        gral_jobs = []
        gramm_jobs = []
        for line in output:
            # check if the line is running GRAMM or GRAL
            if "GRAMM" in line:
                gramm_jobs.append(line)
            elif "GRAL" in line:
                gral_jobs.append(line)

        # for each job, check the last generated files
        # and see if the job already was done in another folder
        last_jobs = {}
        job_ids = {}
        # get the run directory
        run_dir = Path(args.run_dir)
        for job in gramm_jobs + gral_jobs:
            # get the job id
            job_id = job.split()[0]
            # get the last generated files
            job_name = job.split()[3]
            model_str, id_start = job_name.split("_")
            model = Model(model_str)
            # get the directory where the job is running
            job_dir = run_dir / f"{model_str}_{id_start}"
            if model == Model.GRAMM:
                suffix = ".wnd"
            elif model == Model.GRAL:
                # Concnetration files
                suffix = ".con"
            else:
                raise NotImplementedError(
                    f"Unkown model {model}. Only {Model.GRAMM} and {Model.GRAL} are known"
                )
            # get the last runned situation from the last generated files
            runned = [f.stem[:5] for f in job_dir.glob(f"*{suffix}")]
            if not runned:
                # no files generated yet
                last_runned_situation = id_start
            else:
                last_runned_situation = max(sorted(set(runned)))
            last_jobs[job_name] = last_runned_situation
            job_ids[job_name] = job_id
        # print the output
        for job_name in sorted(last_jobs.keys()):
            print(
                f"Job {job_ids[job_name]}, {job_name} is running situation {last_jobs[job_name] }"
            )
            model_str, id_start = job_name.split("_")
            model = Model(model_str)
            # Find if another job has runned this situation
            model_dirs = [d for d in run_dir.glob(f"{model_str}_*") if d.is_dir()]
            potential_dirs = [
                d
                for d in model_dirs
                if int(d.stem.split("_")[1]) > int(id_start)
                and int(d.stem.split("_")[1]) <= int(last_jobs[job_name])
            ]
            if potential_dirs:
                print(
                    f"Other jobs have potentially runned the current situation: {potential_dirs[0]}. Therefore {job_id} could be cancelled."
                )

    else:
        print(f"Unknown task. {args.task}")
        parser.print_help()


if __name__ == "__main__":
    main()
