"""Making a single netcdf output file for the simulation."""
from __future__ import annotations
from pathlib import Path
import xarray as xr 

from pygg.utils import Model
from pygg.run.gramm import GrammRunFiles
from pygg.run.gral import GralRunFiles


def make_nc_file(run_dir: Path, model: Model | None = None):
    """Create the nc file of the output for the given model."""

    ...

#%%
if __name__ == "__main__":

    run_dir = "/scratch/snx3000/lconstan/gramm_gral2"
    make_nc_file(run_dir)
