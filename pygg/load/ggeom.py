

from os import PathLike
import xarray as xr
import numpy as np
from pygg.grids import GrammGrid


def read_ggeom_asc(file: PathLike, grid: GrammGrid) -> xr.Dataset:
    """Read teh GGEOM file.
    
    It contains most of the information needed to establish the GRAMM grid. Although it is written
    as ASCII textfile, the format is not really designed to retrieve information using a text editor.
    The following parameters and arrays are included in subsequent order:
    NX, NY, NZ, X, Y, Z, AH, VOL, AREAX, AREAY, AREAZX, AREAZY, AREAZ, ZSP, DDX,
    DDY, ZAX, ZAY, IKOOA, JKOOA, WINKEL, AHE
    WINKEL is a historic parameter equal to zero (not used anymore).
    
    """

    # Read
    with open(file, "r") as f:
        line0 = f.readline().split(' ')

        nx, ny, nz, = [int(v) for v in line0[:3]]
        # End of line is a newline character
        if line0[-1] == "\n":
            line0 = line0[:-1]
        assert len(line0) == 3+nx+ny+nz+3, f"The file is not in the expected format: {len(line0)} != {3+nx+ny+nz+3}"
        
        assert nx == grid.nx, "The file is not in the expected format (NX)."
        assert ny == grid.ny, "The file is not in the expected format (NY)."
        assert nz == grid.nz, "The file is not in the expected format (NZ)."

        x = np.array(line0[3:3+nx+1], dtype=float)
        y = np.array(line0[3+nx+1:3+nx+ny+2], dtype=float)
        z = np.array(line0[3+nx+ny+2:3+nx+ny+nz+3], dtype=float)

    
        # Second line is AH (height of the surface)
        ah = np.array(f.readline().split(' '), dtype=float).reshape(grid.shape, order="F")

        # Third line is VOL (volume of grid cells)
        vol = np.array(f.readline().split(' '), dtype=float).reshape((nx, ny, nz), order="F")

        # Fourth line is AREAX (area of the grid cell in x-direction)
        areax = np.array(f.readline().split(' '), dtype=float).reshape((nx + 1, ny, nz), order="F")

        # Fifth line is AREAY (area of the grid cell in y-direction)
        areay = np.array(f.readline().split(' '), dtype=float).reshape((nx, ny + 1, nz), order="F")

        # Sixth line is AREAZX (projection of the ground area of the grid cell in x-direction)
        areazx = np.array(f.readline().split(' '), dtype=float).reshape((nx, ny, nz + 1), order="F")

        # Seventh line is AREAZY (projection of the ground area of the grid cell in y-direction)
        areazy = np.array(f.readline().split(' '), dtype=float).reshape((nx, ny, nz + 1), order="F")

        # Eighth line is AREAZ (bottom area of the grid cell)
        areaz = np.array(f.readline().split(' '), dtype=float).reshape((nx, ny, nz + 1), order="F")

        # Ninth line is ZSP (height of the centre point of each grid cell)
        zsp = np.array(f.readline().split(' '), dtype=float).reshape((nx, ny, nz), order="F")

        # Tenth line is DDX (horizontal grid size in x-direction)
        ddx = np.array(f.readline().split(' '), dtype=float)
        assert len(ddx) == nx, "The file is not in the expected format (DDX)."

        # Eleventh line is DDY (horizontal grid size in y-direction)
        ddy = np.array(f.readline().split(' '), dtype=float)
        assert len(ddy) == ny, "The file is not in the expected format (DDY)."

        # Twelfth line is ZAX (vertical grid size in x-direction)
        zax = np.array(f.readline().split(' '), dtype=float)
        assert len(zax) == nx, "The file is not in the expected format (ZAX)."

        # Thirteenth line is ZAY (vertical grid size in y-direction)
        zay = np.array(f.readline().split(' '), dtype=float)

        # Fourteenth line is IKOOA, JKOOA (coordinates of the lower left corner of the grid) and WINKEL (not used)
        line = f.readline().split(' ')
        if line[-1] == "\n":
            line = line[:-1]
        ikooa, jkooa, winkel = [float(v) for v in line]

        # Fifteenth line is AHE (height of the ground)
        ahe = np.array(f.readline().split(' '), dtype=float).reshape((nx + 1, ny + 1, nz + 1), order="F")

    x_centers = (x[:-1] + x[1:]) / 2
    y_centers = (y[:-1] + y[1:]) / 2
    z_centers = (z[:-1] + z[1:]) / 2

    ds = xr.Dataset()

    ds["x"] = xr.DataArray(x_centers, dims=("x",), attrs={"long_name": "x-coordinate of grid cell center"})
    ds["y"] = xr.DataArray(y_centers, dims=("y",), attrs={"long_name": "y-coordinate of grid cell center"})
    ds["z"] = xr.DataArray(z_centers, dims=("z",), attrs={"long_name": "z-coordinate of grid cell center"})
    ds["x_edges"] = xr.DataArray(x, dims=("x_edges",), attrs={"long_name": "x-coordinate of grid cell edges"})
    ds["y_edges"] = xr.DataArray(y, dims=("y_edges",), attrs={"long_name": "y-coordinate of grid cell edges"})
    ds["z_edges"] = xr.DataArray(z, dims=("z_edges",), attrs={"long_name": "z-coordinate of grid cell edges"})
    ds["ah"] = xr.DataArray(ah, dims=("x", "y"), attrs={"long_name": "height of the surface"})
    ds["vol"] = xr.DataArray(vol, dims=("x", "y", "z"), attrs={"long_name": "volume of grid cells"})
    ds["areax"] = xr.DataArray(areax, dims=("x_edges", "y", "z"), attrs={"long_name": "area of the grid cell in x-direction"})
    ds["areay"] = xr.DataArray(areay, dims=("x", "y_edges", "z"), attrs={"long_name": "area of the grid cell in y-direction"})
    ds["areazx"] = xr.DataArray(areazx, dims=("x", "y", "z_edges"), attrs={"long_name": "projection of the ground area of the grid cell in x-direction"})
    ds["areazy"] = xr.DataArray(areazy, dims=("x", "y", "z_edges"), attrs={"long_name": "projection of the ground area of the grid cell in y-direction"})
    ds["areaz"] = xr.DataArray(areaz, dims=("x", "y", "z_edges"), attrs={"long_name": "bottom area of the grid cell"})
    ds["zsp"] = xr.DataArray(zsp, dims=("x", "y", "z"), attrs={"long_name": "height of the centre point of each grid cell"})
    ds["ddx"] = xr.DataArray(ddx, dims=("x",), attrs={"long_name": "horizontal grid size in x-direction"})
    ds["ddy"] = xr.DataArray(ddy, dims=("y",), attrs={"long_name": "horizontal grid size in y-direction"})
    ds["zax"] = xr.DataArray(zax, dims=("x",), attrs={"long_name": "vertical grid size in x-direction"})
    ds["zay"] = xr.DataArray(zay, dims=("y",), attrs={"long_name": "vertical grid size in y-direction"})
    ds["ahe"] = xr.DataArray(ahe, dims=("x_edges", "y_edges", "z_edges"), attrs={"long_name": "height of the ground"})
    ds["ikooa"] = xr.DataArray(int(ikooa), attrs={"long_name": "x-coordinate of the lower left corner of the grid"})
    ds["jkooa"] = xr.DataArray(int(jkooa), attrs={"long_name": "y-coordinate of the lower left corner of the grid"})
    ds["winkel"] = xr.DataArray(float(winkel), attrs={"long_name": "not used"})



    return ds
    
    

