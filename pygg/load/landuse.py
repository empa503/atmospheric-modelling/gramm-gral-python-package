from os import PathLike
import xarray as xr
import numpy as np

from pygg.grids import GrammGrid

def read_landuse_asc(filepath: PathLike, grid: GrammGrid) -> xr.Dataset:
    """Read an ASC file with landuse data."""

    # Read
    with open(filepath, "r") as f:
        lines = f.readlines()
    
    ds = xr.Dataset()


    # Extract the data
    for line, name in zip(lines, ["rhob", "alambda", "z0", "fw", "epsg", "albedo"]):
        data = np.array(line.split(), dtype=float).reshape(grid.shape, order="F")
        ds[name] = (['x', 'y'], data)
    
    return ds
