from __future__ import annotations
from enum import Enum

from typing import TYPE_CHECKING

import xarray as xr
import numpy as np

if TYPE_CHECKING:
    from pygg.grids import BasicGrid


class Model(Enum):
    """Available models."""

    GRAL = "GRAL"
    GRAMM = "GRAMM"


def fields_to_xarray(
    fields: dict[str, np.ndarray],
    grid: BasicGrid,
    is_2d: bool = False,
    use_cell_edges: bool = False,
    z_axis: list[float| int] | None = None,
) -> xr.Dataset:
    """Merge data fields on a xarray dataset adding grid information.

    The output is then quite easy to visualize or plot thanks to xarray.

    :arg fields: A dictionary of the fields to be put together in the xarray dataset.
    :arg grid: The grid on which the field is taken.
    :arg is_2d: Whether the field is 2D only.
    :arg use_cell_edges: whether to use the cell edges instead of the cell centers
    :arg z_axis: Alternative way to give the z axis, instead of using the one from the grid 
    """

    dims = ("x", "y", "z") if not is_2d else ("x", "y")
    coords = {}
    if use_cell_edges:
        coords["x"] = grid.xcorner
        coords["y"] = grid.ycorner
    else:
        coords["x"] = grid.xcenter
        coords["y"] = grid.ycenter

    if not is_2d:
        if z_axis is None:
            z_axis = grid.z if use_cell_edges else grid.zcenter
        if len(z_axis) >= 2048:
            # TODO: change this somehwere else, only for gral having 
            # unspecified number of cells in z 
            first_field = next(iter(fields.values()))
            n_z = first_field.shape[2]
            z_axis = z_axis[:n_z]

        coords["z"] = z_axis
    ds = xr.Dataset(
        {key: (dims, array) for key, array in fields.items()}, coords=coords
    )
    return ds


if __name__ == "__main__":
    model = Model.GRAL
    print(f"{model.value=}")
