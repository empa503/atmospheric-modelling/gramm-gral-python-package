"""Algorithms to match simulations to observations.

The goal is to find which weather situation corresponds to which time
base on weather observations.

Problems is that we have biases sometimes in the data.

The match 2 obs was initally described in the paper, section 2.2
https://www.sciencedirect.com/science/article/pii/S1352231017301620?via%3Dihub 

"""
from __future__ import annotations
from enum import Enum
from typing import Any
import numpy as np
import xarray as xr
import pandas as pd
import scipy.signal


class Algorithm(Enum):
    """Enum for the different algorithms to match simulations to observations."""

    ORIGINAL = 1
    """Original algorithm from the paper."""

    DIR_SPEED = 2
    """Calculate a match 2 bos score with an algorithm that uses the direction and speed.
    """

    COSINE_SIMILARITY = 3

    DIR_SPEED_PAPER = 4

    DIR_SPEED_PAPER_MODIFIED = 5


REQUIRED_ALGORITHM_KWARGS = {
    Algorithm.ORIGINAL: ["min_obs_speed"],
    Algorithm.DIR_SPEED: ["speed_scaling"],
    Algorithm.DIR_SPEED_PAPER: ["speed_scaling"],
    Algorithm.DIR_SPEED_PAPER_MODIFIED: ["speed_scaling"],
    Algorithm.COSINE_SIMILARITY: [],
}


def calculate_score(
    simulated_values: xr.Dataset,
    observed_values: xr.Dataset,
    algorithm: Algorithm = Algorithm.ORIGINAL,
    **alogrithm_kwargs: dict[str, Any],
):
    """Calculate the match 2 obs score based on the algorithm developed by A. Berchet et al. .

    The simulated value is a mapping containing a arrays of size (n_weather_situtations, n_stations).
    the mapping contains the u and v velocities at the stations.

    The observed values is also a mapping and contains the same as simulated values but
    the arrays have size (n_measurements_times, n_stations)

    The returned scores have shape (n_measurements_times, n_weather_situtations) and
    contain the calculated score for all the weather situations at a given time.

    Scores are proportional to the difference between the observed values and
    the simulated values.

    This function does not perform any smoothing on the scores.

    Station has been replaced by processes, as in case the sensor of a station is moved,
    a new location has to be extracted from the catalogue.
    """

    # Check the dimensions of the arrays such that they have the correct dims
    assert "time" in observed_values.dims
    assert "process" in observed_values.dims
    assert "process" in simulated_values.dims
    assert "weather_situation" in simulated_values.dims

    # Check algorithm requirements
    required_kwargs = REQUIRED_ALGORITHM_KWARGS[algorithm]
    if not all(
        required_kwarg in alogrithm_kwargs for required_kwarg in required_kwargs
    ):
        raise ValueError(
            f"Algorithm {algorithm} requires {required_kwargs=} to be provided as"
            " keyword arguments."
        )

    def get_angle_difference():
        # Calculate the error for the direction
        # Correct for the fact that direction of 0 and 360 are the same
        wind_diff = np.abs(observed_values["WIND_DIR"] - simulated_values["WIND_DIR"])
        angle_ae = np.minimum(
            np.abs(wind_diff),
            np.abs(wind_diff - 360),
        )
        return angle_ae

    if algorithm == Algorithm.ORIGINAL:
        # Mean value of the measurments for each station results in size (n_stations)
        mean_u_obs = observed_values["U"].mean(dim="time")
        mean_v_obs = observed_values["V"].mean(dim="time")

        # For each site, give a deviation from its mean speed
        site_deviation_from_mean_speed = np.sqrt(
            (
                (observed_values["U"] - mean_u_obs) ** 2
                + (observed_values["V"] - mean_v_obs) ** 2
            ).mean(dim="time")
        )

        # Make the score for each observation for each weather situation and station
        # The score is the distance between the simulated values and the observed values
        distance_obs_sim = (observed_values["U"] - simulated_values["U"]) ** 2 + (
            observed_values["V"] - simulated_values["V"]
        ) ** 2

        min_obs_speed = alogrithm_kwargs["min_obs_speed"]

        # Clip probably to avoid a division by zero later
        obs_speed = np.clip(observed_values["WIND_SPEED"], min_obs_speed, None)

        # Scale score with the observed speed and the diference from mean
        # The largers the speed, the smaller the score
        # The larger the deviation from the mean, the smaller the score
        weights = obs_speed * site_deviation_from_mean_speed
        scores = distance_obs_sim / weights

    elif algorithm == Algorithm.DIR_SPEED:
        # Calculate the error for the speed and direction
        # Make the error for each observation for each weather situation and station

        speed_mse = (
            observed_values["WIND_SPEED"] - simulated_values["WIND_SPEED"]
        ) ** 2

        angle_ae = get_angle_difference()

        # Tells how much the angle is scaled, 1.0 means (1 angle unit = 1 speed unit)
        speed_scaling = alogrithm_kwargs["speed_scaling"]

        # Scale score with the observed speed and the diference from mean
        scores = speed_scaling * speed_mse + (1.0 - speed_scaling) * angle_ae

    elif algorithm == Algorithm.COSINE_SIMILARITY:

        # The dot product is simply: u_sim * u_obs + v_sim * v_obs
        dot_product = (
            simulated_values["U"] * observed_values["U"]
            + simulated_values["V"] * observed_values["V"]
        )
        # Scale with the norm of the vectors
        norms = np.sqrt(
            simulated_values["U"] ** 2 + simulated_values["V"] ** 2
        ) * np.sqrt(observed_values["U"] ** 2 + observed_values["V"] ** 2)
        # 0 norms are not allowed, replace them with 1
        norms = xr.where(norms == 0, 1.0, norms)
        scores = dot_product / norms
        # Replace missing scores with an average score of 0
        scores = scores.fillna(scores.mean("process"))
        # Cosine similarity is between -1 and 1, with -1 meaning opposite vectors
        # and 1 meaning same vectors
        # inverse and scale thh scores such that
        # the most similar has the smallest score = 0 and the most different has the largest score = 1
        scores = (1.0 - scores) / 2

    elif (
        algorithm == Algorithm.DIR_SPEED_PAPER
        or algorithm == Algorithm.DIR_SPEED_PAPER_MODIFIED
    ):
        # v_min in paper,  median of measured hourly horizontal wind speeds at all sites
        median_speed = observed_values["WIND_SPEED"].median()
        average_measured_speed_at_sites = (
            observed_values["WIND_SPEED"]
            .clip(min=median_speed, max=None)
            .mean(dim="process")
        )

        # Difference in magnitude of the wind vectors
        speed_difference = np.abs(
            observed_values["WIND_SPEED"] - simulated_values["WIND_SPEED"]
        )

        # As defined in paper
        # By weighting with the inverse average wind speed at the stations,
        # the relative contribution of stations with large observed wind speeds are reduced
        d = speed_difference / average_measured_speed_at_sites
        max_d = d.max()

        a = get_angle_difference()
        if algorithm == Algorithm.DIR_SPEED_PAPER_MODIFIED:
            # Scale the angles difference with the speed
            # to make the contribution of the angle difference smaller with small winds
            a = a * observed_values["WIND_SPEED"].clip(min=0.0, max=median_speed)

        max_a = a.max()

        # Tells how much the angle is scaled, 1.0 means (1 angle unit = 1 speed unit)
        speed_scaling = alogrithm_kwargs["speed_scaling"]
        if speed_scaling > 1.0 or speed_scaling < 0.0:
            raise ValueError(f"{speed_scaling=} must be between 0 and 1.")

        scores = speed_scaling * d / max_d + (1.0 - speed_scaling) * a / max_a

    else:
        raise NotImplementedError(f"Algorithm {algorithm} not implemented.")

    # Average the score over the sites
    scores = scores.mean(dim="process")

    return scores


def smooth_scoring(scores: xr.DataArray | np.ndarray, smooth_window: int = 3):
    """Smooth the scores using an exponential rolling window.

    Scores must be a 2d array of shape (n_observations, n_weather_situations).
    """

    smooth_weights = np.exp(
        -np.abs(np.arange(-smooth_window, smooth_window + 1.0)) / smooth_window
    )

    smoothed_scores = scipy.signal.convolve(
        scores, smooth_weights.reshape((-1, 1)), mode="same"
    )

    if isinstance(scores, xr.DataArray):
        # apply the same dimensions as scores  to smoothed_scores
        smoothed_scores = xr.DataArray(smoothed_scores, coords=scores.coords)

    return smoothed_scores


def get_mask_invalid_irradiation(
    ds_obs: xr.Dataset, df_meteopgt: pd.DataFrame
) -> xr.Dataset:
    """Filter the scores where the irradiation is not valid.

    Where the observed irradiation does not correspond to the one
    from the stability class allowed by the Pasquill Gifford classes.
    """

    # Mean over stations
    average_rad = ds_obs["RAD"]
    # set negative rad values to zero
    average_rad = xr.where(average_rad < 0, 0, average_rad)
    # MAke a mask where the irradiation is invalid compared to the meteopgt
    # Use broadcasting for xarray
    da_solarmax = xr.DataArray(
        df_meteopgt["solarmax"].to_numpy(),
        dims=["weather_situation"],
    )
    da_solarmin = xr.DataArray(
        df_meteopgt["solarmin"],
        dims=["weather_situation"],
    )
    # Combine the two conditions
    return (average_rad < da_solarmin) | (average_rad > da_solarmax)


def get_mask_invalid_wind(
    average_wind: xr.Dataset, df_meteopgt: pd.DataFrame
) -> xr.Dataset:
    """Filter the scores where the windspeed is not valid.

    Where the observed windspeed does not correspond to the one
    from the stability class allowed by the Pasquill Gifford classes.
    """

    # Mean over stations / select a specific station? TODO

    # Make a mask where the wind is invalid compared to the meteopgt
    # Use broadcasting for xarray
    da_windmax = xr.DataArray(
        df_meteopgt["windmax"].to_numpy(),
        dims=["weather_situation"],
    )
    da_windmin = xr.DataArray(
        df_meteopgt["windmin"],
        dims=["weather_situation"],
    )
    # Combine the two conditions
    return (average_wind < da_windmin) | (average_wind > da_windmax)


def get_best_weather_situations(
    ds_scores_matchobs: xr.DataArray, N_BEST: int = 10
) -> pd.DataFrame:
    """Get the best weather situations for each observation.

    :arg ds_scores_matchobs: Data array with the scores for each weather situation
        and each observation.
    :arg N_BEST: Number of best weather situations to return.

    :return: Dataframe with the best weather situations for each observation.
        Columns are integers from 0 to N_BEST-1, serving as a ranking.
        0 is the best, where the score is the lowest.
    """
    ds = ds_scores_matchobs
    df_best_match2obs = pd.DataFrame(index=ds.time, columns=range(N_BEST))
    for i, time in enumerate(ds.time):
        sorted_indices = ds.sel(time=time).argsort()
        best_weather_situations = ds["weather_situation"][sorted_indices[:N_BEST].data]
        df_best_match2obs.iloc[i, :] = best_weather_situations.data

    return df_best_match2obs


if __name__ == "__main__":
    # The following is a simple test of the match2obs function
    # n_stations = 3
    # n_weathers = 2
    # n_times = 4
    scores = calculate_score(
        simulated_values={
            "U": np.array(
                [
                    [0.12, 0.21, 0.31],
                    [0.13, 0.25, 0.32],
                ]
            ),
            "V": np.array(
                [
                    [0.11, 0.21, 0.32],
                    [0.12, 0.21, 0.31],
                ]
            ),
        },
        observed_values={
            "U": np.array(
                [
                    [0.1, 0.2, 0.3],
                    [0.12, 0.2, 0.3],
                    [0.13, 0.2, 0.3],
                    [0.14, 0.2, 0.3],
                ]
            ),
            "V": np.array(
                [
                    [0.1, 0.2, 0.3],
                    [0.11, 0.2, 0.3],
                    [0.12, 0.2, 0.3],
                    [0.13, 0.2, 0.3],
                ]
            ),
        },
    )
    print(scores)

    smoothed_scores = smooth_scoring(scores, smooth_window=1)

    import matplotlib.pyplot as plt

    plt.plot(scores)
    plt.plot(smoothed_scores)
