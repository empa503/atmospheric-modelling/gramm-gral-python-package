"""
Plot windroses.

Original code from: Gabriele Oddo
"""

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt


def wind_rose(
    wind_dir,
    wind_speed,
    n_angles_bins: int = 36,
    thresholds: list[float] = [np.inf, 6, 3, 2, 1],
    colors: list[str] = ["b", "c", "g", "y", "beige"],
    units: str = "m/s",
):
    """Plot a windrose"""
    theta = np.linspace(0, 360, n_angles_bins, endpoint=False)
    sector_angle = 360 / n_angles_bins

    fig, ax = plt.subplots(subplot_kw={"projection": "polar"})

    if len(thresholds) != len(colors):
        raise ValueError(
            f"thresholds and colors must have the same length, got {len(thresholds)} and {len(colors)}"
        )

    # Get the largest threshold, ignoring the np.inf
    thresholds = np.array(thresholds)
    max_thresholds = thresholds[thresholds != np.inf]
    max_threshold = np.max(max_thresholds)

    for i, threshold in enumerate(thresholds):
        angles = np.zeros(n_angles_bins, dtype=int)

        mask_in_threshold = wind_speed < threshold

        np.add.at(
            angles,
            # add %360 to transform 360deg to 0deg
            (wind_dir[mask_in_threshold] % 360 / sector_angle).astype(int),
            1,
        )
        label = f"< {threshold} {units}"
        if threshold == np.inf:
            label = f">={max_threshold} {units}"
        ax.bar(
            np.deg2rad(theta) + np.deg2rad(sector_angle / 2),
            angles,
            width=np.deg2rad(sector_angle),
            color=colors[i],
            bottom=0,
            label=label,
        )
        # if i == 0:
        #    radius = np.max(angles)

    ax.set_theta_zero_location("N")
    ax.set_theta_direction(-1)
    # ax.set_rmax(int(radius + radius / 10))

    ax.legend(bbox_to_anchor=(1.4, 1))
    fig.subplots_adjust(right=0.85)  
    #ax.legend()
    
    return fig, ax


def wind_rose_ax(
    ax,
    wind_dir,
    wind_speed,
    n_angles_bins: int = 36,
    thresholds: list[float] = [np.inf, 6, 3, 2, 1],
    colors: list[str] = ["b", "c", "g", "y", "beige"],
    units: str = "m/s",
):
    """
    Plot a windrose in a given axis.
    When calling the axes/figure, polar projection has to be given as an input:
    fig, axs = plt.subplots(n,m, subplot_kw={"projection": "polar"})

    The legend is not plotted, but can be added later with:
    handles, labels = ax.get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper right')"""

    theta = np.linspace(0, 360, n_angles_bins, endpoint=False)
    sector_angle = 360 / n_angles_bins

    if len(thresholds) != len(colors):
        raise ValueError(
            f"thresholds and colors must have the same length, got {len(thresholds)} and {len(colors)}"
        )

    # Get the largest threshold, ignoring the np.inf
    thresholds = np.array(thresholds)
    max_thresholds = thresholds[thresholds != np.inf]
    max_threshold = np.max(max_thresholds)

    for i, threshold in enumerate(thresholds):
        angles = np.zeros(n_angles_bins, dtype=int)

        mask_in_threshold = wind_speed < threshold

        np.add.at(
            angles,
            (wind_dir[mask_in_threshold] % 360 / sector_angle).astype(
                int
            ),  # leob: add %360 to transform 360deg to 0deg
            1,
        )
        label = f"< {threshold} {units}"
        if threshold == np.inf:
            label = f">={max_threshold} {units}"
        ax.bar(
            np.deg2rad(theta) + np.deg2rad(sector_angle / 2),
            angles,
            width=np.deg2rad(sector_angle),
            color=colors[i],
            bottom=0,
            label=label,
        )

    ax.set_theta_zero_location("N")
    ax.set_theta_direction(-1)


if __name__ == "__main__":

    # wind_speeds = np.random.randint(0, 20, 1000)
    # wind_dirs = np.random.randint(0, 360, 1000)
    wind_speeds = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 1])
    wind_dirs = np.array([0, 1, 2, 30, 40, 50, 359, 359, 359, 359])

    wind_rose(wind_dirs, wind_speeds)

    plt.show()

    # subplots:
    fig, axs = plt.subplots(1, 2, subplot_kw={"projection": "polar"})
    axs = axs.flatten()
    wind_rose_ax(axs[0], wind_dirs, wind_speeds, set_labels=True)
    wind_rose_ax(axs[1], wind_dirs, wind_speeds, set_labels=False)
    handles, labels = axs[1].get_legend_handles_labels()
    fig.legend(handles, labels, loc="upper right")

    plt.show()
