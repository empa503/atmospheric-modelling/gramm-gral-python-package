"""Gral creates a geometry file.

It is the data that GRAL uses to compute the flow fields.
"""


import struct
import zipfile
from os import PathLike

import numpy as np


def read_gral_geometries_txt(file: PathLike) -> dict[str, np.ndarray]:
    """Read the file GRAL_geometries.txt.
    
    The return dictionary contains the ground geometries used in the simulation:

    ahk: absolute altitude of the terrain (including buildings)
    kkart: grid cell index of the height of the terrain in gral.
        Matches the gral flow fields files.
    building_heights: The height of the building 

    """
    if zipfile.is_zipfile(file):
        gralgeom = zipfile.ZipFile(file, "r")

        for filename in gralgeom.namelist():
            byte_list = gralgeom.read(filename)
            gralgeom.close()

    else:
        with open(file, mode="rb") as binfile:
            byte_list = binfile.read()

    nheader = 32
    header = byte_list[:nheader]
    nz, ny, nx, ikooagral, jkooagral, dzk, stretch, ahmin = struct.unpack(
        "iiiiifff", header
    )

    if stretch < 0.1:
        raise NotImplementedError(
            "Stretch factor < 0.1 are not supported. Please check WriteGeometries.cs"
            " function WriteGRALGeometries() in the source code of GRAL for more"
            " information."
        )

    dt = np.dtype([("ahk", "f4"), ("kkart", "i4"), ("bui_height", "f4")])

    data = np.frombuffer(byte_list, dt, offset=nheader)
    # Ignore the last row columns as they are full of zeros
    # TODO: check that this is really true, because doing 
    # ahk - building_heights gives some negatvie values !!!
    data = data.reshape((nx + 1, ny + 1))[:-1, :-1]
    # oro = ahk - bui_height
    return {
        "ahk": data["ahk"],
        "kkart": data["kkart"],
        "building_heights": data["bui_height"],
    }
