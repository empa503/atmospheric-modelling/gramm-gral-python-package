"""The catalogue is the collection of the winds and concentrations at the site locations.


Currently pygg can generate 3 kinds of catalogue.

* gramm winds
* gral winds (from the flow fields)
* concentration fields

The data from the catalogue for the winds is a 2-D array of dimensions:
(n_stations, n_weather_situations)
containing the wind speeds 
the data for concentration is a 3-D array of dimensions:
(n_stations, n_weather_situations, n_sourcegroups)

"""
from __future__ import annotations
from enum import Enum, auto
import logging
from pathlib import Path
from os import PathLike
import time
from typing import Callable
from pygg.outputs.geometries import read_gral_geometries_txt
from pygg.utils import Model

import xarray as xr
import numpy as np
import pandas as pd
from scipy.interpolate import RegularGridInterpolator

from pygg.utils import fields_to_xarray
from pygg.grids import BasicGrid, GrammGrid, GralGrid
from pygg.run.utils import RunFiles
from pygg.run.gramm import GrammRunFiles
from pygg.run.gral import GralRunFiles

from pygg.load.ggeom import read_ggeom_asc

from pygg.outputs.utils import extract_at_stations, get_mask_stations_in_grid_bound
from pygg.outputs.wind import available_wind_fields, read_wind_gff, read_wind_wnd, load_part_of_gral, load_gff_bytes

from pygg.inputs.meteo import read_meteopgt
from pygg.outputs.concentrations import read_3d_con


class Catalogues(Enum):
    GRAMM_WINDS = auto()
    GRAL_WINDS = auto()
    GRAL_CONCS = auto()


class Algorithm(Enum):
    """The algorithm used to extract the data from the catalogue.

    INTERPOLATION: Interpolate the data on the stations locations.
    NEAREST: Take the nearest grid cell to the station location.
    CYLINDER_MEDIAN: Take the median of the values in the cylinder around the station.
        Values insided buildings are not taken into account.
        The cylinder is defined by arguments `extraction_radius` and `extraction_height`.
    """

    INTERPOLATION = auto()
    CYLINDER_MEDIAN = auto()


GRAL_CATALOGUES = [Catalogues.GRAL_WINDS, Catalogues.GRAL_CONCS]
WIND_CATALOGUES = [Catalogues.GRAL_WINDS, Catalogues.GRAMM_WINDS]

MODEL_FROM_CATALOGUE: dict[Catalogues, Model] = {
    Catalogues.GRAMM_WINDS: Model.GRAMM,
    Catalogues.GRAL_WINDS: Model.GRAL,
    Catalogues.GRAL_CONCS: Model.GRAL,
}


def raise_catalogue_error(catalogue: Catalogues):
    if not isinstance(catalogue, Catalogues):
        raise TypeError(f'{catalogue=} given as input is not of type "Catalogues" ')
    raise NotImplementedError(
        f"Something was not implmemented for {catalogue=}"
        "Please check the code to understand why."
    )


def generate_catalogue(
    catalogue: Catalogues,
    run_dir: PathLike,
    df_processes: pd.DataFrame,
    weather_situations: list[int] = None,
    catalogue_nc_file: PathLike | None = None,
    files_dir: PathLike | None = None,
    source_groups: list[int] | None = None,
    algorithm: Algorithm = Algorithm.INTERPOLATION,
    return_cubes: bool = False,
    extraction_radius: float = 10.0,
    extraction_height: float = 10.0,
    extraction_heights_pixels: int | None = None,
    
) -> xr.DataArray:
    """Generate the specified catalogue.


    GRAL generates flow fields.
    The Gral Grid for winds is starting on the lowest topography altitude.
    Flow fields in regions where buildings are located are 0.


    TODO: use the offset information from conc vs wind, because at the
    moment it is not used.

    :arg catalogue: The catalogue that should be plotted
    files_dir: alternative way to specify the directory with the files.
    :arg source_groups: For concentrations, alternatively we can specify
        only a substed of source groups that should be compouted instead of
        all the ones specified for the simulation in the grid file.
    :arg return_cubes: If True, the function will return a tuple of 2 elements:
        the catalogue and the cubes. The cubes are the 3D fields extracted
        around the stations. This is useful for plotting.
    
    Arguments for the extraction of the data around the stations: (when return_cubes=True)
    :arg extraction_radius: The radius of the cube around the station to extract
    :arg extraction_height: The height of the cube around the station to extract
    :arg extraction_heights_pixels: The number of pixels above and below the
        height of the sensor to extract. If this is given, the `extraction_height`
        will not be used.
    """
    logger = logging.getLogger(f"pygg.outputs.generate_catalogue.{catalogue}")

    run_dir = Path(run_dir)

    if extraction_heights_pixels is not None:
        assert isinstance(extraction_heights_pixels, int)
        assert extraction_heights_pixels > 0

    # Read files and grids
    if catalogue in GRAL_CATALOGUES:
        files = GralRunFiles(run_dir)
        grid = GralGrid.from_gral_rundir(files.run_dir)
        if catalogue == Catalogues.GRAL_WINDS:
            files_extension = ".gff"
        elif catalogue == Catalogues.GRAL_CONCS:
            files_extension = ".con"
            if source_groups is None:
                # Read the simulated source groups
                source_groups = grid.sourcegroups
        else:
            raise_catalogue_error(catalogue)
    elif catalogue == Catalogues.GRAMM_WINDS:
        files = GrammRunFiles(run_dir)
        grid = GrammGrid.from_geb(files.grid_geb)
        files_extension = ".wnd"
    else:
        raise_catalogue_error(catalogue)

    if catalogue_nc_file is None:
        # Use the default location
        catalogue_nc_file = files.catalogue_nc

    df_meteopgt = read_meteopgt(files.meteopgt)
    if not weather_situations:
        # Take all of the weather conditions
        weather_situations = df_meteopgt.index.to_list()

    extracted_das = {}
    extracted_cubes = {}
    if algorithm == Algorithm.CYLINDER_MEDIAN:
        # This will remember which coordinates are included in the cylinder
        # for each process
        masks_cylinder: dict[int, xr.DataArray] = {}

    x = df_processes["x"]
    y = df_processes["y"]

    mask = get_mask_stations_in_grid_bound(
        x,
        y,
        grid,
    )
    if not any(mask):
        raise ValueError(
            "No sites found in gral domain. Check sites locations with respect to"
            " domain and the CRS you use."
        )
    # for the altitude in the gral flow fields,
    # we need to use the information on gral geometry

    if catalogue in GRAL_CATALOGUES:
        geometry_file = files.run_dir / "GRAL_geometries.txt"
        if not geometry_file.is_file():
            # Geometry file should be found in the first weather simulation folder
            geometry_file = files.dir_simu(1) / "GRAL_geometries.txt"
        ds_geom = fields_to_xarray(
            read_gral_geometries_txt(geometry_file),
            grid,
            is_2d=True,
        )
    # Find the ground level in the gramm wind fields domain
    index_x, index_y = grid.get_index(x.loc[mask], y.loc[mask])
    sel_kwargs = {
        "coords": {
            "process": df_processes.loc[mask].index.to_list(),
        },
        "dims": "process",
    }
    selectors = {
        "x": xr.DataArray(index_x, **sel_kwargs),
        "y": xr.DataArray(index_y, **sel_kwargs),
    }

    if catalogue in GRAL_CATALOGUES:
        # Minimum altitude in the grid
        gral_min_alt = ds_geom["ahk"].min().data
        # Find the height of the ground surface that was used in simulation
        gral_surface_height = ds_geom["ahk"].isel(selectors) - gral_min_alt
        # find the gral height of the grid cell containing the site
        gral_buildings_height = ds_geom["building_heights"].isel(selectors)
        # Altutude of the ground
        gral_ground_level = gral_surface_height - gral_buildings_height
        # Make sure that sites wont be inside buildings (concentrations and winds will be 0)
        sensor_in_buildings = df_processes.loc[mask, "z"] <= gral_buildings_height
        if any(sensor_in_buildings):
            logger.warning(
                "Some sensors have a height smaller than the building at the"
                " corresponding grid cell in gral :"
                f" {df_processes.loc[mask].loc[sensor_in_buildings, 'site'].to_list()}."
                " GRAL building heights ="
                f" {gral_buildings_height.to_numpy()[sensor_in_buildings]}  \nPlease"
                " check the data for them."
            )

    # Gramm height is the height over the ground floor # TODO make sure this is correct
    # Gral z value is the gral ground level + the elevation of the site over the ground
    if catalogue == Catalogues.GRAL_WINDS:
        z = gral_ground_level.to_numpy() + df_processes.loc[mask, "z"]
    elif catalogue == Catalogues.GRAL_CONCS:
        # Height over the ground  level (not the building) (uses the slices)
        z = df_processes.loc[mask, "z"]
        mask_too_high = z > max(grid.slices)
        if any(mask_too_high):
            logger.warning(
                f"Some stations are over the highest slice ({max(grid.slices)}) for"
                " GRAL concentrations. Concentration will be extrapolated: "
                f"{[(id_, h) for id_, h in zip(df_processes.loc[mask].loc[mask_too_high, 'site'], z[mask_too_high])]}"
            )
    elif catalogue == Catalogues.GRAMM_WINDS:
        # GRAMM has every grid cell with a different vertical profile.
        # We bypass this issue by first reprojecting stations heights on their
        # individual height levels
        z_absolute = df_processes.loc[mask, "z"]

        ds_ggeom = read_ggeom_asc(files.ggeom, grid)
        # Get geometry at stations locations
        centers_heights = ds_ggeom["zsp"].isel(selectors)
        surface_height = ds_ggeom["ah"].isel(selectors)
        relative_heights = centers_heights - surface_height
        # Project the absolute z to the shared z axis (from the grid)
        z = [
            np.interp(
                z_abs,
                relative_heights.sel(process=process).data,
                grid.zcenter,
            )
            for z_abs, process in zip(z_absolute, relative_heights["process"])
        ]

    else:
        raise_catalogue_error(catalogue)

    df_our_sites = df_processes.loc[mask].assign(z=z)

    mapping_station_locations_3d = {
        ax: xr.DataArray(
            df_our_sites[ax],
            coords={"process": df_our_sites.index.to_list()},
            dims="process",
        )
        for ax in ["x", "y", "z"]
    }

    if catalogue in WIND_CATALOGUES:
        if files_dir is None:
            files_dir = files.run_dir
            # Find files from the simulation folder
            dict_files = available_wind_fields(
                files.run_dir, MODEL_FROM_CATALOGUE[catalogue]
            )
        else:
            # All availabe files should be at this location

            dict_files = {
                int(file.stem): file
                for file in Path(files_dir).glob(f"*{files_extension}")
            }
    elif catalogue == Catalogues.GRAL_CONCS:
        dict_files: dict[int, dict[int, Path]] = {}
        if files_dir is None:
            files_dir = run_dir
        logger.debug(f"{files_dir=}")
        logger.debug(f"{files_extension=}")
        # Use also subfolders  if someone splits the SG or weathers in different dirs
        for file in Path(files_dir).glob(f"**/*{files_extension}"):
            logger.debug(f"{file=}")
            weather_situation = int(file.stem[:5])
            sg = int(file.stem[7:9])
            # Assume the files depending on the height are in the same folder
            if weather_situation not in dict_files:
                dict_files[weather_situation] = {}
            dict_files[weather_situation][sg] = file

    # Following loop could be parallelized for speed up ?
    for situation in weather_situations:
        if situation not in dict_files.keys():
            logger.error(f"Weather condition {situation} was not found in {files_dir}")
            continue
        start = time.time()

        if catalogue == Catalogues.GRAL_WINDS:

            #u, v, w = read_wind_gff(dict_files[situation])
            #ds = fields_to_xarray({"U": u, "V": v, "W": w}, grid, use_cell_edges=True)

            byte_list = load_gff_bytes(dict_files[situation])

            dss = [
                load_part_of_gral(
                    wind_bytes=byte_list,
                    gral_grid=grid,
                    extraction_center=(x,y,z),
                    extraction_radius=extraction_radius,
                    extraction_height=extraction_height,
                )
                for x,y,z in zip(x.loc[mask], y.loc[mask], z)
            ]
            ds = xr.merge(dss)

            


        elif catalogue == Catalogues.GRAMM_WINDS:
            wnd_file = dict_files[situation]

            u, v, w = read_wind_wnd(wnd_file)

            ds = fields_to_xarray({"U": u, "V": v, "W": w}, grid)

        elif catalogue == Catalogues.GRAL_CONCS:
            da_sgs = {}
            for sg in source_groups:
                if sg not in dict_files[situation]:
                    logger.error(
                        f"source group {sg} not found for weather situation ="
                        f" {situation}"
                    )
                    continue
                try:
                    con_field = read_3d_con(
                        dict_files[situation][sg].parent,
                        weather_situation=situation,
                        source_group=sg,
                        gral_grid=grid,
                    )
                except Exception as e:
                    logger.error(
                        f"Error when reading {dict_files[situation][sg]}: {e}"
                    )
                    continue
                da_sgs[sg] = con_field
            # Merget the ds
            ds = xr.concat(
                da_sgs.values(), pd.Index(da_sgs.keys(), name="source_group")
            )

        if algorithm == Algorithm.INTERPOLATION:

            interp_kwargs = {}
            if catalogue in [Catalogues.GRAL_CONCS, Catalogues.GRAMM_WINDS]:
                #  If None, values outside the domain are extrapolated
                # For the lowest level because we don't have h=0 in the domain
                interp_kwargs["fill_value"] = None
            # Interpolates the field at the sites location
            extracted_das[situation] = ds.interp(
                mapping_station_locations_3d, kwargs=interp_kwargs
            )
        elif algorithm == Algorithm.CYLINDER_MEDIAN:
            # Extract the data in a cylinder around the sites
            extracted_das[situation] = {}
            for process in df_our_sites.index:
                if process not in masks_cylinder:
                    # Create the mask for this process
                    if catalogue == Catalogues.GRAL_WINDS:
                        ground = ds_geom["ahk"]
                    else:
                        raise NotImplementedError(
                            f"{algorithm=} for {catalogue=} not implemented"
                        )
                    distance_to_site = np.sqrt(
                        (ds["x"] - df_our_sites.loc[process, "x"]) ** 2
                        + (ds["y"] - df_our_sites.loc[process, "y"]) ** 2
                    )
                    cylinder_top = (
                        df_our_sites.loc[process, "z"] + extraction_height / 2
                    )
                    cylinder_bottom = (
                        df_our_sites.loc[process, "z"] - extraction_height / 2
                    )
                    mask_in_ground = (
                        (ds["U"] == 0.0) & (ds["V"] == 0.0) & (ds["W"] == 0.0)
                    )
                    mask_cylinder = (
                        ~mask_in_ground
                        & (distance_to_site < extraction_radius)
                        & (ds["z"] > cylinder_bottom)
                        & (ds["z"] < cylinder_top)
                    )

                    masks_cylinder[process] = mask_cylinder
                print("there")
                # Extract the data
                extracted_das[situation][process] = ds.where(
                    masks_cylinder[process],
                ).median(dim=["x", "y", "z"])

            # Concatenate the data
            extracted_das[situation] = xr.concat(
                extracted_das[situation].values(),
                pd.Index(extracted_das[situation].keys(), name="process"),
            )

        else:
            raise NotImplementedError(
                f"Algorithm {algorithm=} is not implemented."
                " Please choose one of the available algorithms."
            )

        if return_cubes:
            # Extract a cube of data around the stations
            radiuses = {}
            for site in mapping_station_locations_3d["x"]["process"]:
                site_x, site_y, site_z = (
                    mapping_station_locations_3d["x"].sel(process=site),
                    mapping_station_locations_3d["y"].sel(process=site),
                    mapping_station_locations_3d["z"].sel(process=site),
                )
                sel_dict = {
                    "x": slice(site_x - extraction_radius, site_x + extraction_radius),
                    "y": slice(site_y - extraction_radius, site_y + extraction_radius),
                    "z": slice(site_z - extraction_height, site_z + extraction_height),
                }
                if extraction_heights_pixels is not None:
                    # Use the pixels above and below the height of the sensor
                    # to extract the data
                    site_index_in_slices = np.searchsorted(
                        grid.slices, site_z
                    )
                    start_index, end_index = (
                        site_index_in_slices - extraction_heights_pixels,
                        site_index_in_slices + extraction_heights_pixels - 1 ,
                    )
                    if start_index <= 0:
                        start_index = 0
                        end_index = 2*extraction_heights_pixels - 1
                    if end_index >= len(grid.slices):
                        end_index = len(grid.slices) - 1
                        start_index = len(grid.slices) - 2*extraction_heights_pixels
                
                    sel_dict["z"] = slice(
                        grid.slices[start_index],
                        grid.slices[end_index],
                    )
                ds_rad = ds.sel(**sel_dict)
                # Reset the cooridnates to integers starting from 0
                ds_rad_int = ds_rad.assign_coords(
                    {
                        "x": np.arange(ds_rad.sizes["x"]),
                        "y": np.arange(ds_rad.sizes["y"]),
                        "z": np.arange(ds_rad.sizes["z"]),
                    }
                )
                # Add the information about the coordinates
                ds_rad_int["x_coordinates"] = (("x"),ds_rad["x"].values)
                ds_rad_int["y_coordinates"] = (("y"),ds_rad["y"].values)
                ds_rad_int["z_coordinates"] = (("z"),ds_rad["z"].values)

                radiuses[site.item()] = ds_rad_int
            extracted_cubes[situation] = xr.concat(
                radiuses.values(),
                pd.Index(radiuses.keys(), name="process"),
            )

        # free space by deleting the variables
        del ds
        if catalogue == Catalogues.GRAMM_WINDS:
            del u, v, w
        if catalogue == Catalogues.GRAL_WINDS:
            for ds in dss:
                del ds
            del byte_list

        elapsed = time.time() - start
        logger.info(f"{situation=}, {elapsed} seconds")

    return_ds = xr.concat(
        extracted_das.values(),
        pd.Index(extracted_das.keys(), name="weather_situation"),
    )
    df_processes = df_our_sites.loc[return_ds["process"].to_numpy()]
    # Add  process dimensionS
    coords = {
        col: ("process", df_processes[col].values)
        for col in ["site", "latitude", "longitude", "height_of_building"]
    } | {
        "height_above_ground_extraction": (
            "process",
            df_processes["z"].values,
        )
    }
    return_ds = return_ds.assign_coords(coords)
    if return_cubes:
        return_cubes = xr.concat(
            extracted_cubes.values(),
            pd.Index(extracted_cubes.keys(), name="weather_situation"),
        )
        return_cubes = return_cubes.assign_coords(coords)
        # Transpose for compatibility with plot programms (like ncview)
        return_cubes = return_cubes.transpose(..., "z", "y", "x")

        return return_ds, return_cubes

    return return_ds
