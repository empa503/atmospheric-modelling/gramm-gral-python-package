from os import PathLike
from pathlib import Path
from typing import Iterable

import numpy as np

from pygg.grids import BasicGrid
from pygg.utils import Model


def get_simulated_dirs(run_dir: PathLike, model: Model) -> list[Path]:
    """Returns the paths of all the simulated directories."""
    return [
        d
        for d in Path(run_dir).iterdir()
        if d.is_dir() and d.name.startswith(f"{model.value}_")
    ]


def get_mask_stations_in_grid_bound(
    stations_x: Iterable[float],
    stations_y: Iterable[float],
    grid: BasicGrid,
    n_neigbors: int = 0,
) -> np.ndarray:
    return np.logical_and.reduce(
        [
            (stations_x - n_neigbors * grid.dx) > grid.xmin,
            (stations_x + n_neigbors * grid.dx) < grid.xmax,
            (stations_y - n_neigbors * grid.dy) > grid.ymin,
            (stations_y + n_neigbors * grid.dy) < grid.ymax,
        ],
        axis=0,
    )


def extract_at_stations(
    stations_x: Iterable[float],
    stations_y: Iterable[float],
    grid: BasicGrid,
    arrays: dict[str, np.ndarray],
    n_neigbors: int = 1,
) -> dict[str, np.ndarray]:
    """Extract a value of the arrays at the station positions.

    First dimension of the arrays must be x and second must be y.
    The arrays are transformed this ways:
    dims_in: (x, y, **other_dims)
    dims_out: (neibors_x, neigbors_y, stations, **other_dims)

    The point to extract is assumed to be always in the middle of a grid cell.
    The number of neigbor given is the number of grid cell centers in each direction
    to be extracted.
    TODO: If the number is 0, this will return a single value which is interpolated between the neighbors.
    """
    # perform a ceck on the stations loaction
    mask_valid_stations = get_mask_stations_in_grid_bound(
        stations_x,
        stations_y,
        grid,
        n_neigbors,
    )
    if not np.all(mask_valid_stations):
        raise ValueError(
            "Some stations are out of bounds when including the neighborhood of the"
            f" measurement. {np.where(~mask_valid_stations)}"
        )

    reserved_keys = ["x", "y", "z"]
    if any((key in reserved_keys for key in arrays.keys())):
        raise KeyError(
            f"Arrays cannot contain the following reserved_keys {reserved_keys}. "
            f"Received {list(arrays.keys())}."
        )
    # Check the size of the arrays
    cell_index_x, cell_index_y = grid.get_index(
        np.array(stations_x), np.array(stations_y)
    )
    variables_mapping = {
        key: np.asarray(
            [
                [
                    # We just add neighbor_x, neigbor_y, station in front of the other dims
                    array[cell_index_x + x_offset, cell_index_y + y_offset]
                    for y_offset in range(-n_neigbors, n_neigbors + 1)
                ]
                for x_offset in range(-n_neigbors, n_neigbors + 1)
            ]
        )
        for key, array in arrays.items()
    }

    # Add the coordinates of grid cell centers
    variables_mapping["x"] = (
        grid.xmin
        + grid.dx
        * np.column_stack(
            [
                cell_index_x + x_offset
                for x_offset in range(-n_neigbors, n_neigbors + 1)
            ],
        )
        + grid.dx * 0.5
    )
    variables_mapping["y"] = (
        grid.ymin
        + grid.dy
        * np.column_stack(
            [
                cell_index_y + y_offset
                for y_offset in range(-n_neigbors, n_neigbors + 1)
            ],
        )
        + grid.dy * 0.5
    )
    variables_mapping["z"] = grid.z

    # Add the coordinates
    return variables_mapping
