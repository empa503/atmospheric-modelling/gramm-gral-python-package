"""Concentrations are given by the gral model as .con files.

One file in gral correspond to one weather situation, one source group and 
one vertical slice.
"""
from __future__ import annotations

from os import PathLike
from pathlib import Path
from typing import Iterable
import shutil

from concurrent.futures import ThreadPoolExecutor

import numpy as np
import xarray as xr

from pygg.grids import GralGrid
from pygg.utils import fields_to_xarray
from pygg.outputs.utils import get_simulated_dirs
from pygg.utils import Model


def available_conc_files(run_dir: Path) -> list[Path]:
    """Get all the files available.

    For GRAL
    """
    # list the gramm run directory for gramm simulation directories
    simu_dirs = get_simulated_dirs(run_dir, Model.GRAL)

    # each dir contain one or more wind files
    suffix = ".con"

    return [f for d in simu_dirs for f in d.iterdir() if f.suffix == suffix]


def available_conc_fields(run_dir: Path) -> dict[int, Path]:
    """Get the fields that have been generated.

    Can be used for gramm or for gral.
    """

    # get the field number from the file name
    return dict(
        sorted({int(f.stem[:5]): f for f in available_conc_files(run_dir)}.items())
    )


def read_con_file(
    file: Path, gral_grid: GralGrid, check_data: bool = False
) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Read a .con file.

    Note this reads the concentration field as an array for the gral grid.
    TODO: check if the concentration field is given for the whole grid cell or for the cell edges, or for the cell center

    :param file: Path to the .con file
    :param gral_grid: The GRAL grid of the con file
    :param check_data: Whether to check that the sum of the loaded data is
        the same as the concentration from the file.
    """
    dt = np.dtype([("x", "i4"), ("y", "i4"), ("c", "f4")])

    with open(file, "rb") as f:
        b = f.read()
    data = np.frombuffer(b, dt, offset=4)  # read whole binary file

    # Turn coordinates into indices, make sure indices are integers for vecorised indexing
    i = (data["x"] - int(gral_grid.xmin)) // int(gral_grid.dx)
    j = (data["y"] - int(gral_grid.ymin)) // int(gral_grid.dy)

    conc = np.zeros(gral_grid.shape, np.float32)

    # Add each coordinates to the cell, convert to 1D because np.add.at does not
    # work on the 2d arrays
    # np.add.at(conc.reshape(-1), (i*gral_grid.ny + j).astype(int), data['c'])
    conc[i, j] = data["c"]

    if check_data:
        # The following assertion seems to not always work exactly proabably due to rounding errors
        assert np.sum(conc) == np.sum(
            data["c"]
        ), f"got {np.sum(conc)=} != {np.sum(data['c'])=}"

    return conc


def read_3d_con(
    gral_dir: PathLike,
    weather_situation: int,
    source_group: int,
    gral_grid: GralGrid | None = None,
) -> xr.Dataset:
    """Read 3d concentration from a gral output directory.


    :arg gral_grid: Can provide the grid. Useful if this is called many time,
        we don't need to generate the grid
    """

    gral_dir = Path(gral_dir)

    if gral_grid is None:
        # Read from the gral directory
        gral_grid = GralGrid.from_gral_rundir(gral_dir)
    else:
        # Make sure the slices were given
        if not gral_grid.slices:
            raise ValueError(
                "Slices should be set to the gral grid given. "
                "Slices are read not from the grid.geb file but from "
                "the in.dat file. If you generate your grid using "
                "GralGrid.from_gral_rundir() slices will be set if the in.dat file is found."
            )

    data = {
        height: read_con_file(
            gral_dir / f"{weather_situation:05}-{level+1}{source_group:02}.con",
            gral_grid,
        )
        for level, height in enumerate(gral_grid.slices)
    }

    conc_3d = np.stack(list(data.values()), -1)
    conc_ds = fields_to_xarray({"conc": conc_3d}, gral_grid, z_axis=list(data.keys()))

    return conc_ds


def extract_con_files(
    run_dir: PathLike, output_dir: PathLike, subdir_sg_format: str | None = None
):
    """Copy the simulated concentration files to the output directory.

    :arg run_dir: The directory where the simulations are running
    :arg output_dir: The directory where the files should be moved
    :arg subdir_sg_format: Optional variable.
        If given the files are moved to subsdirectories corresponding to the
        different source groups. The format string should contain one
        formatting variable for the source group number. ex: "SG_{sg}" .
    """

    run_dir = Path(run_dir)
    output_dir = Path(output_dir)


    def copy_file(file: Path):
        # Get the source group number from the file name
        sg = int(file.stem[-2:])

        # Get the output directory
        if subdir_sg_format is not None:
            # Create the subdirectory if it does not exist
            output_dir_sg = output_dir / subdir_sg_format.format(sg=sg)
            output_dir_sg.mkdir(exist_ok=True)
        else:
            output_dir_sg = output_dir

        # Get the output file name
        output_file = output_dir_sg / file.name

        if output_file.is_file():
            return

        shutil.copy(file, output_file)


    with ThreadPoolExecutor() as executor:
        executor.map(copy_file, run_dir.rglob("*.con"))



