"""Wind files are output of the GRAMM simulation.

Each weather situation has a corresponding wind file.
The stem of the file is the weather situation id.
"""
from __future__ import annotations
from typing import TYPE_CHECKING
from pathlib import Path
import struct
import zipfile
import numpy as np
import xarray as xr
from pygg.outputs.utils import get_simulated_dirs
from pygg.utils import Model
if TYPE_CHECKING:
    from pygg.grids import GralGrid

def available_wind_files(run_dir: Path, model: Model) -> list[Path]:
    """Get all the files available."""
    # list the gramm run directory for gramm simulation directories
    simu_dirs = get_simulated_dirs(run_dir, model)

    # each dir contain one or more wind files
    if model == Model.GRAMM:
        suffix = ".wnd"
    elif model == Model.GRAL:
        suffix = ".gff"
    else:
        raise NotImplementedError(
            f"Unkown model {model}. Only {Model.GRAMM} and {Model.GRAL} are known"
        )

    return [f for d in simu_dirs for f in d.iterdir() if f.suffix == suffix]


def available_wind_fields(run_dir: Path, model: Model) -> dict[int, Path]:
    """Get the fields that have been generated.

    Can be used for gramm or for gral.
    """

    # get the field number from the file name
    return dict(
        sorted({int(f.stem): f for f in available_wind_files(run_dir, model)}.items())
    )


def read_wind_wnd(wind_file: Path) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Read a wind.wnd file from a gramm simulation.

    This function read wind.wnd file of the format 0
    """

    with open(wind_file, "rb") as f:
        data = f.read()

    N_HEADER = 20  # header, ni,nj,nk,gridsize  -> 4*integer(4*4) + 1*float(4)
    header, nx, ny, nz, dx = struct.unpack("<iiiif", data[:N_HEADER])

    datarr = np.frombuffer(data[N_HEADER:], dtype=np.short)
    datarr = np.reshape(datarr, [nx, ny, nz, 3]) * 0.01
    u = datarr[:, :, :, 0]
    v = datarr[:, :, :, 1]
    w = datarr[:, :, :, 2]

    return u, v, w

def load_gff_bytes(wind_file: Path) -> bytes:
    if zipfile.is_zipfile(wind_file):
        gff = zipfile.ZipFile(wind_file, "r")

        for filename in gff.namelist():
            byte_list = gff.read(filename)
            gff.close()
    else:
        with open(wind_file, mode="rb") as binfile:
            byte_list = binfile.read()
    
    return byte_list

def read_wind_gff(wind_file: Path):
    """Read a GRAL wind file.

    This operation takes time because the file is huge and it has to be unzipped.
    """


    byte_list = load_gff_bytes(wind_file)

    N_HEADER = 32
    nz, ny, nx, direction, speed, akla, dxy, header = struct.unpack(
        "iiiffifi", byte_list[:N_HEADER]
    )

    if header != -1:
        raise NotImplementedError(
            "Only one format is supported at the moment. "
            f"Expected header =-1 but {header=}. "
            "See GFFRead.cs from GRAL source code for more details."
        )
    count = (nx + 1) * (ny + 1) * (nz + 1) * 3

    data = np.frombuffer(byte_list, dtype=np.short, offset=N_HEADER, count=count)

    d = data.reshape((nx + 1, ny + 1, nz + 1, 3)) * 0.01
    u = d[:, :, :, 0]
    v = d[:, :, :, 1]
    w = d[:, :, :, 2]
    return u, v, w





def load_part_of_gral(
    wind_bytes: bytes,
    gral_grid: GralGrid, 
    extraction_center: tuple[float, float, float],
    extraction_radius: float,
    extraction_height: float | None = None,
) -> xr.Dataset:
    """Return a small part of the gral domain."""
    
    N_HEADER = 32

    nz, ny, nx, direction, speed, akla, dxy, header = struct.unpack(
        "iiiffifi", wind_bytes[:N_HEADER]
    )

    # Get the indices of the extraction center
    i, j = gral_grid.get_index(extraction_center[0], extraction_center[1])

    di = int(extraction_radius / gral_grid.dx)
    dj = int(extraction_radius / gral_grid.dy)
    # Z in not regular in GRAL
    if extraction_height is None:
        extraction_height = extraction_radius
    else:
        extraction_height /= 2
    k_top = np.searchsorted(gral_grid.z, extraction_center[2] + extraction_height)
    k_bot = np.searchsorted(gral_grid.z, extraction_center[2] - extraction_height)

    istart = max(0, i - di)
    iend = min(nx, i + di)
    jstart = max(0, j - dj)
    jend = min(ny, j + dj)

    if istart >= iend or jstart >= jend:
        raise ValueError("The extraction point is outside the domain.")
    
    ni = iend - istart
    nj = jend - jstart
    nk = k_top - k_bot


    # Extract the 3D field
    array = np.empty((ni, nj, nk, 3), dtype=np.float32)



    for i_ in range(istart, iend):
        for j_ in range(jstart, jend):
            # Before was:
            # offset = N_HEADER + 2 * ((i + x - dx) * (ny + 1) * (nz + 1) + (j + y - dy) * (nz + 1) + z) * 3
            # now:
            offset = N_HEADER + 2 * 3 * (i_   * (ny + 1) * (nz + 1) + j_  * (nz + 1) + k_bot)

            # Read the data
            datarr = np.frombuffer(wind_bytes, dtype=np.short, offset=offset, count=nk * 3)
            array[i_ - istart, j_ - jstart, :, :] = datarr.reshape((nk, 3))

        
                

    # Reshape and scale the array
    array *= 0.01

    u = array[:, :, :, 0]
    v = array[:, :, :, 1]
    w = array[:, :, :, 2]

    # Create the dataset
    ds = xr.Dataset(
        {
            "U": (["x", "y", "z"], u),
            "V": (["x", "y", "z"], v),
            "W": (["x", "y", "z"], w),
        },
        coords={
            "x": gral_grid.xcorner[istart:iend],
            "y": gral_grid.ycorner[jstart:jend],
            "z": gral_grid.z[k_bot:k_top],
        },
    )

    return ds
