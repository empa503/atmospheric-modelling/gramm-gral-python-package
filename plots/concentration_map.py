# %%
from pathlib import Path
from multiprocessing import Pool
from functools import partial

import pandas as pd
import numpy as np
import xarray as xr

from pygg.outputs.concentrations import read_con_file, read_3d_con
from pygg.grids import GralGrid
from pygg.utils import fields_to_xarray

# %%

gral_grid = GralGrid.from_geb(
    "/store/c2sm/amrs/gg/zurich/Zurich_CO2_clean/GRAL.geb"
)

# %%
# match2obs file

match2obs_file = "/scratch/snx3000/lconstan/match2obs/results_match2obs.nc"
ds_match2obs = xr.load_dataset(match2obs_file, engine="netcdf4")

best_siutations = ds_match2obs["weather_situation"]  # Get the best situation
ranks = len(ds_match2obs["rank"])
unique_siutations, counts = np.unique(
    best_siutations, return_counts=True
)  # Get the unique situations and their counts

ds_match2obs

# %%

con_fields = {}
gral_grid.slices = [2, 5, 10, 20, 30, 45, 60, 75, 100]
source_groups = list(ds_match2obs['source_group'].values)
print(source_groups)
for source_group in source_groups:
#for source_group in [44, 45]:
    print("processing source group", source_group)
    conc_dir = Path(f"/store/c2sm/amrs/gg/zurich/CO2/SG_{source_group:02d}/")

    def read_con_file_wrapper(situation, gral_grid):
        return read_3d_con(
            gral_dir=conc_dir,
            weather_situation=situation,
            source_group=source_group,
            gral_grid=gral_grid,
        )["conc"].to_numpy()

    # Get the scaling we need to apply to each situation
    activity = (
        ds_match2obs["activity_of_source_group"].sel(source_group=source_group).values
    )
    time_factors = ds_match2obs["scaling_factors"].sel(category=activity)
    not_nan_times = ~np.isnan(time_factors)
    # how to scale the weather situations
    scaling_factors = (
        xr.ones_like(ds_match2obs["weather_situation"])
        * time_factors
        / len(ds_match2obs["rank"])
        / not_nan_times.sum()
    )

    # Get how much each situation should be scaled for the total map
    situation_scaling = np.zeros(len(ds_match2obs["weather_situation"]) + 1)
    np.add.at(
        situation_scaling,
        ds_match2obs["weather_situation"].loc[not_nan_times].values.reshape(-1),
        scaling_factors.loc[not_nan_times].values.reshape(-1),
    )
    (unique_siutations,) = np.where(situation_scaling)
    situation_scalings = situation_scaling[unique_siutations]

    with Pool(16) as p:
        conc_fields = p.map(
            partial(read_con_file_wrapper, gral_grid=gral_grid), unique_siutations
        )

    
    global_field = np.zeros_like(conc_fields[0])
    for field, weigth in zip(conc_fields, situation_scalings):
        global_field += field * weigth 

    con_fields[source_group] = (
        fields_to_xarray({"conc": global_field}, gral_grid, z_axis=gral_grid.slices)
        .assign_coords(source_group=source_group)
        .expand_dims("source_group")
    )
    # Delete from the memory
    for f in conc_fields:
        del f
    del conc_fields


conc_ds = xr.concat(con_fields.values(), dim="source_group")


# %%
conc_ds = conc_ds.rio.set_spatial_dims(x_dim="x", y_dim="y")
conc_ds = conc_ds.transpose(..., "y", "x")
conc_ds = conc_ds.rio.write_crs("LV03")

# %%
# SAve as a raster
conc_ds["conc"].to_netcdf(f"/scratch/snx3000/lconstan/match2obs/conc_map/conc_3d.nc")


#%%

# %%
