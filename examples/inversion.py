import xarray as xr
import pandas as pd
import datetime
import numpy as np


def run_inversion(obs, sim, time1, time2, station_err, emission_err, optimize_background = False, is_background_category = None):
    """Algorithm for the inversion of the emissions using the Kalman filter
    obs: observed data, already selected for desired optimization period, xarray with time and site coordinates
    sim: simulated data, already selected for desired optimization period, xarray with time, site and category coordinates
    time1: start time for the inversion
    time2: end time for the inversion
    station_err : array with the uncertainty of the stations, one value for each site(not yet squared)
    emission_err: Squared uncertainty of the emissions
    optimize_background: If True, the off-diagonal elements of the covariance matrix B are adapted for background-categories
    is_background_category: Array with boolean values, True if the category is a background category, False otherwise
    """
    ## ---------------- y_obs ----------------------##
    # observation vector y_obs:
    # make a 1D vector with all sites stacked one after the other, for all selected times
    ds_y_obs = obs.stack(all_sites_and_times=("site", "time"))
    y_obs = ds_y_obs.values  # 1xm vector
    #dont use nans
    mask_nan = ~np.isnan(y_obs)
    y_obs = y_obs[mask_nan]


    ## ---------------- H ----------------------##
    # simulation matrix H:
    # make a matrix with all sites and times stacked one after the other, for all categories as columns:
    ds_H = sim.stack(all_sites_and_times=("site", "time")).T
    H = ds_H.values  # n x m matrix

    #if optimize_background:
        # set the values that are not part of the specific day-column to zero
        
    # remove for each category the rows with nans
    H = H[mask_nan,:]

    ## ---------------- x_a ----------------------##
    ## A priori state vector
    # Should contain onle 1s, as we use our simulations (derived from the given inventory) as a priori state
    # This is the vector x_a in the Bayesian inversion
    x_a = np.ones(H.shape[1])  # n x 1 vector

    ## ---------------- R ----------------------##
    # Error covariance matrix model-observation error covariance matrix R
    # We assume that the error is given by the station statistics (crmse of obs-sim)

    # Get a vector with repeated crmse for each station and time step.
    # For each station, use a constant crmse for all time steps
    errs_per_time_and_site = np.stack(
        [np.repeat(r, len(obs.time)) for r in station_err], axis=0
    ).flatten()[mask_nan]  #  m x m matrix with m = len(time)xlen(sites), mask station and times with nan

    # The matrix should have for each station the crmse of that station (same for each time step)
    R = np.diag(np.ones(H.shape[0]) * errs_per_time_and_site**2)

    ## ---------------- B ----------------------##
    # Define the emission uncertainty error covariance matrix B (apriori error covariance matrix)
    B = np.diag(emission_err)
    if optimize_background:
        # set the off-diagonal elements of the covariance matrix B
        for i in range(B.shape[0]):
            for j in range(B.shape[1]):
                if is_background_category[i] == False or is_background_category[j] == False: 
                    # only adapt the elements for the background, not for other categories
                    continue
                else:
                    if i == j:
                        #B[i, j] = 0.01**2 # diagonal elements (should already be set before)
                        continue
                    else:
                        dT = abs(i-j) # time difference in days
                        tau = 1 # correlation lenghts in days
                        B[i, j] = B[i,i] * B[j,j] * np.exp(-dT/tau) # off-diagonal elements

    ## ---------------- Kalman filter ----------------------##
    ## Apply the Kalman filter
    # The Kalman filter is applied in the following way:
    # x = x_a + K * (y_obs - H * x_a)
    # K = B * H^T * (H * B * H^T + R)^-1
    # State covariance matrix P = (I - K * H) * B
    # The Kalman filter is applied for each time step separately
    # I is the identity matrix
    #print(f"\n Run the Kalman filter for {time1} to {time2}")
    K = (B @ H.T) @ np.linalg.inv(H @ B @ H.T + R)  ## Apply the Kalman filter

    x = x_a + K @ (y_obs - H @ x_a)

    # State covariance matrix:
    P = (np.eye(H.shape[1]) - K @ H) @ B

    return x, K, P