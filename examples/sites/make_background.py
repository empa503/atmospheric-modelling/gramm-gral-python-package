"""
This script creates a merged background CO2 concentration from the 3 stations Laegern, Beromünster and Birchwil (brei). 

Author: Dominik Brunner
Modified by: Leonie Bernet
"""

## Dominik's Code to get a merged background depending on wind direction

# coordinate transformations / projections

import pandas as pd
from pyproj import Proj, Transformer
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt


from sensorsdata import VariableNames, CarbosenseReader


data_dir = Path("/store/c2sm/amrs/gg/zurich")

carbosense_data = CarbosenseReader(
    data_dir / "measurements_data/20240923_140259_smonitor_icos_cities_data_export.zip"
)

def get_site_conc_and_weights(site):
    """
    Get site concentrations and calculate wind direction weights compared to zürich
    """
    pWorld = 4326  # epsg:4326
    pCH = 2056  # epsg:2056
    pMerc = 3857  # web-mercator
    w2ch = Transformer.from_crs(pWorld, pCH)
    ch2w = Transformer.from_crs(pCH, pWorld)
    ch2m = Transformer.from_crs(pCH, pMerc)
    w2m = Transformer.from_crs(pWorld, pMerc)

    co2_var = 'CO2' if site == 'brei' else 'CO2_GA'

    if site == 'brm':
        # Load all available processes at Berromünster and select the highest measurement height
        process_brm = 7023 # 7023 is the process for the highest measurement height (212m)
        conc, std = carbosense_data.timeseries_of_site(
            site, VariableNames[co2_var], return_mean=False, return_std=True
        )
        conc = conc.loc[process_brm]  
        std = std.loc[process_brm]
    else:
        conc, std = carbosense_data.timeserie_of_site(
            site, VariableNames[co2_var], return_std=True, drop_duplicates=True
        )

    # We construct a background time series with regular hourly timesteps by averaging Breite, Lägern and Beromünster
    # with weights corresponding to the distance of the actual wind angle to the angle
    # at which a given station is ideally upwind of Zurich
    # Coordinates:
    lon = carbosense_data.df_sites.loc[site].longitude
    lat = carbosense_data.df_sites.loc[site].latitude
    x, y = w2ch.transform(lat, lon)

    # Reference coordinate: Zurich
    zuelon = carbosense_data.df_sites.loc["zue"].longitude
    zuelat = carbosense_data.df_sites.loc["zue"].latitude
    zuex, zuey = w2ch.transform(zuelat, zuelon)

    if site == "brei":
        # Birchwil (Breite) is located east of Zurich -> negative wind directions
        a_site = 90.0 - np.rad2deg(np.arctan((x - zuex) / (y - zuey)))
    else:
        a_site = 270.0 - np.rad2deg(np.arctan((x - zuex) / (y - zuey)))

    # compute weights as function of angle difference for the 3 sites
    # and illustrate the weights for all wind directions from 0 to 360deg
    angles = np.arange(36) * 10.0
    w_site = []
    width = 30.0
    for a in angles:
        w_site.append(
            np.exp(-np.square(min(np.abs([a - a_site, a - 360.0 - a_site])) / (2 * width)))
        )

    # plt.figure()
    # plt.plot(angles,w_site)
    # plt.title(site)
    # plt.show()

    return conc, std, a_site, w_site


def create_background_co2(sites=["brei","lae","brm"], save_file=True, t_start="2022-08-01", t_end="2024-09-02"):
    """
    Create a background time series based on the three surrounding sites, weighted with the wind direction 
    data_dir: str _ path to the directory where the file is stored (if save_file is True)
    t_end: This day will not be included anymore (e.g. 2.9. will end at 2.9. 00:00)
    """

    ##------------------
    # Now we construct the background time series, but first we need to merge all data sets into a single
    # dataframe with regular hourly resolution and fill gaps by linear interpolation
    sdate = t_start
    edate = t_end

    # create empty data frame with hourly resolution from sdate to edate
    datetime = pd.date_range(sdate, edate, freq="H")
    data = np.zeros(len(datetime))
    df = pd.DataFrame({"date": datetime, "background": data})
    df = df.set_index("date")

    # we can try to use wind measurements at Breite to determine the general flow siutation
    brei_ws, brei_std = carbosense_data.timeserie_of_site(
        "brei", VariableNames.WIND_SPEED, return_std=True, drop_duplicates=True
    )
    brei_wd, brei_std = carbosense_data.timeserie_of_site(
        "brei", VariableNames.WIND_DIR, return_std=True, drop_duplicates=True
    )

    df = df.merge(brei_wd.to_frame(name="brei_wd"), how="left", on="date")

    ##-------------------
    # combine with conc time series from the three stations and wind from breite
    for site in sites: 
        conc, std, a_site, w_site = get_site_conc_and_weights(site)
        df = df.merge(conc.to_frame(name=site + "_conc"), how="left", on="date")
        i_site = df[site + "_conc"].isna()

        
        # temporally interpolate all conc and wind time series (not a good idea for wind direction, but only very few gaps (if any))
        df.interpolate(method="linear", axis="index", inplace=True)
        
        ##-------------------
        width = 30  # width (1 sigma) of the gaussian weights in degrees

        # compute weights for the three stations and sum of weights
        df["w"+site] = np.exp(
            -np.square(
                np.min(np.abs([df["brei_wd"] - a_site, df["brei_wd"] - 360.0 - a_site]), axis=0)
                / (2 * width)
            )
        )

        # set weights to zero for all invalid concentration measurements
        df.loc[i_site, ("w"+site)] = 0.0
        

    ##-------------------
    # sum of weights used for normalization
    df["wsum"] = df["wbrei"] + df["wlae"] + df["wbrm"]

    # weighted mean concentration
    df["background"] = (
        df["brei_conc"] * df["wbrei"]
        + df["lae_conc"] * df["wlae"]
        + df["brm_conc"] * df["wbrm"]
    ) / df["wsum"]

    if save_file:
        file_dir = data_dir / "background/"
        bgfile = file_dir /  f"background_{sdate}-{edate}.csv"
        df["background"].to_csv(bgfile)
    
    for site in sites: 
        i_site = df[site + "_conc"] == 0
        df.loc[i_site, (site + "_conc")] = np.nan

    return df


# Run the function if main is executed
if __name__ == "__main__":
    create_background_co2()