"""Processing human respirations emissions.

This script is just attended as an example of what one can do for loading 
the data set and read the respiration data from people living.
"""
# %%
%load_ext autoreload 
%autoreload 2
from pathlib import Path
import geopandas as gpd 
from emiproc.human_respiration import load_data_from_quartieranalyse, people_to_emissions
from pygg.grids import GralGrid


# %% Load the data
gral_grid = GralGrid.from_gral_rundir("/scratch/snx3000/lconstan/gramm_gral2/GRAL_00002")

df = load_data_from_quartieranalyse(
    "Quartieranalyse_-OGD.gpkg", gral_grid
)
df
# %% Load into an emiproc Inventory
resp_inv = people_to_emissions(
    df,
    time_ratios={'people_living': 0.6, 'people_working': 0.4}, 
    output_gdfs=True
)

#%%
from emiproc.inventories.utils import get_total_emissions

total_emissions = get_total_emissions(resp_inv)

# %% Do a first approximation of what we should get to make sure we are not too wrong
approximated_emission = (
    400000. # inhabitants in zurich 
    * 1 # kg co2 / person / day
    * 365 # days in year
)
# Check the ratio
approximated_emission / total_emissions['CO2']['__total__']

#%%
######
# Comparison with previous emissions

# It is just there if someone wants to compare the emissions
######

# %%
import xarray

raster_ivo_dir = Path("/store/empa/em05/isuter/projects/gg/data/emissions/zurich/Raster/CO2")
xds = xarray.open_dataset(raster_ivo_dir/ "61_Einw_HA_10m_small_filled_scaled.tif", engine="rasterio")
xds['band_data'].T.plot()
# %%
from emiproc.regrid import remap_inventory

remapped = remap_inventory(resp_inv, gral_grid.to_emiproc())
# %%
plt.figure()
plt.imshow(remapped.gdf[('people_living', 'CO2')].to_numpy().reshape(gral_grid.shape).T)
plt.show()