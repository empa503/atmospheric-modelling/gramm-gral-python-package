#%%
import logging
from pathlib import Path
from datetime import datetime
from sensorsdata import VariableNames, CarbosenseReader
from pygg.outputs.catalogue import generate_catalogue, Catalogues, Algorithm


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("pygg.create_catalogue")

carbosense_dir = Path(
    "/store/c2sm/amrs/gg/zurich/measurements_data/20240522_160955_smonitor_icos_cities_data_export.zip"
)
carbosense_data = CarbosenseReader(carbosense_dir, metadata=True)


catalogues_dir = Path("/store/c2sm/amrs/gg/zurich")


files_of_catalogue = {
    Catalogues.GRAMM_WINDS: catalogues_dir / "wndandscl",
    Catalogues.GRAL_WINDS:  catalogues_dir / "gff",
    Catalogues.GRAL_CONCS: catalogues_dir / "CO2",
}

extraction_radiuses = {
    Catalogues.GRAMM_WINDS: 1000,
    Catalogues.GRAL_WINDS: 50,
    Catalogues.GRAL_CONCS: 20,
}
# How many pixels above and below the height of the sensor to extract
# If not there, it means we use the height instead
extraction_heights_pixels = {
    Catalogues.GRAL_CONCS: 1,
}

all_source_groups=[
        1,
        6,
        7,
        9,
        11,
        12,
        14,
        15,
        17,
        18,
        34,
        35,
        36,
        37,
        44,
        45,
        46,
        47,
        50,
        51,
        52,
        53,
        54,
        55,
        56,
        57,
        58,
        59,
        61,
        62,
        71,
        72,
        74,
    ]

return_cubes = True

# Extracting catalogue at sites from transient simulation
trans_sim = False

# define independent variables:
catalogue = Catalogues.GRAL_CONCS
source_groups = all_source_groups

# define variables depending on choice of trans_sim:
if trans_sim:
    # transient simulation case
    run_dir = (
        catalogues_dir / "test_gral_transient_simulation/run_dir_trans_sim"
    )
    # for transient simulation: give the folder where the concentration files are saved:
    files_dir = catalogues_dir / "test_gral_transient_simulation/run_dir_trans_sim/GRAL_00512"
    # for transient simulation: give the hours instead of weather situations to read correct conc files!
    # get the number of transient hours from the mettimeseries.dat file:
    with open(f"{files_dir}/mettimeseries.dat") as file:
        transient_n_hours = sum(1 for line in file)
    weather_situations = list(range(1, transient_n_hours + 1))
    #output directory:
    dir = Path(catalogues_dir / "test_gral_transient_simulation/extracted_catalogue/") 
else:
    # default case
    run_dir = catalogues_dir / "Zurich_CO2_clean"
    weather_situations = None
    files_dir = files_of_catalogue[catalogue]
    # Output dir must be on scratch as sbatch is not allowed to write on /store
    dir = Path("/scratch/snx3000/lbernet/carbosense_data/catalogues_at_sites/")

#%%
# no height processes 

mask_no_height = (carbosense_data.df_processes['height_above_ground_sensor'].isnull() & carbosense_data.df_processes['height_above_ground_measurement'].isnull()) 
mask_var = carbosense_data.df_processes['variable'].isin(['ws', 'wd', ])
sites_missing_heights = carbosense_data.df_processes.loc[mask_no_height & mask_var, 'site'].unique()
logger.warning(f"Sites missing heights wind: {sites_missing_heights}")

# For conc is another var 
mask_no_height = (carbosense_data.df_processes['height_above_ground_measurement'].isnull())
mask_var = carbosense_data.df_processes['variable'].isin(['co2_adj_cyl'])
sites_missing_heights = carbosense_data.df_processes.loc[mask_no_height & mask_var, 'site'].unique()
logger.warning(f"Sites missing heights concs: {sites_missing_heights}")

#%%
output = generate_catalogue(
    catalogue=catalogue,
    run_dir=run_dir,
    df_processes=carbosense_data.get_df_for_catalogue(catalogue),
    weather_situations=weather_situations,
    files_dir=files_dir,
    extraction_radius=extraction_radiuses[catalogue],
    extraction_heights_pixels=extraction_heights_pixels.get(catalogue, None),
    return_cubes=return_cubes,
    algorithm=Algorithm.INTERPOLATION,
    source_groups=source_groups,
)
date_str = datetime.now().strftime("%Y%m%d-%H%M%S")
start_name = f"{date_str}_catalogue_{catalogue.name}"
logger.info(f"Output dir is {dir}")

if return_cubes is True:
    catalogue_array, cubes = output
else:
    catalogue_array = output

# Save to nc file
nc_file = dir / f"{start_name}.nc"
catalogue_array.to_netcdf(dir / nc_file)
logger.info(f"Catalogue saved to {nc_file}")

if return_cubes is True:
    cubes_files = dir / f"{start_name}_cubes_{extraction_radiuses[catalogue]}m.nc"
    cubes.to_netcdf(dir / cubes_files)
    logger.info(f"Cubes saved to {cubes_files}")

# %%
