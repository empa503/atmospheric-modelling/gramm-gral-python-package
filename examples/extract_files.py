"""Small script to extract files at the end of a simulation."""

from pygg.outputs.concentrations import extract_con_files



extract_con_files(
    run_dir="/scratch/snx3000/lconstan/gral/vegetation_zh/run_dir_v2",
    output_dir="/scratch/snx3000/lconstan/gral/vegetation_zh/con_v2",
    subdir_sg_format="SG_{sg}",

)