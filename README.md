# GRAMM-GRAL Python package

Python package for GRAMM-GRAL including pre-processing, running and post-processing

## Running 

Running GRAMM-GRAL requires the following steps:

* preprocessing the inputs
* running GRAMM 
* preprocessing GRAL emissions (using emiproc)
* running GRAL 

### config.yaml

This file can be used to run the preprocessing and to launch GRAMM and GRAL models.
An commented example of this file is found in the examples directory. 

To use this file the command `pygg.run` can be used: 

```bash 
python -m pygg.run /path/to/config.yaml
```

There is also an additional command with some utilities.
Run the following command to know more about it: 

```bash
python -m pygg --help
```

### Preprocessing 
Various inputs are required for running GRAMM and GRAL. 
Everything but the emission can be defined in the config.yaml file and is all automatically handled.

The source of the raw data is 

Topography: ASTER 
Landuse: CORINE 
Buildings heigts: (currently) a TIF file 


For the emissions, emiproc should be used.
An example script of how to generate the emissions is found on the [emiproc repository](https://github.com/C2SM-RCM/emiproc/blob/master/scripts/zh_2_gral.py) . 

### Running selected weather situations  
Choose  a subset of weather situations of interest. This can be specified in the config.yaml.

### Running all weather situations of a catalogue

This is done by running  pygg using the same config.yaml repeatedly. The script checks which input data was already generated and which GRAMM situations were already simulated, and then prepares the ones that are not, and launches them.
One simply has to figure out how many are left to simulate and continue to lauch the script to simulate the remaining ones.

The same applies to the GRAL simulations: They can be started only after the GRAMM simulations have finished. pygg will start the simulations that are missing in the same way. 

### Only running GRAL 

If you want to run different GRAL scenarios, you can point the run_dir to the same GRAMM simulation used for a previous GRAL-run but change the gral_run_dir to your new simulation. Make sure you don't overwrite GRAMM or GRAL files from the old simulation.


## Installing the pygg package

Run the following command in the root directory of the repository:

```bash
pip install -e .
```

### Dotnet on Piz Daint

To run the model on a Linux system, a dotnet framework needs to be installed.
We have a version of dotnet on Piz Daint at CSCS (/apps/empa/dotnet), which was installed by CSCS technical staff upon request by Empa: 
Contact: Edoardo Baldi

On daint we don't have access to the dotnet SDK required to compile the models, so we need to build them on our personal laptops and then copy them to daint.

### Installing GRAMM

1. Install the dotnet SDK on your laptop https://dotnet.microsoft.com/en-us/download. Make sure you install the same version as available on daint.
2. Clone or download a version from https://github.com/GralDispersionModel/GRAMM
3. Open the `src` folder in Visual Studio code
4. Run the build task *self contained exe linux*
    - (Ctrl + Shift + B) and then select the task
    - The tasks are defined in the .vscode/tasks.json file
5. Copy the the full folder with the executable to daint
    - executables are found in subfolders of bin/Release/
    - you can adapt the following command `scp -r path_from_your_computer/bin/Release/net6.0/linux-x64 daint:path_on_daint`


### Installing GRAL

Proceed in the same as for GRAMM. Note that the code is hosted on another repository.

## Postprocessing 


### Visualizing the inputs and outputs of the model

pygg provides functions to load and plot the data files used and produced by GRAMM/GRAL.
Various examples notebook are found in the example directory under the names visualize_***.ipynb 

### Extracting catalogues

Catalogues of winds and concentarations can be extracted using the function `pygg.outputs.catalogue.generate_catalogue`.
Examples for different types of catalogues are found in the examples directory.

### Match2obs catalogue

Once catalogues are extracted, the simulated catalogues can be matched to the observations. 
Example for this is found in the notebook `examples/match2obs.ipynb`


### data for match2obs

From this new version, we decided to only use the data files produced by carbosense network.

Stewart has a script to generate the data:

* wind (speed and direction)
* concentrations
* irradiation
* temperature 

Few comments on the data 

Dates are in UTC and one hour is the average of it till the next

Standard deviation are given as deviation over the hourly means

Uncertainty on the concentration can be estimated to 1 ppm, but as some sensor currently have a 5 ppm, we should use a 2 ppm uncertainty.

Low cost sensors are not included because 

Rogenstrasse is probably bad for the wind and alos other street ground level measurments.

data will be put on the scratch drive of steward as a zip scratch/503/stweart

## Limitations

### Not implemented features from GRAL 

The following features are not supported in pygg.

* tunnels 
* transient mode (only steady-state simulations are supported)
* separate surface rougness for each cell (see file RoughnessLengthsGral.dat )
* vegetation (file Vegetation.dat and file VegetationDepoFactor.txt)

* emissions specific to a weather situation, currently only supports same inventory for all GRAL runs.

* specificities for PM simulations (this needs to be jointly implemented with emiproc gral module)


### Bridges in GRAL 

The impact of bridges on the flow cannot be properly modeled with GRAL as buildings cannot have wholes.
It is thus not possible to simulate the air flow under a brigde.
Currently one has to mannually remove bridges from the dataset to avoid these bridges looking like buildings blocking the flow.

If one day it should be possible, the file creation for buildings must be
adapted to accound for the small heights.


### Meteorology
For running GRAL there exist multiple ways of providing meteo data.

In this project we choose to use only the meteogpt.dat file.

But as the previous implementation contained sonic and inputz, we 
kept the python files inside this reopository.


### How to design a model for the worst

This is just a reminder of all the bad design choiced in GRAMM and GRAL.

* Requiring a file to be present in the directory to say if an output should be generated.
* Having just one letter of difference between the two model names so you don't understand which one we talk about.
* try catch where the catch doesn't do anything
* config files that depend on a line number
* Putting german words in the code 
-> my favorite variable name is "ZeitschleifeNonSteadyState"
* Make the code so convoluted that you cannot test only parts of it
* A documentation that lives separated from your code (ideally you should always build the api documentation from the code)
* Use the same variable for different things with different meaning based on the execution context 
* conditional else if that don't end in a else for the default
* having thousands files for the input data
-> having two files with almost the same, but one is reversed